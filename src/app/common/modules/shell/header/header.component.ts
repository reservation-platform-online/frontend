import {Component, OnInit} from '@angular/core';
import {HeaderService} from '@app/common/services/header/header.service';
import {MenuController, NavController} from '@ionic/angular';
import {Store} from '@ngrx/store';
import {Logout} from '@app/ngrx-store/authentication/authentication.actions';
import * as fromStore from '@app/ngrx-store';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {Reactive} from '@app/common/cdk/reactive';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent extends Reactive implements OnInit {

  isLoggedIn: boolean = false;

  constructor(
      private readonly store: Store,
      private readonly screenSizeService: ScreenSizeService,
      private readonly headerService: HeaderService,
      private readonly menuController: MenuController,
      private readonly alertPromptService: AlertPromptService,
      private readonly navController: NavController
  ) {
    super();
  }

  get isMobile$(): Observable<boolean> {
    return this.screenSizeService.isMobileView$;
  }

  get title(): string {
    return this.headerService.getTitle();
  }

  ngOnInit() {

    this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((result: boolean) => {

      this.isLoggedIn = result;

    });

  }

  get profile$(): Observable<ProfileInterface> {
    return this.store.select(fromStore.selectProfileData).pipe(this.takeUntil()) as Observable<ProfileInterface>;
  }

  back() {
    this.navController.pop();
  }

  logout() {
    this.alertPromptService.presentAlert('Czy na pewno chcesz wylogować się?', {
      buttons: [
        {
          text: 'Nie',
          handler: () => {
          }
        },
        {
          text: 'Tak',
          handler: () => {
            this.store.dispatch(new Logout());
          }
        }
      ]
    });
  }

  toggleMenu() {
    this.menuController.toggle('side-menu');
  }
}
