import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from '@app/common/modules/shell/header/header.component';
import {IonicModule} from '@ionic/angular';
import {HeaderServiceModule} from '@app/common/services/header/header.module';
import {RouterModule} from '@angular/router';
import {ImgModule} from '@app/common/ui/img/img.module';



@NgModule({
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent
  ],
    imports: [
        CommonModule,
        HeaderServiceModule,
        IonicModule,
        RouterModule,
        ImgModule
    ]
})
export class HeaderModule { }
