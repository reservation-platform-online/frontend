import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  constructor(
      private readonly screenSizeService: ScreenSizeService
  ) { }

  ngOnInit() {}

  onIsMobile(): Observable<boolean> {
    return this.screenSizeService.isMobileView$;
  }

}
