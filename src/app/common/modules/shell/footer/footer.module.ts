import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FooterComponent} from '@app/common/modules/shell/footer/footer.component';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [
    FooterComponent
  ],
  exports: [
    FooterComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule
    ]
})
export class FooterModule { }
