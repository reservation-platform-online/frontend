import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToUpComponent} from '@app/common/modules/shell/to-up/to-up.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [
    ToUpComponent
  ],
  exports: [
    ToUpComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ToUpModule { }
