import {Component} from '@angular/core';

@Component({
  selector: 'app-to-up',
  template: `
    <ion-fab id="to-up" vertical="bottom" horizontal="center" slot="fixed" (click)="goToTop()">
      <ion-fab-button>
        <ion-icon name="custom-chevron-up-outline"></ion-icon>
        Do góry
      </ion-fab-button>
    </ion-fab>

  `
})
export class ToUpComponent {

  // TODO Detect any ion-content and handle scroll events

  goToTop() {
    document.querySelector('ion-content').scrollToTop(500);
  }
}
