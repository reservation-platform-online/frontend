import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopBarComponent} from '@app/common/modules/shell/top-bar/top-bar.component';
import {IonicModule} from '@ionic/angular';
import {IosArrowIconModule} from '@app/common/ui/icons/arrow/ios-arrow-icon.module';


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        IosArrowIconModule
    ],
    declarations: [
        TopBarComponent
    ],
    exports: [
        TopBarComponent
    ]
})
export class TopBarModule {

}
