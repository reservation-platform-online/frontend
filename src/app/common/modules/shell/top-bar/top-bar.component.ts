import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {Reactive} from '@app/common/cdk/reactive';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-top-bar',
    template: `
        <div (click)="openHomePage()" class="title">
            reservation-platform
        </div>

        <div class="reservation-platform-bar-top-right">

            <ion-button fill="clear" (click)="toggleSidebar()">
                <ion-icon name="custom-menu"></ion-icon>
            </ion-button>

        </div>
    `,
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopBarComponent extends Reactive implements OnInit {

    constructor(
        private readonly store: Store,
        private readonly menu: MenuController,
        private readonly navController: NavController,
    ) {
        super();
    }

    ngOnInit() {
    }

    toggleSidebar(): void {
        this.menu.toggle('reservation-platform-drawer');
    }

    openHomePage() {
        this.navController.navigateRoot(['home']);
    }
}
