import {Component, Input, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {Store} from '@ngrx/store';
import {Logout} from '@app/ngrx-store/authentication/authentication.actions';
import * as fromStore from '@app/ngrx-store';
import {Reactive} from '@app/common/cdk/reactive';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {ActivatedRoute} from '@angular/router';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html'
})
export class SideMenuComponent extends Reactive implements OnInit {

  @Input()
  module: 'company' | 'client' | 'worker' = 'client';

  isLoggedIn: boolean = false;
  isDesktop: boolean = false;

  appPages = [];
  selectedIndex: number;

  standard = [
    {
      title: 'Główna',
      url: '/',
      icon: 'planet-outline'
    },
    // {
    //   title: 'Zgłoś usterkę',
    //   url: '/report',
    //   icon: 'bug-outline'
    // },
    // {
    //   title: 'Polityka prywatności',
      //   url: '/privacy-policy',
      //   icon: 'document-text-outline'
    // }
  ];
  private uuid: string;
  public profile: ProfileInterface;

  constructor(
      private readonly store: Store,
      private readonly activatedRoute: ActivatedRoute,
      private readonly menuController: MenuController,
      private readonly alertPromptService: AlertPromptService,
      private readonly navController: NavController
  ) {
    super();
  }

  ngOnInit() {

    this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {
      if (params['uuid']) {
        this.uuid = params['uuid'];
      }
      this.updateMenuList();
    });

    this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((result: boolean) => {

      this.isLoggedIn = result;

      this.updateMenuList();

    });

    this.store.select(fromStore.selectProfileData).pipe(this.takeUntil()).subscribe((result: ProfileInterface) => {

      this.profile = result;

      if (result) {

        if (result.companies && result.companies.length > 0) {

          this.updateMenuList();

        }

      }

    });

  }

  logout() {
    this.alertPromptService.presentAlert('Czy na pewno chcesz wylogować się?', {
      buttons: [
        {
          text: 'Nie',
          handler: () => {
          }
        },
        {
          text: 'Tak',
          handler: () => {
            this.store.dispatch(new Logout());
          }
        }
      ]
    });
  }

  private updateMenuList() {

    switch (this.module) {
      case 'client':

        if (this.isLoggedIn) {


          this.appPages = [
            ...this.standard,
            // {
            //   title: 'Rezerwacje',
            //   url: '/reservation',
            //   icon: 'calendar-outline'
            // },
            {
              title: 'Profil & Rezerwacje',
              url: '/profile',
              icon: 'person-circle'
            },
          ];

        } else {

          if (this.isDesktop) {

            this.appPages = [
              ...this.standard
            ];

          } else {

            this.appPages = [
              ...this.standard,
              {
                title: 'Login',
                url: '/auth/login',
                icon: 'log-in-outline'
              },
              {
                title: 'Rejestracja',
                url: '/auth/registration',
                icon: 'person-add-outline'
              }
            ];

          }

        }

        break;
    }

  }

  back() {

    this.navController.pop();

  }

    closeMenu() {
        this.menuController.toggle('side-menu');
    }
}
