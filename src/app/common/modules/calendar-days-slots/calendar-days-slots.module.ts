import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {CalendarDaysSlotsComponent} from '@app/common/modules/calendar-days-slots/calendar-days-slots.component';



@NgModule({
  declarations: [CalendarDaysSlotsComponent],
  exports: [
    CalendarDaysSlotsComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class CalendarDaysSlotsModule { }
