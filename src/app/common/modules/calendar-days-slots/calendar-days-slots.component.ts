import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import * as moment from 'moment';
import {DaySlotsInterface} from '@app/common/interfaces/modules/client/reservation/day-slots.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {Store} from '@ngrx/store';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {ParamsCalendar} from '@app/ngrx-store/pws/pws.actions';
import * as fromStore from '@app/ngrx-store';
import {selectPwsCalendar, selectPwsParamsCalendar} from '@app/ngrx-store';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {Reactive} from '@app/common/cdk/reactive';
import {ModalController} from '@ionic/angular';
import {ReservationService} from '@app/common/services/api/modules/client/reservation/reservation.service';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

export interface CalendarDate {
    mDate: moment.Moment;
    selected?: boolean;
    today?: boolean;
    badge?: number;
}

@Component({
    selector: 'app-calendar-days-slots',
    templateUrl: './calendar-days-slots.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarDaysSlotsComponent extends Reactive implements OnInit {

    @Input()
    daysCount: number = 7;

    @Input()
    widthDay: number = 50;

    @Input()
    item: SearchItemInterface;

    @Input()
    inModal: boolean = false;

    @Input()
    returnObject: boolean = false;

    public slotStatus = {
        enabled: 1,
        disabled: 0
    };

    initialize: boolean = false;

    days: DaySlotsInterface[];

    dayConfig: {
        width: string,
    } = {
        width: `100%`
    };

    public slotsIsEmpty: boolean = true;
    private firstGenerateDays: boolean = true;

    public currentDate: moment.Moment;
    public namesOfDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    public weeks: Array<CalendarDate[]> = [];

    private content;
    private prevBtn;
    private nextBtn;
    private loader;
    private daysWithSlots;
    private calendar: SearchItemInterface;
    private isLoggedIn: boolean = false;

    private static isToday(date: moment.Moment): boolean {
        return moment().isSame(moment(date), 'day');
    }

    private static isTomorrow(date: moment.Moment): boolean {
        return CalendarDaysSlotsComponent.isToday(date.clone().subtract(1));
    }

    constructor(
        private readonly store: Store,
        private readonly modalController: ModalController,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly elementRef: ElementRef,
        private readonly reservationService: ReservationService,
        private readonly authenticationService: AuthenticationService,
        private readonly renderer2: Renderer2,
    ) {
        super();
    }

    ngOnInit() {

        moment.locale('pl');
        this.namesOfDays = moment.weekdaysShort(true);
        this.currentDate = moment().locale('pl');

        const startDate = {
            start: this.currentDate.format('YYYY-MM-DD'),
            end: moment(this.currentDate).add(this.daysCount, 'days').format('YYYY-MM-DD'),
        };

        if (this.returnObject) {

            const newParams = {
                itemId: this.item.id,
                ...startDate
            };
            this.store.dispatch(new fromPws.ParamsCalendar(newParams));
            this.refreshContent();

        }

        this.store.select(selectPwsParamsCalendar).pipe(this.takeUntil()).subscribe((searchParams: SearchInterface) => {

            if (searchParams && searchParams.itemId) {

                if (!isNotNullOrUndefined(searchParams?.start) || !isNotNullOrUndefined(searchParams?.end)) {

                    const params = {
                        ...searchParams,
                        ...startDate
                    };

                    this.store.dispatch(new ParamsCalendar(params));

                }

                if (searchParams.start && searchParams.end) {

                    this.refreshContent();

                }

            }

        });

        this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((isLoggedIn: boolean) => {

            this.isLoggedIn = isLoggedIn;

        });

        this.store.select(selectPwsCalendar).pipe(this.takeUntil()).subscribe((calendar: SearchItemInterface) => {

            if (calendar) {

                this.calendar = calendar;

                if (this.firstGenerateDays) {

                    setTimeout(() => {

                        this.content = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content');

                        this.daysCount = Math.floor((this.content.clientWidth - 36) / this.widthDay);

                        this.widthDay = Math.floor((this.content.clientWidth - (36)) / this.daysCount);

                        this.dayConfig.width = `${this.widthDay}px`;

                        this.prevBtn = this.elementRef.nativeElement.querySelector('.calendar-days-slots__header__prev-day');
                        this.nextBtn = this.elementRef.nativeElement.querySelector('.calendar-days-slots__header__next-day');
                        this.loader = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content__loader');
                        this.daysWithSlots = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content__days-with-slots');
                        this.checkBtnNextPrev();

                        this.generateCalendar();

                    }, 250);

                } else {

                    this.generateCalendar();

                }

            }

        });

    }

    /**
     *
     * @param day
     * @param slot
     */
    async selectSlot(day: DaySlotsInterface, slot: SlotInterface) {

        if (this.returnObject) {

            if (this.inModal) {

                this.dismissModal({
                    day,
                    slot,
                });

            }

        } else {

            if (this.isLoggedIn) {

                await this.reservationService.create({
                    day,
                    slot
                }, this.item);

            } else {

                await this.authenticationService.communicateForGuest();

            }

        }

    }

    // public isDisabledMonth(currentDate): boolean {
    //   const today = moment();
    //   return moment(currentDate).isBefore(today, 'months');
    // }


    private buildSlots(currentDate: moment.Moment) {

        const slots = [];

        const disabledList = this.calendar ? this.calendar.disabled.filter((disabled) => {
            return disabled.date === currentDate.format('YYYY-MM-DD');
        }) : [];

        const workTime = this.item.workTime.find((wt) => wt.weekday === (currentDate.day() === 0 ? 7 : currentDate.day()));

        if (workTime) {

            const startTime = currentDate.clone();
            const endTime = currentDate.clone();

            const interval = this.getInterval(workTime.break);

            startTime.set({
                hours: Number(workTime.startAt.split(':')[0]),
                minutes: Number(workTime.startAt.split(':')[1]),
                seconds: 0
            });

            // TODO current_time/now() need add for start_time
            // TODO check if start_time is old for now() and end_time
            // TODO add check for min_reservation_make_time/date from api

            endTime.set({
                hours: Number(workTime.endAt.split(':')[0]),
                minutes: Number(workTime.endAt.split(':')[1]),
                seconds: 0
            });

            const buildSlots: boolean = !CalendarDaysSlotsComponent.isToday(currentDate);

            if (!buildSlots) {

                // TODO check if today when check time and if time is after start_at need
                //  build slots without slots who can be build from start_at

                // buildSlots = this.checkDateAndTime(new Date(), startTime, {
                //   checkToDay: true,
                //   checkAfter: true
                // });

            }

            if (buildSlots) {

                while (endTime.isAfter(startTime)) {

                    const slot = {
                        start: startTime.format('HH:mm'),
                        end: null,
                        status: 0
                    };

                    startTime.add(interval.getHours(), 'hours');
                    startTime.add(interval.getMinutes(), 'minutes');

                    slot.end = startTime.format('HH:mm');

                    if (disabledList.length === 0 || disabledList.findIndex((disabled) => {
                        return slot.start === `${disabled.startAt.split(':')[0]}:${disabled.startAt.split(':')[1]}`;
                    }) === -1) {

                        slot.status = 1;
                        slots.push(slot);

                    }

                }

            }


        }

        return slots;
    }

    private getInterval(breakInterval: string): Date {

        const interval = new Date();

        interval.setHours(
            Number(this.item.service.interval.split(':')[0]),
            Number(this.item.service.interval.split(':')[1]),
            0,
            0
        );

        interval.setHours(
            interval.getHours() + Number(breakInterval.split(':')[0]),
            interval.getMinutes() + Number(breakInterval.split(':')[1])
        );

        return interval;

    }

    private checkBtnNextPrev() {

        if (moment(moment(this.currentDate).add(-this.daysCount, 'day')).isBefore(moment(), 'day')) {

            this.renderer2.setAttribute(this.prevBtn, 'disabled', 'true');

        } else {

            this.renderer2.removeAttribute(this.prevBtn, 'disabled');

        }

    }

    nextDays() {

        // TODO max reservation date check

        this.renderer2.removeClass(this.daysWithSlots, 'show');
        this.renderer2.addClass(this.loader, 'show');
        this.currentDate = moment(this.currentDate).add(this.daysCount, 'days');

        const params = {
            start: this.currentDate.format('YYYY-MM-DD'),
            end: moment(this.currentDate).add(this.daysCount, 'days').format('YYYY-MM-DD'),
        };

        this.store.dispatch(new ParamsCalendar(params));

        this.checkBtnNextPrev();

    }

    prevDays() {

        // TODO min reservation date check


        this.renderer2.removeClass(this.daysWithSlots, 'show');
        this.renderer2.addClass(this.loader, 'show');
        this.currentDate = moment(this.currentDate).subtract(this.daysCount, 'days');

        const params = {
            start: this.currentDate.format('YYYY-MM-DD'),
            end: moment(this.currentDate).add(this.daysCount, 'days').format('YYYY-MM-DD'),
        };

        this.store.dispatch(new ParamsCalendar(params));

        this.checkBtnNextPrev();

    }

    dismissModal(data = null) {
        return this.modalController.dismiss(data, 'cancel');
    }

    /**
     *
     * trackByFn
     */

    /**
     *
     * @param item
     */
    trackByFn(item: any) {
        return item && item.start;
    }

    /**
     *
     * @param item
     */
    trackByDayFn(item: any) {
        return item && item.start;
    }

    refreshContent($event: any = null) {

        if ($event) {
            $event.target.complete();
        }

        this.store.dispatch(new fromPws.RefreshCalendar());

    }

    private generateCalendar(): void {

        this.days = [];
        this.slotsIsEmpty = true;
        const weekStart = this.currentDate.startOf('day');

        for (let i = 0; i <= this.daysCount - 1; i++) {

            const day = moment(weekStart).add(i, 'day');
            const dayName = CalendarDaysSlotsComponent.isToday(day) ? '<strong>DZIŚ</strong>' : CalendarDaysSlotsComponent.isTomorrow(day) ? 'Jutro' : day.format('ddd');
            const dayNumber = day.format('DD.MM');

            this.days.push({
                date: day,
                dayName,
                dayNumber,
                slots: this.buildSlots(day)
            });

        }

        if (this.slotsIsEmpty) {

            for (const day of this.days) {

                if (day.slots.length > 0) {

                    this.slotsIsEmpty = false;
                    break;

                }

            }

        }

        if (this.firstGenerateDays) {

            if (this.slotsIsEmpty) {

                // TODO build next week

            } else {

                this.firstGenerateDays = false;

            }

        }

        this.renderer2.removeClass(this.loader, 'show');
        this.renderer2.addClass(this.daysWithSlots, 'show');

        this.changeDetectorRef.detectChanges();

    }
}
