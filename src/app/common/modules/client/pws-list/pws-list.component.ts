import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {SearchItemListInterface} from '@app/common/interfaces/modules/client/pws/search-item-list.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';
import {ReservationService} from '@app/common/services/api/modules/client/reservation/reservation.service';
import {Reactive} from '@app/common/cdk/reactive';
import * as fromStore from '@app/ngrx-store';
import {selectPwsList, selectPwsParams} from '@app/ngrx-store';
import {Store} from '@ngrx/store';
import {Tools} from '@app/common/tools/tools';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {UniversalSelectComponent} from '@app/common/modules/client/popover/universal-select/universal-select.component';
import {environment} from '@envi/environment';
import {IonInfiniteScroll, NavController, PopoverController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {DaySlotsInterface} from '@app/common/interfaces/modules/client/reservation/day-slots.interface';

@Component({
    selector: 'app-client-pws-list-component',
    templateUrl: './pws-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PwsListComponent extends Reactive implements OnInit{

    @ViewChild(IonInfiniteScroll)
    infiniteScroll: IonInfiniteScroll;

    public readonly pwsList$: ReplaySubject<SearchItemListInterface> = new ReplaySubject<SearchItemListInterface>();

    private isLoggedIn: boolean = false;
    private searchListBuffer: SearchItemListInterface;
    public searchParams: SearchInterface;
    public selectedOrderBy = environment.filters[0];

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly authenticationService: AuthenticationService,
        private readonly popoverController: PopoverController,
        private readonly navController: NavController,
        private readonly navService: NavService,
        private readonly store: Store,
        private readonly reservationService: ReservationService,
    ) {
        super();
    }

    ngOnInit() {

        this.store.select(selectPwsParams).pipe(this.takeUntil()).subscribe((searchParams: SearchInterface) => {

            this.searchParams = searchParams;

            if (searchParams.category) {

                if (searchParams.search) {
                    this.searchPwsLIst(searchParams.search);
                } else {

                    if (!this.searchListBuffer) {

                        this.refreshContent();


                    }
                }

            } else {

                this.store.dispatch(new fromPws.LoadListSuccess(null));

            }

        });

        this.store.select(selectPwsList).pipe(this.takeUntil()).subscribe((searchList) => {

            this.searchListBuffer = Tools.copyObject(searchList);
            this.pwsList$.next(Tools.copyObject(this.searchListBuffer));

            this.changeDetectorRef.detectChanges();

        });

        this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((isLoggedIn: boolean) => {

            this.isLoggedIn = isLoggedIn;
            this.changeDetectorRef.detectChanges();

        });

    }

    refreshContent() {
        this.pwsList$.next(null);
        this.store.dispatch(new fromPws.RefreshList());
    }

    trackByFn(item: SearchItemInterface) {
        return item && item.id;
    }

    clearSelectedCategory() {
        this.navController.navigateRoot(['/']).then(() => {

            this.store.dispatch(new fromPws.Params({
                category: null,
            }));

        });
    }

    async selectSlot($event: { day: DaySlotsInterface; slot: SlotInterface }, item: SearchItemInterface) {

        if (this.isLoggedIn) {

            await this.reservationService.create($event, item);

        } else {

            await this.authenticationService.communicateForGuest();

        }

    }

    private searchPwsLIst(searched: string) {

        let item = Tools.copyObject(this.searchListBuffer);

        if (item) {

            if (searched && searched.length > 0) {

                const returnFindChildren = (children: SearchItemInterface[], search: string) => {

                    const newChildren = [];

                    if (children && children.length > 0) {

                        for (const child of children) {

                            if (child.service.name.toLowerCase().search(search) > -1) {

                                newChildren.push(child);

                            } else if (child.worker.firstName.toLowerCase().search(search) > -1 || child.worker.lastName.toLowerCase().search(search) > -1) {

                                newChildren.push(child);

                            } else if (child.worker.company.name.toLowerCase().search(search) > -1) {

                                newChildren.push(child);

                            }

                        }

                    }

                    return newChildren;

                };

                item = {
                    models: returnFindChildren(item.models, searched),
                    total: item.models.length
                };


            }

            this.pwsList$.next(item);

        }

    }

    async openFilter(ev) {

        const popover = await this.popoverController.create({
            component: UniversalSelectComponent,
            event: ev,
            mode: 'md',
            componentProps: {
                options: environment.filters,
            },
            translucent: true
        });

        popover.onDidDismiss().then((result) => {
            if (result.data) {
                this.selectedOrderBy = result.data;
                this.store.dispatch(new fromPws.Params({
                    ...this.searchParams,
                    orderBy: this.selectedOrderBy.value['by'],
                    sort: this.selectedOrderBy.value['dir']
                }));
                this.refreshContent();
            }
        });

        return await popover.present();

    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    loadData() {
        this.store.dispatch(new fromPws.NextListPage());
    }

    toggleInfiniteScroll() {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }

}
