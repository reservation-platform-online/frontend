import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PwsListComponent} from '@app/common/modules/client/pws-list/pws-list.component';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {IonicModule} from '@ionic/angular';
import {WorkerModule} from '@app/common/modules/client/worker/worker.module';



@NgModule({
  declarations: [PwsListComponent],
  exports: [
    PwsListComponent
  ],
  imports: [
    CommonModule,
    DefaultContainerModule,
    IonicModule,
    WorkerModule
  ]
})
export class PwsListModule { }
