import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReservationFormComponent} from '@app/common/modules/client/reservation-form/reservation-form.component';
import {IonicModule} from '@ionic/angular';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {RouterModule} from '@angular/router';
import {InputModule} from '@app/common/ui/input/input.module';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        ReservationFormComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ReservationStatusModule,
        StringTimeConvertModule,
        RouterModule,
        InputModule,
        ReactiveFormsModule
    ]
})
export class ReservationFormModule { }
