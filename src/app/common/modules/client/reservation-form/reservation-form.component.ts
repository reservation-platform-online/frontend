import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {InitCreateInterface} from '@app/common/interfaces/modules/client/reservation/init-create.interface';
import {ServiceListInterface} from '@app/common/interfaces/modules/client/service/service-list.interface';
import {Store} from '@ngrx/store';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ModalController, NavController} from '@ionic/angular';
import {selectReservationCreateInit, selectServiceList} from '@app/ngrx-store';
import * as fromReservation from '@app/ngrx-store/reservation/reservation.actions';
import * as ReservationActions from '@app/ngrx-store/reservation/reservation.actions';
import {ReservationInterface} from '@app/common/interfaces/modules/client/reservation/reservation.interface';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {ReservationApiService} from '@app/common/services/api/modules/client/reservation/reservation-api.service';
import {ReservationApiService as ReservationApiServiceCompany} from '@app/common/services/api/modules/company/reservation/reservation-api.service';
import {RESERVATION_STATUS} from '@app/common/services/api/modules/client/reservation/reservation.status';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';

@Component({
    selector: 'app-reservation-form',
    templateUrl: './reservation-form.component.html',
    styleUrls: ['./reservation-form.component.scss'],
})
export class ReservationFormComponent extends Reactive implements OnInit {

    @Input()
    public reservation: ReservationInterface;

    @Input()
    public isBusiness: boolean = false;

    public reservationStatus = RESERVATION_STATUS;

    public initCreate: InitCreateInterface;
    private serviceList: ServiceListInterface;

    public isView: boolean = false;

    public form: FormGroup;

    constructor(
        private readonly store: Store,
        private readonly loadingController: LoadingService,
        private readonly modalController: ModalController,
        private readonly navController: NavController,
        private readonly formBuilder: FormBuilder,
        private readonly inAppBrowser: InAppBrowser,
        private readonly reservationApiServiceCompany: ReservationApiServiceCompany,
        private readonly reservationApiService: ReservationApiService,
        private readonly alertPromptService: AlertPromptService,
    ) {
        super();

        this.form = this.formBuilder.group({
            note: [null, [Validators.required, Validators.minLength(10)]],
            withoutNote: [null],
        });

        this.form.controls['withoutNote'].valueChanges.pipe(this.takeUntil()).subscribe((value) => {
            if (value) {
                this.form.controls['note'].disable();
            } else {
                this.form.controls['note'].enable();
            }
        });

    }

    ngOnInit() {

        this.reservationApiService.onReservationIsCreated().pipe(this.takeUntil()).subscribe((result) => {

            if (result) {

                this.modalController.dismiss(null, 'success').then(() => {
                    this.modalController.dismiss(null, null, 'calendar-select-slot');
                    this.navController.navigateRoot(['profile', 'active']).then(() => {
                        console.log('Load reservation');
                        this.reservationApiService.clearReservationIsCreated();
                        this.store.dispatch(new ReservationActions.Load());
                    });
                });

            }

        });

        if (this.reservation) {
            this.isView = true;
        }

        this.store.select(selectReservationCreateInit).pipe(this.takeUntil()).subscribe((initCreate: InitCreateInterface) => {

            this.initCreate = initCreate;

        });

        this.store.select(selectServiceList).pipe(this.takeUntil()).subscribe((serviceList: ServiceListInterface) => {

            this.serviceList = serviceList;

        });

    }

    getNameOfService(id: string) {

        if (!this.serviceList) {
            return '-';
        }

        return this.serviceList.models.find((service) => service.id === id).name;

    }

    refreshContent(event): void {
        event.target.complete();
    }

    cancel() {

        this.alertPromptService.presentAlert('Czy na pewno chcesz anulować?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.store.dispatch(new fromReservation.Cancel({
                            id: this.reservation.id
                        }));
                    }
                }
            ]
        });

    }

    create() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const form = this.form.value;
            delete form.isSubmitted;
            this.store.dispatch(new fromReservation.Create({
                date: this.initCreate.date,
                end: this.initCreate.end,
                pwsId: this.initCreate.item.id,
                start: this.initCreate.start,
                ...(form.withoutNote ? {} : {
                    note: form.note
                })
            }));
            this.form.markAsUntouched();
            this.form.updateValueAndValidity();
        }
    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    canCanceled() {
        return [RESERVATION_STATUS.STATUS_CONFIRMED, RESERVATION_STATUS.STATUS_ENABLED].includes(this.reservation.status);
    }

    /**
     * Reservation
     */


    /**
     *
     * @param reservation
     */
    confirmReservation(reservation: ReservationInterface) {


        this.alertPromptService.presentAlert('Czy na pewno chcesz <strong>potwierdzić</strong>?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.reservationApiServiceCompany.putItem({
                            status: 2
                        }, reservation.id).pipe(this.takeUntil()).subscribe((result) => {
                            if (result) {
                                this.reservation.status = 2;
                                // this.refresh.emit();
                                this.dismissModal();
                            }
                        });
                    }
                }
            ]
        });
    }

    /**
     *
     * @param reservation
     */
    cancelReservation(reservation: ReservationInterface) {

        this.alertPromptService.presentAlert('Czy na pewno chcesz anulować?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.reservationApiServiceCompany.putItem({
                            status: 4
                        }, reservation.id).pipe(this.takeUntil()).subscribe((result) => {
                            if (result) {
                                this.reservation.status = 4;
                                // this.refresh.emit();
                                this.dismissModal();
                            }
                        });
                    }
                }
            ]
        });
    }

    /**
     *
     * @param reservation
     */
    resetReservation(reservation: ReservationInterface) {

        this.alertPromptService.presentAlert('Czy na pewno chcesz ponowić?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.reservationApiServiceCompany.putItem({
                            status: 1
                        }, reservation.id).pipe(this.takeUntil()).subscribe((result) => {
                            if (result) {
                                this.reservation.status = 1;
                                // this.refresh.emit();
                                this.dismissModal();
                            }
                        });
                    }
                }
            ]
        });
    }

    /**
     *
     * @param reservation
     */
    finishReservation(reservation: ReservationInterface) {

        this.alertPromptService.presentAlert('Czy na pewno chcesz zakończyć?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.reservationApiServiceCompany.putItem({
                            status: 3
                        }, reservation.id).pipe(this.takeUntil()).subscribe((result) => {
                            if (result) {
                                this.reservation.status = 3;
                                // this.refresh.emit();
                                this.dismissModal();
                            }
                        });
                    }
                }
            ]
        });
    }

}
