import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchComponent} from '@app/common/modules/client/search/search.component';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [SearchComponent],
  exports: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class SearchModule { }
