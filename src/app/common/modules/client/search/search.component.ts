import {Component, OnInit} from '@angular/core';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Store} from '@ngrx/store';
import {selectPwsParams} from '@app/ngrx-store';
import {Reactive} from '@app/common/cdk/reactive';

@Component({
  selector: 'app-client-search-component',
  templateUrl: './search.component.html'
})
export class SearchComponent extends Reactive implements OnInit {

  searched: string = '';

  constructor(
      private readonly store: Store
  ) {
    super();
  }

  ngOnInit() {

    this.store.select(selectPwsParams).pipe(this.takeUntil()).subscribe((params) => {
      this.searched = params['search'];
    });

  }

  clearSelectedCategory() {
    this.store.dispatch(new fromPws.Params({
      search: null,
    }));
  }

  search($event: any) {

    if ($event) {

      this.searched = $event.target.value.toLowerCase();

      this.store.dispatch(new fromPws.Params({
        search: this.searched,
      }));

    }

  }
}
