import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {CommunicateForGuestComponent} from '@app/common/modules/client/communicate-for-guest/communicate-for-guest.component';
import {LoginFormModule} from '@app/common/modules/client/login-form/login-form.module';



@NgModule({
  declarations: [
      CommunicateForGuestComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        ReservationStatusModule,
        StringTimeConvertModule,
        LoginFormModule
    ]
})
export class CommunicateForGuestModule { }
