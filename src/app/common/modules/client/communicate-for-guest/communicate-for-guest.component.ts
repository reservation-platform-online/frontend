import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {Store} from '@ngrx/store';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {AlertController, ModalController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './communicate-for-guest.component.html',
  styleUrls: ['./communicate-for-guest.component.scss'],
})
export class CommunicateForGuestComponent extends Reactive implements OnInit {

  constructor(private readonly store: Store,
              private readonly loadingController: LoadingService,
              private readonly navController: NavController,
              private readonly modalController: ModalController,
  ) {
    super();
  }

  ngOnInit() {

  }

  async dismissModal() {
    await this.modalController.dismiss(null, 'cancel');
  }

  openLoginPage() {
    this.dismissModal().then(() => {
      this.navController.navigateForward(['auth', 'login']);
    });
  }

  openRegistrationPage() {
    this.dismissModal().then(() => {
      this.navController.navigateForward(['client', 'auth', 'registration']);
    });
  }
}
