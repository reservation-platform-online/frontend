import {Component, OnInit} from '@angular/core';
import {NavService} from '@app/common/services/util/nav.service';
import {Observable} from 'rxjs';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html'
})
export class BannerComponent implements OnInit {
  isNative: boolean = false;

  constructor(
      private readonly navService: NavService,
      private readonly screenSizeService: ScreenSizeService
  ) {
    this.isNative = navService.isNative;
  }

  get onIsMobile$(): Observable<boolean> {
    return this.screenSizeService.isMobileView$;
  }

  ngOnInit() {
  }

}
