import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    Input,
    OnChanges,
    OnInit,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {HeaderService} from '@app/common/services/header/header.service';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {Store} from '@ngrx/store';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {selectCategoryList} from '@app/ngrx-store';
import {Tools} from '@app/common/tools/tools';
import {CategoryModalComponent} from '@app/common/modal/category-modal/category-modal.component';

@Component({
    selector: 'app-client-company-form-service',
    templateUrl: './service.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceComponent extends Reactive implements OnInit, OnChanges {

    @Input()
    service: ServiceInterface;

    public selectedSegment: 'basic' | 'advanced' = 'basic';

    public isEdit: boolean = false;

    public form: FormGroup;

    public submitted: boolean = false;

    private categories: CategoryListInterface;

    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Wpisz opis swojej usługi...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        uploadWithCredentials: false,
        sanitize: false,
        outline: false,
        toolbarPosition: 'top',
        toolbarHiddenButtons: [
            [
                'underline',
                'strikeThrough',
                'subscript',
                'superscript',
                'justifyLeft',
                'justifyCenter',
                'justifyRight',
                'justifyFull',
                'indent',
                'outdent',
                'insertUnorderedList',
                'insertOrderedList',
                'heading',
                'fontName'
            ],
            [
                'fontSize',
                'customClasses',
                'insertImage',
                'insertVideo',
                'insertHorizontalRule'
            ]
        ]
    };

    //
    // Start: Image cropper
    //

    @ViewChild('bannerInput')
    bannerInputVar: ElementRef;

    imageChangedEvent: any = null;
    croppedImage: any = null;

    //
    // Finish: Image cropper
    //

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly headerService: HeaderService,
        private readonly loadingService: LoadingService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
    ) {
        super();
        this.headerService.setTitle('Usługi');

    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.service) {
            this.isEdit = true;
        }
    }

    async dismissModal(data = null) {
        await this.modalController.dismiss(data, 'cancel');
    }

    segmentChanged($event: any) {
        this.selectedSegment = $event.detail.value;
    }

    //
    // Start: Image cropper
    //

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
        this.form.controls['image'].setValue(true);
    }

    //
    // Finish: Image cropper
    //

    ngOnInit() {

        this.loadingService.presentLoadingWithOptions({
            id: 'LoadingCompanyServiceForm'
        }).then(() => {

            this.store.select(selectCategoryList)
                .pipe(
                    this.takeUntil()
                )
                .subscribe((categories: CategoryListInterface) => {
                    this.categories = categories;
                });

            this.initForm();

        });
    }

    public openFileSelect(): void {

        // @ts-ignore
        document.querySelector('.select-file-input input').click();
        // todo write for native

    }

    public create(): void {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const value = Tools.clearObject(this.form.value);
            delete value.isSubmitted;
            this.form.markAsUntouched();
            this.form.updateValueAndValidity();
            this.dismissModal({
                ...value,
                image: this.croppedImage
            });
        } else {
            // const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            // firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

    }

    private checkCategoryField(): void {

        if (this.form.controls['categoryId'] && this.form.controls['categoryId'].value && this.categories) {

            const category = this.categories.models.find((item) => item.id === Number(this.form.controls['categoryId'].value));
            this.form.controls['categoryId'].setValue(category.id);
            this.form.controls['categoryName'].setValue(category.name);

        }

    }

    private initForm(): void {

        if (this.isEdit) {

            this.form = this.formBuilder.group({
                categoryName: ['', [Validators.required]],
                categoryId: [this.service.categoryId, [Validators.required]],
                name: [this.service.name, [Validators.required]],
                description: [this.service.description],
                price: [this.service.price, [Validators.required]],
                priceTo: [this.service.priceTo],
                discount: [this.service.discount],
                image: [true, [Validators.required]],
                discountIsPercent: [this.service.discountIsPercent],
                earliestReservationTime: [this.service.earliestReservationTime],
                earliestReservationDate: [this.service.earliestReservationDate],
                lastReservationDate: [this.service.lastReservationDate],
                paymentIsImportant: [this.service.paymentIsImportant],
                interval: [this.service.interval],
                timerTimeForPay: [this.service.timerTimeForPay]
            });

        } else {

            this.form = this.formBuilder.group({
                categoryName: [null, [Validators.required]],
                categoryId: [null, [Validators.required]],
                name: [null, [Validators.required]],
                description: [null],
                price: [null, [Validators.required]],
                priceTo: [null],
                discount: [null],
                image: [null, [Validators.required]],
                discountIsPercent: [null],
                earliestReservationTime: ['01:00:00'],
                earliestReservationDate: [null],
                lastReservationDate: [null],
                paymentIsImportant: [1],
                interval: ['01:00:00'],
                timerTimeForPay: ['00:15:00']
            });

        }

        this.checkCategoryField();

        if (!this.isEdit) {

            // this.openModalCategory();

        }

        this.loadingService.dismissLoader('LoadingCompanyServiceForm');

        this.changeDetectorRef.detectChanges();

    }

    public openModalCategory(): void {

        this.modalController.create({
            component: CategoryModalComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result && result.data) {
                    this.form.get('categoryName').setValue(result.data.name);
                    this.form.get('categoryId').setValue(result.data.id);
                }
            });
        });

    }

    public clearNewImg(): void {

        this.croppedImage = null;
        this.imageChangedEvent = null;
        if (this.bannerInputVar) {
            this.bannerInputVar.nativeElement.value = '';

        }

    }

}
