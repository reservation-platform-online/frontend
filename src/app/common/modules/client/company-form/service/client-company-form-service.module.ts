import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServiceComponent} from '@app/common/modules/client/company-form/service/service.component';
import {ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {InputModule} from '@app/common/ui/input/input.module';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ImgModule} from '@app/common/ui/img/img.module';


@NgModule({
  declarations: [ServiceComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        IonicModule,
        InputModule,
        AngularEditorModule,
        ImageCropperModule,
        ImgModule
    ]
})
export class ClientCompanyFormServiceModule { }
