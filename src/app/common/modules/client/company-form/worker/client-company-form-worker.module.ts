import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkerComponent} from '@app/common/modules/client/company-form/worker/worker.component';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {InputModule} from '@app/common/ui/input/input.module';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ImgModule} from '@app/common/ui/img/img.module';


@NgModule({
    declarations: [WorkerComponent],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        InputModule,
        ImageCropperModule,
        ImgModule
    ]
})
export class ClientCompanyFormWorkerModule { }
