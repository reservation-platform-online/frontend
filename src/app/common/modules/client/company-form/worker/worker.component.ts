import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';
import {HeaderService} from '@app/common/services/header/header.service';
import {NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {WorkerApiService} from '@app/common/services/api/modules/company/worker/worker-api.service';
import {Store} from '@ngrx/store';
import {ActivatedRoute} from '@angular/router';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {selectCategoryList} from '@app/ngrx-store';
import {Tools} from '@app/common/tools/tools';
import {ConvertTool} from '@app/common/tools/convert.tool';

@Component({
  selector: 'app-client-company-form-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss'],
})
export class WorkerComponent extends Reactive implements OnInit {

  public isEdit: boolean = false;
  public uuid: string = null;

  public form: FormGroup;

  public submitted: boolean = false;
  private categories: CategoryListInterface;
  public worker: WorkerInterface;

  //
  // Start: Image cropper
  //

  @ViewChild('bannerInput')
  bannerInputVar: ElementRef;

  imageChangedEvent: any = null;
  croppedImage: any = null;

  //
  // Finish: Image cropper
  //

  constructor(
      private readonly formBuilder: FormBuilder,
      private readonly headerService: HeaderService,
      private readonly navController: NavController,
      private readonly navService: NavService,
      private readonly workerApiService: WorkerApiService,
      private readonly store: Store,
      private readonly elementRef: ElementRef,
      private readonly activatedRoute: ActivatedRoute
  ) {
    super();
    this.headerService.setTitle('Pracownik');

    this.initForm();

  }

  //
  // Start: Image cropper
  //

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // TODO request for save logo
    // show message
  }

  openFileSelect() {

    // @ts-ignore
    document.querySelector('.select-file-input input').click();
    // todo write for native

  }

  //
  // Finish: Image cropper
  //

  ngOnInit() {

    this.store.select(selectCategoryList).pipe(this.takeUntil()).subscribe((categories: CategoryListInterface) => {
      this.categories = categories;
    });

    this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {

      if (params['uuid']) {

        this.uuid = params['uuid'];
        this.isEdit = true;

        this.workerApiService.getItem(this.uuid).pipe(this.takeUntil()).subscribe((service: WorkerInterface) => {

          if (service) {

            this.worker = service;
            this.initForm();

          }

        });

      }

    });
  }

  create() {
    this.form.markAsTouched();
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      const value = Tools.clearObject(this.form.value);
      delete value.isSubmitted;
      if (this.isEdit) {
        this.workerApiService
            .putEdit(value, this.uuid)
            .pipe(
                this.takeUntil()
            )
            .subscribe((result) => {
              this.form.markAsUntouched();
              this.form.updateValueAndValidity();
              if (result) {
                this.saveUploadImage(this.uuid);
              }
            });
      } else {
        this.workerApiService
            .postCreate(value)
            .pipe(
                this.takeUntil()
            )
            .subscribe((result: string) => {
              this.form.markAsUntouched();
              this.form.updateValueAndValidity();
              if (result) {
                this.saveUploadImage(result);
              }
            });
      }
    } else {
      const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
      firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
    }

  }

  back() {
    if (this.isEdit) {
      this.navController.navigateRoot(`${this.navService.getPrevUrl({
        cut: 2
      })}/view/${this.uuid}`);
    } else {

      this.navService.back();
    }
  }

  private checkCategoryField() {

    if (this.form.controls['categoryId'] && this.form.controls['categoryId'].value && this.categories) {

      const category = this.categories.models.find((item) => item.id === Number(this.form.controls['categoryId'].value));
      this.form.controls['categoryId'].setValue(category.id);
      this.form.controls['categoryName'].setValue(category.name);

    }

  }

  private initForm() {

    if (this.isEdit) {

      this.form = this.formBuilder.group({
        firstName: [this.worker.firstName, [Validators.required]],
        lastName: [this.worker.lastName, [Validators.required]],
        position: [this.worker.position],
        userEmail: [''],
      });

    } else {

      this.form = this.formBuilder.group({
        firstName: [null, [Validators.required]],
        lastName: [null, [Validators.required]],
        position: [null],
        userEmail: [null],
      });

    }

    this.checkCategoryField();

  }

  private saveUploadImage(uuid: string) {
    if (this.croppedImage) {

      this.workerApiService.saveImage(uuid, ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), uuid)).subscribe(() => {

        this.clearNewImg();


        if (this.isEdit) {

          this.back();

        } else {

          this.navController.navigateRoot(`${this.navService.getPrevUrl()}/view/${uuid}`);
        }

      });
    } else {

      if (this.isEdit) {

        this.back();

      } else {

        this.navController.navigateRoot(`${this.navService.getPrevUrl()}/view/${uuid}`);
      }
    }

  }

  clearNewImg() {

    this.croppedImage = null;
    this.imageChangedEvent = null;
    if (this.bannerInputVar) {
      this.bannerInputVar.nativeElement.value = '';

    }

  }

}
