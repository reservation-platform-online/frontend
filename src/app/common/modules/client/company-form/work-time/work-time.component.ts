import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';
import {WorkTimeInterface} from '@app/common/interfaces/modules/company/work-time/work-time.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WorkTimeApiService} from '@app/common/services/api/modules/company/work-time/work-time-api.service';
import {ModalController} from '@ionic/angular';
import {WorkerApiService} from '@app/common/services/api/modules/company/worker/worker-api.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {BusinessPwsListRefresh} from '@app/ngrx-store/business/pws/pws.actions';
import {Store} from '@ngrx/store';

@Component({
    selector: 'app-client-company-form-work-time',
    templateUrl: './work-time.component.html'
})
export class WorkTimeComponent extends Reactive implements OnInit {

    @Input()
    pws: PwsInterface;

    @Input()
    service: ServiceInterface;

    @Input()
    workTime: WorkTimeInterface;

    @Input()
    isLocal: boolean = true;

    form: FormGroup;

    submitted: boolean = false;

    constructor(
        private readonly workTimeApiService: WorkTimeApiService,
        private readonly modalController: ModalController,
        private readonly workerApiService: WorkerApiService,
        private readonly serviceApiService: ServiceApiService,
        private readonly alertPromptService: AlertPromptService,
        private readonly store: Store,
        private readonly formBuilder: FormBuilder
    ) {
        super();
    }

    ngOnInit() {
        if (this.isLocal) {
            this.form = this.formBuilder.group({
                pwsId: [this.pws.id],
                startAt: ['08:00', Validators.required],
                endAt: ['20:00', Validators.required],
                weekday: [0, Validators.required],
                break: ['00:00', Validators.required],
            });
        } else {
            this.form = this.formBuilder.group({
                business__worker_id: [this.pws.worker.id, Validators.required],
                business__point_worker_service_id: [this.pws.id, Validators.required],
                startAt: ['08:00', Validators.required],
                endAt: ['20:00', Validators.required],
                weekday: [0, Validators.required],
                break: ['00:00', Validators.required],
            });
        }
        if (this.workTime) {
            this.form.get('weekday').setValue(this.workTime.weekday - 1);
            this.form.get('startAt').setValue(this.workTime.startAt);
            this.form.get('endAt').setValue(this.workTime.endAt);
            this.form.get('break').setValue(this.workTime.break);
        }
    }

    async dismissModal(data = null) {
        await this.modalController.dismiss(data, 'cancel');
    }

    save() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const form = {
                ...this.form.value,
                weekday: this.form.value['weekday'] + 1
            };
            delete form.isSubmitted;
            if (this.isLocal) {

                this.dismissModal(form);
            } else {

                if (this.workTime) {

                    this.workTimeApiService.putEdit(form, this.workTime.id)
                        .pipe(
                            this.takeUntil()
                        )
                        .subscribe((result) => {
                            this.form.markAsUntouched();
                            this.form.updateValueAndValidity();
                            if (result) {
                                this.store.dispatch(new BusinessPwsListRefresh());
                                this.dismissModal();
                            }
                        });
                } else {

                    this.workTimeApiService.postCreate(form)
                        .pipe(
                            this.takeUntil()
                        )
                        .subscribe((result) => {
                            this.form.markAsUntouched();
                            this.form.updateValueAndValidity();
                            if (result) {
                                this.store.dispatch(new BusinessPwsListRefresh());
                                this.dismissModal();
                            }
                        });
                }

            }
        }
    }

    delete() {
        this.alertPromptService.presentAlert('Czy na pewno chcesz usunąć godzinę?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {
                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        if (this.isLocal) {
                            this.dismissModal('delete');
                        } else {

                            this.workTimeApiService.delete(this.workTime.id)
                                .pipe(
                                    this.takeUntil()
                                )
                                .subscribe((result) => {
                                    this.submitted = false;
                                    if (result) {
                                        this.store.dispatch(new BusinessPwsListRefresh());
                                        this.dismissModal();
                                    }
                                });
                        }
                    }
                }
            ]
        });

    }
}
