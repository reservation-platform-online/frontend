import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkTimeComponent} from '@app/common/modules/client/company-form/work-time/work-time.component';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';
import {InputModule} from '@app/common/ui/input/input.module';


@NgModule({
    declarations: [WorkTimeComponent],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        WeekdayNameModule.forRoot(),
        InputModule
    ]
})
export class ClientCompanyFormWorkTimeModule { }
