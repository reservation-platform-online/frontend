import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController, NavController, ToastController} from '@ionic/angular';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {PasswordResetApiService} from '@app/common/services/api/modules/client/password-reset/password-reset-api.service';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {HeaderService} from '@app/common/services/header/header.service';
import {ActivatedRoute} from '@angular/router';
import {Login, LoginExternal} from '@app/ngrx-store/authentication/authentication.actions';
import {Tools} from '@app/common/tools/tools';
import {RegistrationApiService} from '@app/common/services/api/modules/client/registration/registration-api.service';

@Component({
  selector: 'app-client-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent extends Reactive implements OnInit {

  @Input()
  sizeMd: number = 6;

  @Input()
  offsetMd: number = 3;

  @Input()
  useInModel: boolean = false;

  @Output()
  closeModal = new EventEmitter();

  form: FormGroup;
  submitted = false;

  constructor(
      private readonly navController: NavController,
      private readonly formBuilder: FormBuilder,
      private readonly iab: InAppBrowser,
      private readonly store: Store<fromStore.State>,
      private readonly passwordResetService: PasswordResetApiService,
      private readonly alertPromptService: AlertPromptService,
      private readonly toastController: ToastController,
      private readonly headerService: HeaderService,
      private readonly activatedRoute: ActivatedRoute,
      private readonly registrationApiService: RegistrationApiService,
      private readonly modalController: ModalController
  ) {

    super();

    this.form = this.formBuilder.group({
      email: ['testcli@example.pl', Validators.required],
      password: ['123123123', Validators.required]
    });
    this.headerService.setTitle('Logowanie');
  }

  ngOnInit() {

    this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {

      if (params['code']) {
        this.store.dispatch(new LoginExternal({
          code: atob(params['code']),
        }));
      }

    });

    this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((result: boolean) => {

      if (result) {

        this.closeModal.emit(true);

      }

    });

  }

  login(): void {
    this.form.markAsTouched();
    this.form.updateValueAndValidity();

    if (this.form.valid) {

      const value = Tools.copyObject(this.form.value);
      delete value.isSubmitted;

      this.store.dispatch(new Login(this.form.value));
      this.form.markAsUntouched();
      this.form.updateValueAndValidity();
    } else {
      this.alertPromptService.presentAlert('Podaj e-mail i hasło', {
        header: 'Błąd'
      });
    }
  }

  back() {
    this.navController.pop();
  }

  public onEnter(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.login();
    }

  }

  public dismissModal(): void {
    this.modalController.getTop().then((modal) => {
      modal.dismiss();
    });
  }

  forgotPassword() {
    this.passwordResetService.forgotPassword();
  }
}
