import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginFormComponent} from '@app/common/modules/client/login-form/login-form.component';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {InputModule} from '@app/common/ui/input/input.module';
import {ReactiveFormsModule} from '@angular/forms';
import {PasswordResetModule} from '@app/common/services/api/modules/client/password-reset/password-reset.module';


@NgModule({
  declarations: [LoginFormComponent],
  exports: [
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    InputModule,
    ReactiveFormsModule,
    PasswordResetModule
  ]
})
export class LoginFormModule { }
