import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import {Observable} from 'rxjs';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {ModalController} from '@ionic/angular';
import {WorkerCommentsModalComponent} from '@app/common/modal/client/worker-comments-modal/worker-comments-modal.component';
import {NavService} from '@app/common/services/util/nav.service';
import {DaySlotsInterface} from '@app/common/interfaces/modules/client/reservation/day-slots.interface';
import {ROLE} from '@app/common/interfaces/api/role.enum';
import {WorkTimeModalComponent} from '@app/common/modal/client/business/work-time-modal/work-time-modal.component';
import {FormReservationModalComponent} from '@app/common/modal/client/business/form-reservation-modal/form-reservation-modal.component';

@Component({
  selector: 'app-client-worker-component',
  templateUrl: './worker.component.html'
})
export class WorkerComponent implements OnInit {

  @Input()
  role: ROLE;

  @Input()
  item: SearchItemInterface;

  @Output()
  selectedSlot = new EventEmitter<{
    day: DaySlotsInterface,
    slot: SlotInterface
  }>();

  @Output()
  refresh = new EventEmitter();

  public readonly roles = ROLE;

  constructor(
      private readonly navService: NavService,
      private readonly modalController: ModalController,
      private readonly screenSizeService: ScreenSizeService
  ) { }

  onIsMobile(): Observable<boolean> {
    return this.screenSizeService.isMobileView$;
  }

  ngOnInit() {
  }

  selectSlot($event) {
    this.selectedSlot.emit($event);
  }

  openService() {
    this.navService.open(['service', this.item.id]);
  }

  openComments() {

    this.modalController.create({
      component: WorkerCommentsModalComponent,
      id: 'view-comments',
      cssClass: 'ion-page-without-padding-top',
      swipeToClose: true,
      componentProps: {
        item: this.item
      },
      backdropDismiss: true,
    }).then((modal) => {
      modal.present();
    });

  }

  openReservationForm() {
    this.modalController.create({
      component: FormReservationModalComponent,
      id: 'reservation-form',
      cssClass: 'ion-page-without-padding-top',
      swipeToClose: true,
      componentProps: {
        item: this.item,
      },
      backdropDismiss: true,
    }).then((modal) => {
      modal.present();
    });

  }

  openWorkTime() {
    this.modalController.create({
      component: WorkTimeModalComponent,
      id: 'view-work-time',
      cssClass: 'ion-page-without-padding-top',
      swipeToClose: true,
      componentProps: {
        item: this.item,
      },
      backdropDismiss: true,
    }).then((modal) => {
      modal.present();
    });
  }
}
