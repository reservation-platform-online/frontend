import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkerComponent} from '@app/common/modules/client/worker/worker.component';
import {IonicModule} from '@ionic/angular';
import {BarRatingModule} from 'ngx-bar-rating';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {RouterModule} from '@angular/router';
import {ImgModule} from '@app/common/ui/img/img.module';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';


@NgModule({
    declarations: [WorkerComponent],
    exports: [
        WorkerComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        BarRatingModule,
        StringTimeConvertModule,
        RouterModule,
        ImgModule,
        WeekdayNameModule
    ]
})
export class WorkerModule { }
