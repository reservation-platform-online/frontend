import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CommentListComponent} from '@app/common/modules/client/comment-list/comment-list.component';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {IonicModule} from '@ionic/angular';
import {BarRatingModule} from 'ngx-bar-rating';
import {ImgModule} from '@app/common/ui/img/img.module';



@NgModule({
  declarations: [
    CommentListComponent
  ],
  exports: [
    CommentListComponent
  ],
    imports: [
        CommonModule,
        DefaultContainerModule,
        IonicModule,
        BarRatingModule,
        ImgModule
    ]
})
export class ClientServiceCommentListModule { }
