import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {selectPwsComments, selectPwsParamsComments} from '@app/ngrx-store';
import {SearchCommentsInterface} from '@app/common/interfaces/modules/client/pws/search-comments.interface';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Store} from '@ngrx/store';
import {Reactive} from '@app/common/cdk/reactive';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {SearchItemCommentListInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment-list.interface';
import {IonInfiniteScroll} from '@ionic/angular';

@Component({
  selector: 'app-client-service-comment-list',
  templateUrl: './comment-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentListComponent extends Reactive implements OnInit {

  @ViewChild(IonInfiniteScroll)
  infiniteScroll: IonInfiniteScroll;

  @Input()
  item: SearchItemInterface;

  private loadDataEvent: any;
  private searchParams: SearchCommentsInterface;

  constructor(
      private readonly store: Store,
      private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {

    this.store.select(selectPwsParamsComments).pipe(this.takeUntil()).subscribe((searchParams: SearchCommentsInterface) => {

      if (searchParams && searchParams.itemId && !this.searchParams) {

        this.searchParams = searchParams;
        this.refreshContent();

      }

    });

  }

  public get item$(): Observable<SearchItemCommentListInterface> {
    return this.store.select(selectPwsComments).pipe(this.takeUntil(), tap((item: SearchItemCommentListInterface) => {
      if (!this.item && item) {
        this.changeDetectorRef.detectChanges();
      }
      if (this.loadDataEvent) {

        this.loadDataEvent.target.classList.remove('show');

        this.loadDataEvent.target.complete();
        this.loadDataEvent.target.disabled = true;
        this.loadDataEvent = null;

      }
    })) as Observable<SearchItemCommentListInterface>;
  }

  refreshContent($event: void) {
    this.store.dispatch(new fromPws.RefreshComments());
  }

  loadData(event) {

    this.store.dispatch(new fromPws.NextCommentsPage());
    this.loadDataEvent = event;
    this.loadDataEvent.target.classList.add('show');

  }

  toggleInfiniteScroll() {

    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;

  }

}
