import {Component, Input, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-client-popover-universal-select',
  templateUrl: './universal-select.component.html'
})
export class UniversalSelectComponent implements OnInit {

  @Input()
  options: {
    name: string,
    value: any
  }[];

  constructor(
      private readonly popoverController: PopoverController
  ) { }

  ngOnInit() {}

  selectedOption(option: {
    name: string,
    value: any
  }) {
    this.dismissClick(option);
  }

  async dismissClick(data: {
    name: string,
    value: any
  }) {
    await this.popoverController.dismiss(data);
  }

}
