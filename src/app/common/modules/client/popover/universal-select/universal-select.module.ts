import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UniversalSelectComponent} from '@app/common/modules/client/popover/universal-select/universal-select.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
  declarations: [UniversalSelectComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class UniversalSelectModule { }
