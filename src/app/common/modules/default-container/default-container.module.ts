import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultContainerComponent} from '@app/common/modules/default-container/default-container.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
  declarations: [DefaultContainerComponent],
  exports: [
    DefaultContainerComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class DefaultContainerModule { }
