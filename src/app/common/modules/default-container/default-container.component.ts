import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {Reactive} from '@app/common/cdk/reactive';
import {tap} from 'rxjs/operators';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Component({
    selector: 'app-default-container',
    templateUrl: './default-container.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultContainerComponent extends Reactive implements OnChanges {

    @Input()
    item$: Observable<any>;

    @Input()
    noDataText: string = 'Brak danych.';

    @Input()
    hasModels: boolean = false;

    @Input()
    halfContentHeight: boolean = false;

    @Input()
    useRefreshButton: boolean = true;

    @Input()
    ionMarginTop: boolean = false;

    @Input()
    useLoadMoreData: boolean = false;

    @Output()
    onRefresh = new EventEmitter<void>();

    @Output()
    onLoadMoreData = new EventEmitter<void>();

    showRefreshButton: boolean = false;
    seconds: number = 4;

    private loadDataEvent: any;

    private readonly timer = (seconds: number = 4) => {

        this.seconds = seconds - 1;

        if (this.seconds > 0) {

            setTimeout(() => {

                this.timer(this.seconds);

            }, 1000);

        } else {

            this.seconds = 4;
            this.showRefreshButton = true;

        }

        this.changeDetectorRef.detectChanges();

    }

    refresh() {
        this.onRefresh.emit();
        this.showRefreshButton = false;
        this.timer();
    }

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef
    ) {

        super();

    }

    ngOnChanges(changes: SimpleChanges) {

        if (changes.item$) {

            this.item$ = this.item$.pipe(this.takeUntil(), tap((value) => {

                if (this.loadDataEvent) {

                    this.loadDataEvent.target.classList.remove('show');

                    this.loadDataEvent.target.complete();
                    this.loadDataEvent = null;

                }

                if ((!value || (this.hasModels ? value.total === 0 : false)) && this.useRefreshButton) {
                    if (this.seconds === 4) {
                        this.timer();
                    }
                }
            }));

        }

    }

    isNotEmpty(item: any): boolean {
        return isNotNullOrUndefined(item) ? Object.keys(item).length > 0 : false;
    }

    loadData(event) {
        this.onLoadMoreData.emit();
        this.loadDataEvent = event;
        this.loadDataEvent.target.classList.add('show');
    }

}
