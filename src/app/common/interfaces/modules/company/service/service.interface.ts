import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';
import {ImageInterface} from '@app/common/interfaces/modules/company/service/image.interface';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';

export interface ServiceInterface {

    pwsList?: PwsInterface[];
    images?: ImageInterface[];
    image?: string;
    worker?: WorkerInterface;

    id: string;
    companyId: string;
    categoryId: number;
    name: string;
    description: string;
    interval: string;
    price: string;
    priceTo: string;
    discount: number;
    discountIsPercent: number;
    earliestReservationTime: string;
    earliestReservationDate: number;
    lastReservationDate: string;
    paymentIsImportant: number;
    timerTimeForPay: string;
    hide: number;
    enabled: number;
    status: number;
    createdAt: string;
    updatedAt: string;

    _isSelected?: boolean;

}
