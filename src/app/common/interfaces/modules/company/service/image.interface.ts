export interface ImageInterface {
    id: string;
    galleryId: string;
    link: string;
    name: string;

}
