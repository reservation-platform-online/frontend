import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {WorkTimeInterface} from '@app/common/interfaces/modules/company/work-time/work-time.interface';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';

export interface PwsInterface {

    id: string;
    companyId: string;
    pointId: string;
    service: ServiceInterface;
    worker: WorkerInterface;
    workTimeList: WorkTimeInterface[];
    limitReservationPerSlot: number;
    enabled: number;
    status: number;
    createdAt?: string;
    updatedAt?: string;

}
