import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';

export interface WorkerInterface {

    services?: ServiceInterface[];
    pwsList?: PwsInterface[];

    id: string;
    companyId: string;
    firstName: string;
    lastName: string;
    score: number;
    photo: string;
    status: number;
    enabled: number;
    position: string;
    createdAt?: string;
    updatedAt?: string;

    userEmail?: string;

    _isSelected?: boolean;

}
