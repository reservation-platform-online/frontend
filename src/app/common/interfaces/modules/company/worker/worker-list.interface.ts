import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';

export interface WorkerListInterface {

    total: number;
    models: WorkerInterface[];

}
