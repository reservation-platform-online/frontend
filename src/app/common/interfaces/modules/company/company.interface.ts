export interface CompanyInterface {

    id: string;
    name: string;
    seoLink: string;
    logoLink: string;
    description: string;
    enabled: number;
    status: number;
    typeCompanyId: number;
    createdAt: string;
    updatedAt: string;

}
