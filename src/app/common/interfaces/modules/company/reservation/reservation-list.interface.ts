import {ReservationInterface} from '@app/common/interfaces/modules/client/reservation/reservation.interface';

export interface ReservationListInterface {

    total: number;
    models: ReservationInterface[];

}
