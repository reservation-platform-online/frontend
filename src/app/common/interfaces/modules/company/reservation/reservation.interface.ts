import {ReservationServiceInterface} from '@app/common/interfaces/modules/client/reservation/reservation-service.interface';
import {ReservationWorkerInterface} from '@app/common/interfaces/modules/client/reservation/reservation-worker.interface';
import {ReservationUserInterface} from './reservation-user.interface';

export interface ReservationInterface {

    id: string;
    date: string;
    startAt: string;
    endAt: string;
    weekday: number;
    cost: number;
    charge: number;
    status: number;
    priceTo: string;
    price: string;
    createdAt: string;
    updatedAt: string;
    service: ReservationServiceInterface;
    worker: ReservationWorkerInterface;
    users: ReservationUserInterface[];

}
