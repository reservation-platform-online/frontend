import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';

export interface DayInterface {

    dayName: string;
    dayNumber: number;
    monthName: string;
    date: Date;

    slots: SlotInterface[];

}
