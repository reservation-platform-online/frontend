import {ReservationUserUserInterface} from '@app/common/interfaces/modules/client/reservation/reservation-user-user.interface';

export interface ReservationUserInterface {
    id: string;
    reservationId: string;
    user: ReservationUserUserInterface;
    email: string;
    note: string;
    isInitiator: number;
    meetToken: string;
    createdAt: string;
    updatedAt: string;
    status: number;

}
