export interface UpdateInterface {

    status?: number;
    cost?: number;
    charge?: number;
    pwsId?: string;

}
