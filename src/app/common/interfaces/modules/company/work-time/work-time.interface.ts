export interface WorkTimeInterface {

    id: string;
    workerId: string;
    pwsId: string;
    startDate: string;
    startAt: string;
    endDate: string;
    endAt: string;
    break: string;
    weekday: number;

}
