export interface ReservationUserUserInterface {

    id: string;
    email: string;
    firstName: string;
    lastName: string;
    photo: string;
    phone: string;
    birthday: string;
    numberOfViolations: number;
    numberOfComments: number;

}
