import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import {Moment} from 'moment';
import {ReservationInterface} from '@app/common/interfaces/modules/company/reservation/reservation.interface';

export interface DaySlotsInterface {

    dayName: string;
    dayNumber: string;
    date: Moment;

    slots?: SlotInterface[];
    reservations?: ReservationInterface[];

}
