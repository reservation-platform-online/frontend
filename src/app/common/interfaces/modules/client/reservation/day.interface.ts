import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';

export interface DayInterface {

    dayName: string;
    dayNumber: number;
    date: Date;
    monthName: string;

    slots: SlotInterface[];

}
