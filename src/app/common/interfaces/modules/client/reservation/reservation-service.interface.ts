import {ImageInterface} from '@app/common/interfaces/modules/company/service/image.interface';

export interface ReservationServiceInterface {

    images?: ImageInterface[];

    categoryId: number;
    categoryName?: string;
    companyId: string;
    description?: string;
    id: string;
    interval: string;
    name: string;
    price: string;
    priceTo: string;

    earliestReservationDate?: number;
    earliestReservationTime?: string;
    lastReservationDate?: number;
    paymentIsImportant?: number;
    timerTimeForPay?: string;

}
