export interface SlotInterface {

    start: string;
    end: string;
    status: number;

}
