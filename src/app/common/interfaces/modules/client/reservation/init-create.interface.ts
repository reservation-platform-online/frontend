import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';

export interface InitCreateInterface {

    date: string;
    start: string;
    end: string;
    item: SearchItemInterface;

}
