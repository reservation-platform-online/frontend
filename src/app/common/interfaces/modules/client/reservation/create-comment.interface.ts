export interface CreateCommentInterface {

    reservationUserId: string;
    score: number;
    isAnonymous: number;
    comment: string;
    pwsId: string;

}
