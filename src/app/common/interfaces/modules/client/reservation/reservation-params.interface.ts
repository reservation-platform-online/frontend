export interface ReservationParamsInterface {
    page: number;
    count: number;
    active: number;
    orderBy?: string;
    sort?: string;

}
