import {ReservationServiceInterface} from '@app/common/interfaces/modules/client/reservation/reservation-service.interface';
import {ReservationWorkerInterface} from '@app/common/interfaces/modules/client/reservation/reservation-worker.interface';
import {ReservationUserInterface} from '@app/common/interfaces/modules/client/reservation/reservation-user.interface';
import {ReservationUserShortInterface} from '@app/common/interfaces/modules/client/reservation/reservation-user-short.interface';
import {RESERVATION_STATUS} from '@app/common/services/api/modules/client/reservation/reservation.status';

export interface ReservationInterface {

    id: string;
    date: string;
    startAt: string;
    reservationUser: ReservationUserShortInterface;
    pwsId: string;
    endAt: string;
    weekday: number;
    paymentIsImportant: number;
    timerTimeForPay: string;
    cost: number;
    charge: number;
    status: RESERVATION_STATUS;
    priceTo: string;
    price: string;
    note: string;
    meetLink: string;
    meetPassword: string;
    interval: string;
    createdAt: string;
    updatedAt: string;
    service: ReservationServiceInterface;
    worker: ReservationWorkerInterface;
    users: ReservationUserInterface[];

}
