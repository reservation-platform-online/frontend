export interface BusinessPwsItemWorkerInterface {

    companyId: string;
    createdAt: string;
    enabled: number;
    firstName: string;
    id: string;
    lastName: string;
    photo: string;
    position: string;
    score: string;
    status: number;
    updatedAt: string;
}
