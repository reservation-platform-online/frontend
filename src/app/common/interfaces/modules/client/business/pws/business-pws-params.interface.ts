
export interface BusinessPwsParamsInterface {
    page: number;
    count: number;
    active: number;
    orderBy?: string;
    sort?: string;
    itemId?: string;
    categoryId?: number;
    categoryName?: string;

}
