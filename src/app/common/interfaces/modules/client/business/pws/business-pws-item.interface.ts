import {BusinessPwsItemServiceInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item-service.interface';
import {BusinessPwsItemWorkerInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item-worker.interface';
import {BusinessPwsItemWorkTimeInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item-work-time.interface';

export interface BusinessPwsItemInterface {
    id: string;
    limitReservationPerSlot: number;
    numberOfComments: number;
    score: string;
    status: number;

    service: BusinessPwsItemServiceInterface;

    workTime: BusinessPwsItemWorkTimeInterface[]

    worker: BusinessPwsItemWorkerInterface;
}
