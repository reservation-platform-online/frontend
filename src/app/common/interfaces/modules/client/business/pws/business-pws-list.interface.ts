import {BusinessPwsItemInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item.interface';

export interface BusinessPwsListInterface {

    total: number;
    models: BusinessPwsItemInterface[];

}
