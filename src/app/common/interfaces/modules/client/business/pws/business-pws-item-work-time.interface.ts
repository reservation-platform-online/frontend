export interface BusinessPwsItemWorkTimeInterface {

    break: string;
    endAt: string;
    endDate: string;
    id: string;
    pwsId: string;
    startAt: string;
    startDate: string;
    weekday: number;
    workerId: string;
}
