import {BusinessPwsItemServiceImageInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item-service-image.interface';

export interface BusinessPwsItemServiceInterface {

    categoryId: number;
    companyId: string;
    categoryName?: string;
    createdAt: string;
    description: string;
    discount: number;
    discountIsPercent: number;
    earliestReservationDate: number;
    earliestReservationTime: string;
    enabled: number;
    hide: number;
    id: string;
    images: BusinessPwsItemServiceImageInterface[]
    interval: string;
    lastReservationDate: number;
    name: string;
    paymentIsImportant: number;
    price: string;
    priceTo: string;
    status: number;
    timerTimeForPay: string;
    updatedAt: string;

}
