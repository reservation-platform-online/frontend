export interface BusinessPwsItemServiceImageInterface {

    galleryId: string;
    id: string;
    link: string;
    name: string;
    order: number;


}
