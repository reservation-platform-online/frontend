import {BusinessReservationItemInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-item.interface';

export interface BusinessReservationListInterface {

    total: number;
    models: BusinessReservationItemInterface[];

}
