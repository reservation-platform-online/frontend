export enum BusinessReservationStatusEnum {

    USER_CANCELED,
    ENABLED,
    CONFIRMED,
    FINISHED,
    RECEPTION_CANCELED,
    PAYMENT_TIME_OUT,
    USER_WORKER_DID_NOT_COME,

}

export interface BusinessReservationParamsInterface {
    page: number;
    count: number;
    active: number;
    orderBy?: string;
    sort?: string;
    itemId: string;
    start: string;
    end: string;
    categoryId?: number;
    categoryName?: string;
    statusList?: BusinessReservationStatusEnum[];

}
