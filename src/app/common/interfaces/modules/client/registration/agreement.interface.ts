export interface AgreementInterface {
    id: number;
    title: string;
    description: string;
    isRequired: number;
    checked: number;
}
