export interface ServiceInterface {

    id: string;
    name: string;
    categoryId: number;
    companyId: string;
    description: string;
    status: number;
    createdAt: string;
    updatedAt: string;

}
