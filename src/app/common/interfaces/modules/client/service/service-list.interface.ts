import {ServiceInterface} from '@app/common/interfaces/modules/client/service/service.interface';

export interface ServiceListInterface {

    total: number;
    models: ServiceInterface[];

}
