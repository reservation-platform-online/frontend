export interface SearchItemCompanyInterface {

    createdAt: string;
    description: string;
    id: string;
    logoLink: string;
    name: string;
    seoLink: string;
    status: number;
    typeCompanyId: number;
    updatedAt: string;

}
