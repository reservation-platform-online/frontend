import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';

export interface SearchInterface {

    search?: string;
    serviceId?: string;
    category?: CategoryInterface;
    companyId?: string;
    pointId?: string;

    itemId?: string;

    start?: string; // epoch yyyy-mm-dd
    end?: string; // epoch yyyy-mm-dd

    orderBy?: string;
    sort?: string;
    page?: number;
    count?: number;
}
