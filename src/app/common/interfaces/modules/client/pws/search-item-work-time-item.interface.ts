export interface SearchItemWorkTimeItemInterface {

    startAt: string;
    endAt: string;
    weekday: number;
    break: string;

}
