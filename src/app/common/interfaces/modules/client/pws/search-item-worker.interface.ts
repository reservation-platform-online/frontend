import {SearchItemCompanyInterface} from '@app/common/interfaces/modules/client/pws/search-item-company.interface';

export interface SearchItemWorkerInterface {

    id: string;
    company: SearchItemCompanyInterface;
    firstName: string;
    lastName: string;
    score: number;
    photo: string;
    status: number;
    createdAt: string;
    updatedAt: string;
    numberOfComments: number;

}
