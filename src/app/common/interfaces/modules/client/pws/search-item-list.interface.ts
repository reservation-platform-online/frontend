import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';

export interface SearchItemListInterface {

    total: number;
    models: SearchItemInterface[];

}
