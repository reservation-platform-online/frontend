export interface SearchCommentsInterface {

    itemId?: string;

    orderBy?: string;
    sort?: string;
    page?: number;
    count?: number;
    expand?: string;

}
