import {SearchItemCommentInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment.interface';

export interface SearchItemCommentListInterface {

    total: number;
    models: SearchItemCommentInterface[];

}
