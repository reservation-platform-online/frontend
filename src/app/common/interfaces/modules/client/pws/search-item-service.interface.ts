import {ImageInterface} from '@app/common/interfaces/modules/company/service/image.interface';

export interface SearchItemServiceInterface {

    images?: ImageInterface[];

    categoryId: number;
    categoryName?: string;
    companyId: string;
    description: string;
    id: string;
    interval: string;
    name: string;
    price: string;
    priceTo: string;

    discount?: string;
    discountIsPercent?: string;
    earliestReservationTime?: string;
    earliestReservationDate?: number;
    lastReservationDate?: string;
    paymentIsImportant?: number;
    timerTimeForPay?: string;
    hide?: number;
    enabled?: number;
    status?: number;
    createdAt?: string;
    updatedAt?: string;

}
