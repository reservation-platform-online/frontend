export interface SearchItemCommentInterface {

    id: string;
    isAnonymous: number;
    comment: string;
    score: number;
    createdAt: string;
    user?: {
        firstName: string;
        lastName: string;
        photo: string;
        id: string;
    };

}
