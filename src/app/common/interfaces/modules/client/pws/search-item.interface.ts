import {SearchItemWorkerInterface} from '@app/common/interfaces/modules/client/pws/search-item-worker.interface';
import {SearchItemWorkTimeItemInterface} from '@app/common/interfaces/modules/client/pws/search-item-work-time-item.interface';
import {SearchItemDisabledItemInterface} from '@app/common/interfaces/modules/client/pws/search-item-disabled-item.interface';
import {SearchItemServiceInterface} from '@app/common/interfaces/modules/client/pws/search-item-service.interface';

export interface SearchItemInterface {

    id: string;
    serviceId: string;
    worker: SearchItemWorkerInterface;
    pointId: string;
    status: number;
    numberOfComments: number;
    score: number;
    workTime: SearchItemWorkTimeItemInterface[];
    weekend: [];
    disabled: SearchItemDisabledItemInterface[];
    limitReservationPerSlot: number;
    service: SearchItemServiceInterface;

}
