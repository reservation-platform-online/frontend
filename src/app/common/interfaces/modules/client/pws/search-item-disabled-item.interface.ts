export interface SearchItemDisabledItemInterface {
    id: string;
    date: string;
    startAt: string;
    endAt: string;
    weekday: number;
    count: number;
}
