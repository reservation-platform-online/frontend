export interface ProfileCompanyInterface {

    description: string;
    id: string;
    logoLink: string;
    name: string;
    seoLink: string;
    status: number;
    updatedAt: string;
    createdAt: string;

}
