import {ProfileCompanyUserInterface} from '@app/common/interfaces/modules/client/profile/profile-company-user.interface';
import {ProfileJobInterface} from '@app/common/interfaces/modules/client/profile/profile-job.interface';

export interface ProfileInterface {

    id: string;
    companies: ProfileCompanyUserInterface[];
    jobs: ProfileJobInterface[];
    email: string;
    firstName: string;
    lastName: string;
    photo: string;
    phone: string;
    birthday: string;
    numberOfViolations: number;
    status: number;
    hasExternalAuthorization: number;
    createdAt: string;
    updatedAt: string;

}
