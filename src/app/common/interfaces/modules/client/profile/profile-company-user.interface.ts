import {ProfileCompanyInterface} from '@app/common/interfaces/modules/client/profile/profile-company.interface';

export interface ProfileCompanyUserInterface {

    company: ProfileCompanyInterface;
    id: string;
    role: number;
    createdAt: string;
    updatedAt: string;

}
