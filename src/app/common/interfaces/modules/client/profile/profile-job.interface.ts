import {ProfileCompanyInterface} from '@app/common/interfaces/modules/client/profile/profile-company.interface';

export interface ProfileJobInterface {

    id: string;
    firstName: string;
    lastName: string;
    numberOfComments: number;
    score: number;
    photo: string;
    company: ProfileCompanyInterface;
    userId: string;
    position: string;
    status: number;
}
