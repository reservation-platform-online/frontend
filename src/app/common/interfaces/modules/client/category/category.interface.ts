
export interface CategoryInterface {

    color: string;
    id: number;
    name: string;
    icon: string;
    parentId: number;
    numberOfServices: number;

}
