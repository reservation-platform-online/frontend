export interface CategoryParamsInterface {
     page?: number;
     count?: number;
     orderBy?: string;
     categoryId?: number;
     flat?: number;
     sort?: string;

}
