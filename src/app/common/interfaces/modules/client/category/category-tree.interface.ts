export interface CategoryTreeInterface {

    color: string;
    id: number;
    parentId: number;
    name: string;
    icon: string;
    numberOfChildren: number;
    numberOfServices: number;
    children?: CategoryTreeInterface[];

}
