import {CategoryTreeInterface} from '@app/common/interfaces/modules/client/category/category-tree.interface';

export interface CategoryTreeListInterface {

    total: number;
    models: CategoryTreeInterface[];

}
