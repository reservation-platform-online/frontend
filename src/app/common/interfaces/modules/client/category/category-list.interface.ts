import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';

export interface CategoryListInterface {

    total: number;
    models: CategoryInterface[];

}
