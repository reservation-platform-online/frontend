export interface RequestOptionsInterface {
    url: string;
    urlConfig?: {
        subUrl?: string;
        withVersion?: boolean,
        cityId?: number,
        outerLink?: boolean
    };
    refreshToken?: boolean;
    showLoader?: boolean;
    hideAlert?: boolean;
    method?: 'get' | 'post' | 'put' | 'patch' | 'head' | 'delete' | 'options' | 'upload' | 'download';
    serializer?: 'json' | 'urlencoded' | 'utf8' | 'multipart' | 'raw'; // Only device, is not working for www
    data?: {
        [index: string]: any;
    } | string;
    params?;
    observe?: 'response';
    headersOptions?: {
        notSendAuthorization?: boolean;
        useContentType?: boolean;
        disableCache?: boolean;
        useContentTypeJson?: boolean;
    };
    responseType?: 'text' | 'arraybuffer' | 'blob' | 'json';
}
