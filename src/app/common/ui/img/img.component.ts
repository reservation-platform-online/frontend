import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
    selector: 'app-img',
    template: `
        <ng-container *ngIf="isAvatar; else imgTemplate">
            <img
                    [src]="src"
                    [alt]="alt"
                    (error)="onImgError($event)"/>
        </ng-container>
        <ng-template #imgTemplate>
            <ng-container *ngIf="useAspectRation; else defaultTemplate">
                <div
                        [ngStyle]="configAspectContainer">
                    <img
                            [src]="src"
                            [alt]="alt"
                            [ngStyle]="configAspectImg"
                            (error)="onImgError($event)"/>
                </div>
            </ng-container>
            <ng-template #defaultTemplate>
                <img
                        [src]="src"
                        [alt]="alt"
                        (error)="onImgError($event)"/>
            </ng-template>
        </ng-template>
    `
})
export class ImgComponent implements OnInit, OnChanges {

    @Input()
    src: string;

    @Input()
    alt: string;

    @Input()
    isAvatar: boolean;

    @Input()
    useAspectRation: boolean = false;

    @Input()
    aspectRation: number = 56.25;

    @Input()
    configAspectContainer = {
        width: '100%',
        overflow: 'hidden',
        margin: 0,
        paddingTop: `${this.aspectRation}%`,
        position: 'relative',
    };

    @Input()
    configAspectImg = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        width: '100%',
        transform: 'translate(-50%, -50%)',
    };

    constructor() {
    }

    ngOnInit() {
    }

    onImgError(event) {
        event.target.src = this.getSrc();
    }

    ngOnChanges(changes: SimpleChanges): void {

        if (changes.aspectRation) {

            // aspectRation = 9/16;
            this.aspectRation = this.aspectRation * 100;

        }

        if (changes.src) {

            if (!this.src || this.src === 'null' || this.src.length === 0) {

                this.src = this.getSrc();

            }

        }

    }

    private getSrc(): string {

        return '../../../../../assets/images/default-img.jpg';

    }
}
