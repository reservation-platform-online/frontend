import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImgComponent} from '@app/common/ui/img/img.component';



@NgModule({
  declarations: [
    ImgComponent
  ],
  exports: [
    ImgComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ImgModule { }
