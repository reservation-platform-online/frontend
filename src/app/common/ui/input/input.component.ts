import {Component, Input, OnInit, Self, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';


@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class InputComponent implements OnInit, ControlValueAccessor {

    @Input()
    public type: 'text' | 'password' | 'number' | 'textarea' | 'money' | 'range' | 'toggle' | 'phone' | 'datetime' = 'text';

    @Input()
    public lines: 'full' | 'none' = 'full';

    @Input()
    public placeholder: string;

    @Input()
    public slot: 'end' | 'start' = 'end';

    @Input()
    public isRequired: number = 0;

    @Input()
    public showVisibilityIcon: boolean = false;

    @Input()
    public searchEnabled: boolean = false;

    @Input()
    public onlyError: boolean = false;

    @Input()
    public invalidMessage: string;

    @Input()
    public position: 'floating' | 'stacked' | 'fixed' | undefined = 'floating';

    @Input()
    public min: number = 1;

    @Input()
    public max: number = 5;

    @Input()
    public step: number = 1;

    @Input()
    public snaps: boolean = true;

    @Input()
    public displayFormat: string = 'HH:mm';

    @Input()
    public minuteValues: string = '0,5,10,15,20,25,30,35,40,45,50,55';

    public inputInvalid: boolean = false;

    public inputValue: string | number = null;

    public control: NgControl;

    public static getInvalidMessage(
        type: 'text' | 'password' | 'number' | 'textarea' | 'money' | 'range' | 'toggle' | 'phone' | 'datetime'
    ): string {

        switch (type) {
            case 'text':
                break;
            case 'password':
                break;
            case 'number':
                break;
            case 'textarea':
                break;
            case 'money':
                break;
            case 'range':
                break;
            case 'toggle':
                return 'Jest wymagane';
            case 'phone':
                return 'Format: 012345678';
        }

        return 'Pole należy wypełnić';

    }

    constructor(
        @Self() private readonly ngControl: NgControl
    ) {
        ngControl.valueAccessor = this;
        this.control = ngControl;
    }

    public togglePasswordType(): void {
        if (this.type === 'text') {
            this.type = 'password';
        } else {
            this.type = 'text';
        }
    }

    public isPasswordHidden(): boolean {
        return this.type === 'text';
    }


    public get warningMessage(): string {
        return this.inputInvalid ? this.invalidMessage : '';
    }

    onValueChange = (_: any) => {
    };
    onTouch: any = () => {
    };

    registerOnChange(fn: any) {
        this.onValueChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouch = fn;
    }

    onChange(event) {
        let value = event.target.value;
        switch (this.type) {
            case 'text':
                break;
            case 'password':
                break;
            case 'number':
                break;
            case 'textarea':
                break;
            case 'money':
                break;
            case 'range':
                break;
            case 'toggle':
                value = this.inputValue ? 0 : 1;
                break;
            case 'phone':
                break;
            case 'datetime':
                break;
        }
        this.inputValue = value;
        this.onValueChange(this.inputValue);
    }

    writeValue(value: any): void {
        if (!isNotNullOrUndefined(this.inputValue) && isNotNullOrUndefined(value)) {
            this.inputValue = value;
        }
        // this.inputValue = value ?? null;
    }

    ngOnInit(): void {

        const control: NgControl = this.control;
        const parent = control.control.parent;

        parent.statusChanges
            .subscribe((_) => {

                this.inputInvalid = parent.touched && this.control.invalid;
            });

        if (!isNotNullOrUndefined(this.invalidMessage)) {

            this.invalidMessage = InputComponent.getInvalidMessage(this.type);

        }

    }

}
