import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Input,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';

@Component({
    selector: 'dynamic-svg-template',
    template: `
        <div #svgEl class="dynamic-svg-template"></div>
    `,
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicSvgTemplateComponent implements AfterViewInit {
    @ViewChild('svgEl') svgRef: ElementRef;

    @Input()
    svg: string;

    ngAfterViewInit() {
        this.svgRef.nativeElement.innerHTML = this.svg;
    }
}
