import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
    selector: 'inline-svg',
    template: `
		<object [data]="safeResourceUrl()"></object>
    `,
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicInlineSvgComponent {
    @Input()
    badgeSvg: string;

    constructor(private readonly sanitizer: DomSanitizer) {

    }

    safeResourceUrl(): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.badgeSvg);
    }
}
