import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DynamicSvgTemplateComponent} from './dynamic-svg-template.component';
import {DynamicInlineSvgComponent} from './dynamic-inline-svg.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        DynamicSvgTemplateComponent,
        DynamicInlineSvgComponent
    ],
    exports: [
        DynamicSvgTemplateComponent,
        DynamicInlineSvgComponent
    ]
})
export class DynamicSvgTemplateModule {

}
