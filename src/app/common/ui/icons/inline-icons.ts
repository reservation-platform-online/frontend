export const userSvg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.96 19.92">\n' +
    '    <defs>\n' +
    '        <style>.cls-1 {\n' +
    '            fill: none;\n' +
    '            stroke: currentColor;\n' +
    '            stroke-miterlimit: 10;\n' +
    '        }</style>\n' +
    '    </defs>\n' +
    '    <title>Asset 23ldpi</title>\n' +
    '    <g id="Layer_2" data-name="Layer 2">\n' +
    '        <g id="Layer_1-2" data-name="Layer 1">\n' +
    '            <path class="cls-1"\n' +
    '                  d="M14.79,11.71S12.56,14.5,10,14.5s-4.81-2.79-4.81-2.79C-.1,14.63.52,19.42.52,19.42H19.44S20.06,14.63,14.79,11.71Z"/>\n' +
    '            <path class="cls-1" d="M10,.5S5.48.21,5.48,4.88,8.4,11.71,10,11.71s4.5-2.16,4.5-6.83S10,.5,10,.5Z"/>\n' +
    '            <path class="cls-1" d="M5.48,4.3s2.25,1.46,3.71,0c2.27-2.28,3.12.38,5.29.38"/>\n' +
    '        </g>\n' +
    '    </g>\n' +
    '</svg>'

export const keySvg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.72 13.92">
    <defs>
        <style>.cls-1 {
            fill: none;
            stroke: currentColor;
            stroke-miterlimit: 10;
        }</style>
    </defs>
    <title>Asset 22ldpi</title>
    <g id="Layer_2" data-name="Layer 2">
        <g id="Layer_1-2" data-name="Layer 1">
            <circle class="cls-1" cx="6.96" cy="6.96" r="6.46"/>
            <circle class="cls-1" cx="5.1" cy="6.96" r="1.58"/>
            <path class="cls-1"
                  d="M12.94,4.52h9.93l2,2.19-1.41.46.45.87H22.12l.42,1.21H13A6.12,6.12,0,0,0,12.94,4.52Z"/>
            <path class="cls-1" d="M7,10.54a4.09,4.09,0,0,0,3-.5"/>
        </g>
    </g>
</svg>`

export const emailSvg = `<?xml version="1.0" ?>
<svg id="Слой_1" style="enable-background:new 0 0 128 128;" version="1.1" viewBox="0 0 128 128" xml:space="preserve"
     xmlns="http://www.w3.org/2000/svg"><style type="text/css">
\t.st0 {
        fill: currentColor;
    }
</style>
    <path class="st0"
          d="M112.8,31.8c-0.7-1.2-1.7-2.1-2.9-2.8h0c-1.1-0.6-2.4-1-3.8-1H22c-1.4,0-2.6,0.3-3.8,1h0  c-1.2,0.6-2.2,1.6-2.9,2.7C14.4,32.9,14,34.4,14,36v56c0,1.5,0.4,2.9,1.1,4.1c0.7,1.2,1.6,2.1,2.8,2.8c1.2,0.7,2.6,1.1,4.1,1.1h84  c1.5,0,2.9-0.4,4.1-1.1c1.2-0.7,2.1-1.6,2.8-2.8c0.7-1.2,1.1-2.6,1.1-4.1V36C114,34.4,113.6,33,112.8,31.8z M106,32  c0.1,0,0.2,0,0.3,0L69.7,64c-2.6,2.2-6.9,2.3-9.6,0.1L21.9,32H106z M85.6,68.8l-2.8,2.8L107,95.8c-0.3,0.1-0.7,0.2-1,0.2H22  c-0.4,0-0.7,0-1-0.2l24.3-24.2l-2.8-2.8L18.2,93C18,92.7,18,92.4,18,92V36c0-0.6,0.2-1.2,0.4-1.7l39.2,32.9c2,1.7,4.8,2.8,7.4,2.8  c2.7,0,5.3-1.2,7.4-3l37.3-32.6c0.2,0.5,0.3,1,0.3,1.6v56c0,0.4,0,0.7-0.2,1L85.6,68.8z"
          id="XMLID_46_"/></svg>`

export const phoneSvg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.33 23.75">
    <defs>
        <style>.cls-1 {
            fill: none;
            stroke: currentColor;
            stroke-miterlimit: 10;
        }</style>
    </defs>
    <title>Asset 20ldpi</title>
    <g id="Layer_2" data-name="Layer 2">
        <g id="Layer_1-2" data-name="Layer 1">
            <rect class="cls-1" x="0.5" y="0.5" width="12.33" height="22.75" rx="2.73" ry="2.73"/>
            <line class="cls-1" x1="0.4" y1="17.71" x2="12.94" y2="17.71"/>
            <line class="cls-1" x1="0.4" y1="3.83" x2="12.94" y2="3.83"/>
            <line class="cls-1" x1="6.65" y1="0.64" x2="6.65" y2="3.83"/>
            <circle class="cls-1" cx="6.65" cy="20.48" r="1.31"/>
        </g>
    </g>
</svg>`

export const mapMarkerSvg = `<svg xmlns="http://www.w3.org/2000/svg" width="22.416" height="32" viewBox="0 0 22.416 32">
  <g id="Group_343" data-name="Group 343" transform="translate(-7.02 -1.68)">
    <path id="Path_2151" data-name="Path 2151" d="M18.229,1.68A11.2,11.2,0,0,0,8.7,18.781L17.7,33.295a.81.81,0,0,0,.685.385.792.792,0,0,0,.692-.391L27.857,18.63A11.213,11.213,0,0,0,18.229,1.68Zm8.244,16.128h0l-8.1,13.518-8.308-13.39a9.6,9.6,0,1,1,16.405-.128Z" transform="translate(0 0)" fill="#808E99"/>
    <path id="Path_2152" data-name="Path 2152" d="M19.5,8a6.17,6.17,0,1,0,6.17,6.17A6.177,6.177,0,0,0,19.5,8Zm0,10.54a4.38,4.38,0,1,1,4.37-4.38A4.385,4.385,0,0,1,19.5,18.54Z" transform="translate(-1.272 -0.985)" fill="#808E99"/>
  </g>
</svg>
`

export const postalCodeSvg = `<svg xmlns="http://www.w3.org/2000/svg" width="13.667" height="13.636" viewBox="0 0 13.667 13.636">
  <g id="Group_351" data-name="Group 351" transform="translate(-93.592 -47.742)">
    <rect id="Rectangle_329" data-name="Rectangle 329" width="0.637" height="7.401" transform="translate(100.107 53.977)" fill="#808e98"/>
    <path id="Path_2195" data-name="Path 2195" d="M107.259,54.336H93.592v-3.2a3.4,3.4,0,0,1,3.393-3.393h6.881a3.4,3.4,0,0,1,3.393,3.393ZM94.228,53.7h12.394V51.135a2.759,2.759,0,0,0-2.756-2.756H96.985a2.759,2.759,0,0,0-2.756,2.756Z" transform="translate(0 0)" fill="#808e98"/>
    <path id="Path_2196" data-name="Path 2196" d="M100.378,54.336H93.592v-3.2a3.393,3.393,0,0,1,6.786,0ZM94.228,53.7h5.512V51.135a2.756,2.756,0,0,0-5.512,0Z" transform="translate(0 0)" fill="#808e98"/>
    <circle id="Ellipse_169" data-name="Ellipse 169" cx="0.67" cy="0.67" r="0.67" transform="translate(96.252 50.369)" fill="#808e98"/>
  </g>
</svg>
`

export const citySvg = `<svg xmlns="http://www.w3.org/2000/svg" width="15.305" height="16.167" viewBox="0 0 15.305 16.167">
  <g id="Group_350" data-name="Group 350" transform="translate(-39.833 -40.456)">
    <path id="Path_2188" data-name="Path 2188" d="M57.033,42.4H52.571V40.456h4.462Zm-3.866-.6h3.271v-.752H53.167Z" transform="translate(-7.316)" fill="#808e98"/>
    <path id="Path_2189" data-name="Path 2189" d="M57.4,73.041H55.068v-4H57.4Zm-1.741-.6h1.145V69.636H55.664Z" transform="translate(-8.75 -16.418)" fill="#808e98"/>
    <path id="Path_2190" data-name="Path 2190" d="M57.4,50.221H55.068V47.489H57.4Zm-1.741-.6h1.145V48.085H55.664Z" transform="translate(-8.75 -4.04)" fill="#808e98"/>
    <path id="Path_2191" data-name="Path 2191" d="M57.4,59.587H55.068V56.855H57.4Zm-1.741-.6h1.145v-1.54H55.664Z" transform="translate(-8.75 -9.419)" fill="#808e98"/>
    <path id="Path_2192" data-name="Path 2192" d="M56.545,58.442H49.29V43.623h7.255Zm-6.659-.6h6.063V44.219H49.886Z" transform="translate(-5.432 -1.819)" fill="#808e98"/>
    <path id="Path_2193" data-name="Path 2193" d="M67.988,64.233H64.936V53.706h3.052Zm-2.456-.6h1.86V54.3h-1.86Z" transform="translate(-14.418 -7.61)" fill="#808e98"/>
    <path id="Path_2194" data-name="Path 2194" d="M46.571,67.823H43.519V59.956h3.052Zm-2.456-.6h1.86V60.552h-1.86Z" transform="translate(-2.117 -11.2)" fill="#808e98"/>
    <rect id="Rectangle_328" data-name="Rectangle 328" width="15.305" height="0.596" transform="translate(39.833 56.027)" fill="#808e98"/>
  </g>
</svg>
`
