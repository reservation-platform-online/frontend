import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'ios-arrow',
    template: `
        <svg [style.transform]="'rotate(' + rotateDeg + 'deg)'"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true"
             focusable="false" width="1em" height="1em"
             preserveAspectRatio="xMidYMid meet"
             viewBox="0 0 512 512">
            <path d="M294.1 256L167 129c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.3 34 0L345 239c9.1 9.1 9.3 23.7.7 33.1L201.1 417c-4.7 4.7-10.9 7-17 7s-12.3-2.3-17-7c-9.4-9.4-9.4-24.6 0-33.9l127-127.1z"
                  fill="white"/>
        </svg>
    `,
    styleUrls: ['./ios-arrow-icon.component.scss'],
    host: {
        '[class.ios-arrow]': 'true',
        '[class.disabled]': 'disabled'
    },
    encapsulation: ViewEncapsulation.None
})
export class IosArrowIconComponent {
    @Input()
    rotateDeg: number = 360;

    @Input()
    disabled: boolean;

}
