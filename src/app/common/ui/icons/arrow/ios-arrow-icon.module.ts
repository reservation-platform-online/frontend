import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IosArrowIconComponent} from '@app/common/ui/icons/arrow/ios-arrow-icon.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        IosArrowIconComponent
    ],
    exports: [
        IosArrowIconComponent
    ]
})
export class IosArrowIconModule {

}
