import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CloseIconComponent} from '@app/common/ui/icons/close/close-icon.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        CloseIconComponent
    ],
    exports: [
        CloseIconComponent
    ]
})
export class CloseIconModule {

}
