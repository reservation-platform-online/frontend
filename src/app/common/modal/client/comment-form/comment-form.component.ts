import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {InitCreateInterface} from '@app/common/interfaces/modules/client/reservation/init-create.interface';
import {Store} from '@ngrx/store';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ModalController} from '@ionic/angular';
import {ReservationInterface} from '@app/common/interfaces/modules/client/reservation/reservation.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Tools} from '@app/common/tools/tools';
import {ReservationApiService} from '@app/common/services/api/modules/client/reservation/reservation-api.service';
import * as ReservationActions from '@app/ngrx-store/reservation/reservation.actions';

@Component({
    selector: 'app-reservation-form',
    templateUrl: './comment-form.component.html',
    styleUrls: ['./comment-form.component.scss'],
})
export class CommentFormComponent extends Reactive implements OnInit {

    @Input()
    reservation: ReservationInterface;

    initCreate: InitCreateInterface;

    form: FormGroup;

    constructor(private readonly store: Store,
                private readonly loadingController: LoadingService,
                private readonly modalController: ModalController,
                private readonly formBuilder: FormBuilder,
                private readonly reservationApiService: ReservationApiService,
    ) {
        super();

        this.form = this.formBuilder.group({
            isAnonymous: [0, [Validators.required]],
            scoreService: [3, [Validators.required, Validators.min(1), Validators.max(5)]],
            scoreWorker: [3, [Validators.required, Validators.min(1), Validators.max(5)]],
            comment: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]],
        });

    }

    ngOnInit() {

        // this.reservationApiService.onReservationIsCreated().pipe(this.takeUntil()).subscribe((result) => {
        //
        //     if (result) {
        //
        //         this.modalController.dismiss(null, 'success').then(() => {
        //             this.navController.navigateRoot(['reservation']).then(() => {
        //                 this.reservationApiService.clearReservationIsCreated();
        //                 this.store.dispatch(new ReservationActions.Load());
        //             });
        //         });
        //
        //     }
        //
        // });

    }

    refreshContent(event): void {
        event.target.complete();
    }

    create() {

        this.form.markAsTouched();
        this.form.updateValueAndValidity();

        const value = Tools.copyObject(this.form.value);
        value.score = (value.scoreWorker + value.scoreService) / 2;
        delete value.scoreService;
        delete value.scoreWorker;

        this.reservationApiService.createComment$({
            ...value,
            pwsId: this.reservation.pwsId,
            reservationUserId: this.reservation.reservationUser.id
        }).subscribe(() => {

            this.form.markAsUntouched();
            this.form.updateValueAndValidity();
            this.store.dispatch(new ReservationActions.Load());
            this.dismissModal();
        });

        // this.store.dispatch(new fromReservation.Create({
        //   date: this.initCreate.date,
        //   end: this.initCreate.end,
        //   pwsId: this.initCreate.item.id,
        //   start: this.initCreate.start
        // }));

    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

}
