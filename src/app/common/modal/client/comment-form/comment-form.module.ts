import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {CommentFormComponent} from '@app/common/modal/client/comment-form/comment-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputModule} from '@app/common/ui/input/input.module';


@NgModule({
    declarations: [
        CommentFormComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ReservationStatusModule,
        StringTimeConvertModule,
        ReactiveFormsModule,
        InputModule
    ]
})
export class CommentFormModule { }
