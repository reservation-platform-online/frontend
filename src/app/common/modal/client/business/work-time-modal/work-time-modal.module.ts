import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';
import {WorkTimeModalComponent} from '@app/common/modal/client/business/work-time-modal/work-time-modal.component';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';


@NgModule({
    declarations: [WorkTimeModalComponent],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        WeekdayNameModule.forRoot(),
        StringTimeConvertModule,
    ]
})
export class WorkTimeModalModule {
}
