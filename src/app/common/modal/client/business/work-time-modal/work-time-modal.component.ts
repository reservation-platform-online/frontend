import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {WorkTimeApiService} from '@app/common/services/api/modules/company/work-time/work-time-api.service';
import {ModalController} from '@ionic/angular';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {SearchItemWorkTimeItemInterface} from '@app/common/interfaces/modules/client/pws/search-item-work-time-item.interface';
import {WorkTimeComponent} from '@app/common/modules/client/company-form/work-time/work-time.component';

@Component({
    selector: 'app-client-company-form-work-time',
    templateUrl: './work-time-modal.component.html'
})
export class WorkTimeModalComponent extends Reactive implements OnInit {

    @Input()
    item: SearchItemInterface;

    constructor(
        private readonly workTimeApiService: WorkTimeApiService,
        private readonly modalController: ModalController,
    ) {
        super();
    }

    ngOnInit() {
    }

    async dismissModal(data = null) {
        await this.modalController.dismiss(data, 'cancel');
    }

    openForm(workTime?: SearchItemWorkTimeItemInterface) {

        this.dismissModal();

        this.modalController.create({
            component: WorkTimeComponent,
            componentProps: {
                pws: this.item,
                isLocal: false,
                workTime
            },
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

        });

    }

}
