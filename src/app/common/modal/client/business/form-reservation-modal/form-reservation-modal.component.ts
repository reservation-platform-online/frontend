import {Component, Input, OnInit} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {EMAIL_PATTERN} from '@app/common/tools/patterns.tool';
import {CalendarDaysSlotsComponent} from '@app/common/modules/calendar-days-slots/calendar-days-slots.component';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Store} from '@ngrx/store';
import * as moment from 'moment';
import {ReservationApiService} from '@app/common/services/api/modules/client/business/reservation/reservation-api.service';
import {Tools} from '@app/common/tools/tools';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';

@Component({
  selector: 'app-form-reservation-modal',
  templateUrl: './form-reservation-modal.component.html'
})
export class FormReservationModalComponent implements OnInit {

  @Input()
  item: SearchItemInterface;

  form: FormGroup;

  submitted: boolean = false;

  constructor(
      private readonly modalController: ModalController,
      private readonly formBuilder: FormBuilder,
      private readonly alertPromptService: AlertPromptService,
      private readonly navController: NavController,
      private readonly reservationApiService: ReservationApiService,
      private readonly store: Store,
  ) {

  }

  ngOnInit() {

    this.form = this.formBuilder.group({
      userEmail: [null, [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
      status: [2, [Validators.required]],
      pwsId: [this.item.id, [Validators.required]],
      start: [null, [Validators.required]],
      end: [null, [Validators.required]],
      date: [null, [Validators.required]],
      createRoom: [{value: 1, disabled: false}, [Validators.required]],
    });

  }

  async dismissModal(data?: any) {
    await this.modalController.dismiss(data, 'cancel');
  }

  public create(): void {

    this.form.markAsTouched();
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      const value = Tools.copyObject(this.form.value);
      delete value.isSubmitted;
      if (!Object.keys(value).includes('createRoom')) {
        value['createRoom'] = '0';
      }
      this.reservationApiService.postItem$(value).subscribe((_) => {
        this.form.markAsUntouched();
        this.form.updateValueAndValidity();
        this.openAlertInfoSuccess();
        this.dismissModal();
      });
    }

  }

  public openCalendar(): void {

    this.modalController.create({
      component: CalendarDaysSlotsComponent,
      id: 'calendar-select-slot',
      cssClass: 'ion-page-without-padding-top',
      swipeToClose: true,
      componentProps: {
        item: this.item,
        returnObject: true,
        inModal: true
      },
      backdropDismiss: true,
    }).then((modal) => {
      modal.present();
      // this.store.dispatch(new fromPws.RefreshCalendar());
      modal.onWillDismiss().then((result) => {
        const day = (result.data.day.date as moment.Moment);
        this.form.controls['date'].setValue(day.format('YYYY-MM-DD'));
        this.form.controls['start'].setValue(result.data.slot.start);
        this.form.controls['end'].setValue(result.data.slot.end);
        this.store.dispatch(new fromPws.LoadCalendarSuccess(null));
      });

    });
  }

  public openAlertInfoSuccess(): void {
    this.alertPromptService.presentAlert('', {
      header: 'Wykonano pomyślnie',
      buttons: [
        {
          cssClass: 'cancel-button',
          text: 'Zamknij',
          handler: _ => {
          }
        },
      ]
    });
  }

  changeStatus($event: any) {
    const value = $event.detail.value;

    this.form.controls['createRoom'].setValue(value === 2 ? 1 : 0);
    if (value === 2) {
      this.form.controls['createRoom'].enable();
    } else {
      this.form.controls['createRoom'].disable();
    }

  }
}
