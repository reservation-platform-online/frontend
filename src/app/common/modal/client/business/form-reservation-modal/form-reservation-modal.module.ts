import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormReservationModalComponent} from '@app/common/modal/client/business/form-reservation-modal/form-reservation-modal.component';
import {IonicModule} from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {InputModule} from '@app/common/ui/input/input.module';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {CalendarDaysSlotsModule} from '@app/common/modules/calendar-days-slots/calendar-days-slots.module';


@NgModule({
    declarations: [
        FormReservationModalComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        InputModule,
        ReservationStatusModule,
        CalendarDaysSlotsModule,
    ]
})
export class FormReservationModalModule { }
