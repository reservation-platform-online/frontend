import {Component, Input} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {SearchItemCommentInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment.interface';

@Component({
    selector: 'app-comment-modal',
    templateUrl: './comment-modal.component.html',
    styleUrls: ['./comment-modal.component.scss'],
})
export class CommentModalComponent {

    @Input()
    comment: SearchItemCommentInterface;

    constructor(
        private readonly modalController: ModalController,
    ) {

    }


    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

}
