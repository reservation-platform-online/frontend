import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {CommentModalComponent} from '@app/common/modal/client/comment/comment-modal.component';
import {BarRatingModule} from 'ngx-bar-rating';
import {ImgModule} from '@app/common/ui/img/img.module';


@NgModule({
    declarations: [
        CommentModalComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        BarRatingModule,
        ImgModule,
    ]
})
export class CommentModalModule {
}
