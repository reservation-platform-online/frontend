import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkerCommentsModalComponent} from '@app/common/modal/client/worker-comments-modal/worker-comments-modal.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [WorkerCommentsModalComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class WorkerCommentsModalModule { }
