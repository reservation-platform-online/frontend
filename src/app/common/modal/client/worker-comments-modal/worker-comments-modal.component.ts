import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';

@Component({
  selector: 'app-worker-comments-modal',
  templateUrl: './worker-comments-modal.component.html',
  styleUrls: ['./worker-comments-modal.component.scss'],
})
export class WorkerCommentsModalComponent implements OnInit {

  @Input()
  item: SearchItemInterface;

  constructor(
      private readonly modalController: ModalController,
  ) { }

  ngOnInit() {}

  async dismissModal() {
    await this.modalController.dismiss(null, 'cancel');
  }

}
