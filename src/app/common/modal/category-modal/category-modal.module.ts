import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {CategoryModalComponent} from '@app/common/modal/category-modal/category-modal.component';
import {CategoryModule} from '@app/common/components/category/category.module';



@NgModule({
  declarations: [
    CategoryModalComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    CategoryModule
  ]
})
export class CategoryModalModule { }
