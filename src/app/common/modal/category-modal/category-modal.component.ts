import {Component, Input, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {ServiceListInterface} from '@app/common/interfaces/modules/client/service/service-list.interface';
import {Store} from '@ngrx/store';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ModalController, NavController} from '@ionic/angular';
import {selectCategoryTreeList, selectPwsParams, selectServiceList} from '@app/ngrx-store';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {Tools} from '@app/common/tools/tools';
import * as fromCategory from '@app/ngrx-store/category/category.actions';
import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';
import {CategoryTreeInterface} from '@app/common/interfaces/modules/client/category/category-tree.interface';
import {CategoryTreeListInterface} from '@app/common/interfaces/modules/client/category/category-tree-list.interface';

@Component({
  selector: 'app-search-css-form',
  templateUrl: './category-modal.component.html'
})
export class CategoryModalComponent extends Reactive implements OnInit {

  @Input()
  showAllCategoryBtn: boolean = false;

  private serviceList: ServiceListInterface;

  categoryListBuffer: CategoryTreeListInterface;
  categoryList: CategoryTreeListInterface;
  private searchParams: SearchInterface;

  constructor(
      private readonly navController: NavController,
      private readonly store: Store,
      private readonly loadingController: LoadingService,
      private readonly modalController: ModalController
  ) {
    super();
  }

  ngOnInit() {

    this.store.select(selectCategoryTreeList).pipe(this.takeUntil()).subscribe((categoryList) => {

      this.categoryListBuffer = Tools.copyObject(categoryList);
      this.categoryList = Tools.copyObject(categoryList);

    });

    this.store.select(selectPwsParams).pipe(this.takeUntil()).subscribe((searchParams) => {

      this.searchParams = Tools.copyObject(searchParams);
      this.refreshContent();

    });

    this.store.select(selectServiceList).pipe(this.takeUntil()).subscribe((serviceList) => {

      this.serviceList = serviceList as ServiceListInterface;

    });

  }

  getNameOfService(id: string) {

    if (!this.serviceList) {
      return '-';
    }

    return this.serviceList.models.find((service) => service.id === id).name;

  }

  refreshContent(event = null): void {
    if (event) {
      event.target.complete();
    }
    this.store.dispatch(new fromCategory.Load());
  }

  async dismissModal(data?: any) {
    await this.modalController.dismiss(data, 'cancel');
  }

  selectCategory(category: CategoryInterface) {

    this.dismissModal(category);

  }

  search($event: any) {

    if ($event) {

      const searched = $event.target.value.toLowerCase();

      if (searched.length > 0) {

        const returnFindChildren = (children: CategoryTreeInterface[], search: string) => {

          const newChildren = [];

          if (children && children.length > 0) {

            for (const child of children) {

              if (child.name.toLowerCase().search(search) > -1) {

                newChildren.push(child);

              } else {

                child.children = returnFindChildren(child.children, search);

                if (child.children.length > 0) {

                  newChildren.push(child);

                }

              }

            }

          }

          return newChildren;

        };

        this.categoryList = Tools.copyObject(this.categoryListBuffer);

        this.categoryList.models = returnFindChildren(this.categoryList.models, searched);

        this.categoryList.total = this.categoryList.models.length;

        return;

      } else {

        this.categoryList = Tools.copyObject(this.categoryListBuffer);

      }

    }

  }

  clearSelectedCategory() {
    this.selectCategory({
      id: null,
      name: null
    } as CategoryInterface);
  }
}
