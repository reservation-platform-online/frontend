import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TypeCompanyConvertPipe} from '@app/common/pipes/type-company/type-company-convert.pipe';

@NgModule({
  declarations: [
    TypeCompanyConvertPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TypeCompanyConvertPipe
  ],
})
export class TypeCompanyConvertModule { }
