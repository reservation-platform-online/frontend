import {Pipe, PipeTransform} from '@angular/core';
import {environment} from '@envi/environment';

@Pipe({
  name: 'typeCompany'
})
export class TypeCompanyConvertPipe implements PipeTransform {

  /**
   * @param value
   * @param args
   */
  transform(value: any, args?: any): any {

    if (value) {
      return environment.typeCompanyList.find((type) => type.id === value).name;
    }
    return '-';

  }

}
