import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weekdayName'
})
export class WeekdayNamePipe implements PipeTransform {

  /**
   * @param value
   * @param args
   */
  transform(value: number, args?: any): any {

    const weekdayName = ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'];

    if (args === 'getArray') {

      return weekdayName;

    }

    if (value) {

      return weekdayName[value - 1];

    }

    return '-';

  }

}
