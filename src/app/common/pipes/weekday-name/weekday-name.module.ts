import { NgModule } from '@angular/core';
import {WeekdayNamePipe} from '@app/common/pipes/weekday-name/weekday-name.pipe';

@NgModule({
  declarations: [
    WeekdayNamePipe
  ],
  imports: [],
  exports: [
    WeekdayNamePipe
  ],
})
export class WeekdayNameModule {
  static forRoot() {
    return {
      ngModule: WeekdayNameModule,
      providers: [],
    };
  }
}
