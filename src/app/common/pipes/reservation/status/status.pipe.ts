import {Pipe, PipeTransform} from '@angular/core';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {environment} from '@envi/environment';

@Pipe({
    name: 'reservationStatus'
})
export class ReservationStatusPipe implements PipeTransform {

    /**
     *
     *  0 - Customer canceled
     *  1 - Created
     *  2 - Confirmed
     *  3 - Finish
     *  4 - Reception canceled
     *  5 - Payment time out
     *  6 - Customer: Employee did not come
     *
     */

    /**
     * @param value
     * @param args
     */
    transform(value: number | any[], args?: 'getArray' | { useJoin: boolean, join?: string, useContainer?: boolean }): any {

        const data = environment.data.reservation.status;

        if (args === 'getArray') {

            return data;

        }

        if (args?.useJoin) {

            if (args?.useContainer) {

                return (value as any[]).map((item: number) => `
                    <div class="reservation-status-chip ${'reservation-status-' + item}">
                        ${data[item]}
                    </div>
                `).join(args?.join ?? ' ');

            } else {

                return (value as any[]).map((item: number) => data[item]).join(args?.join ?? ', ');

            }

        } else {

            if (isNotNullOrUndefined(value)) {

                return data[(value as number)];

            }

        }

        return value;

    }

}
