import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReservationStatusPipe} from '@app/common/pipes/reservation/status/status.pipe';

@NgModule({
  declarations: [
    ReservationStatusPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ReservationStatusPipe
  ],
})
export class ReservationStatusModule { }
