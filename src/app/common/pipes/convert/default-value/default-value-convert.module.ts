import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultValueConvertPipe} from '@app/common/pipes/convert/default-value/default-value-convert.pipe';

@NgModule({
  declarations: [
    DefaultValueConvertPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DefaultValueConvertPipe
  ],
})
export class DefaultValueConvertModule { }
