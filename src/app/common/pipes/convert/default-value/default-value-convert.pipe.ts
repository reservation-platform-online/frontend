import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultValueConvert'
})
export class DefaultValueConvertPipe implements PipeTransform {

  /**
   * @param value
   * @param args
   */
  transform(value: any, args?: any): any {

    if (value) {
      return value;
    }
    return '-';

  }

}
