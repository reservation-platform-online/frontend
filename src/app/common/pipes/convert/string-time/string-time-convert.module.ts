import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StringTimeConvertPipe} from '@app/common/pipes/convert/string-time/string-time-convert.pipe';

@NgModule({
  declarations: [
    StringTimeConvertPipe
  ],
  imports: [],
  exports: [
    StringTimeConvertPipe
  ],
})
export class StringTimeConvertModule {
  static forRoot() {
    return {
      ngModule: StringTimeConvertModule,
      providers: [],
    };
  }
}
