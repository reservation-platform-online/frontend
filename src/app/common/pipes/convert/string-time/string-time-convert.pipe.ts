import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringTimeConvert'
})
export class StringTimeConvertPipe implements PipeTransform {

  /**
   * @param value
   * @param args
   */
  transform(value: string, args?: any): any {

    if (value) {

      const timeArray = value.split(':');

      return `${timeArray[0]}:${timeArray[1]}`;

    }

    return '-';

  }

}
