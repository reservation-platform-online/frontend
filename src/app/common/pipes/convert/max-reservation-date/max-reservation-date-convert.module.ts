import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaxReservationDateConvertPipe} from '@app/common/pipes/convert/max-reservation-date/max-reservation-date-convert.pipe';

@NgModule({
  declarations: [
    MaxReservationDateConvertPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MaxReservationDateConvertPipe
  ],
})
export class MaxReservationDateConvertModule { }
