import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxReservationDate'
})
export class MaxReservationDateConvertPipe implements PipeTransform {

  /**
   * @param value
   * @param args
   */
  transform(value: any, args?: any): any {

    if (value) {
      // TODO
      return value;
    }
    return '-';

  }

}
