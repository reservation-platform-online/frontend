import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        AlertPromptService
    ]
})
export class AlertPromptModule {

}
