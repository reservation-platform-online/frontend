import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {AlertButton, AlertInput} from '@ionic/core/dist/types/components/alert/alert-interface';

@Injectable()
export class AlertPromptService {

    private alertOpen: boolean;

    constructor(private readonly alertCtrl: AlertController) {
    }

    async presentAlert(message: string,
                       config?: {
                           header?: string,
                           id?: string,
                           subHeader?: string,
                           buttons?: (AlertButton | string)[],
                           inputs?: AlertInput[],
                           cssClass?: string
                       }): Promise<HTMLIonAlertElement> {
        if (!this.alertOpen) {
            this.alertOpen = true;
            let header,
                subHeader,
                id,
                buttons: (AlertButton | string)[] = ['OK'],
                inputs: AlertInput[],
                cssClass = 'reservation-platform-basic-alert';

            if (config && config.header) {
                header = config.header;
            }

            if (config && config.inputs) {
                inputs = config.inputs;
            }

            if (config && config.id) {
                id = config.id;
            }

            if (config && config.subHeader) {
                subHeader = config.subHeader;
            }

            if (config && config.buttons) {
                buttons = config.buttons;
            }

            if (config && config.cssClass) {
                cssClass = config.cssClass;
            }

            const alert = this.alertCtrl.create({
                header,
                subHeader,
                message,
                buttons,
                id,
                inputs,
                cssClass,
                mode: 'md'
            });

            alert.then((a) => {
                a.present()
                    .then(() => {
                        this.alertOpen = false;
                    });
            });

            return alert;
        }
    }

}
