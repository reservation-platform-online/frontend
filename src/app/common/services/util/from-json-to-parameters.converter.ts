import {Injectable} from '@angular/core';

@Injectable()
export class FromJsonToParametersConverter {

    convert(data): string {
        let parameters = '';

        for (const key of Object.keys(data)) {
            parameters += key + '=' + data[key] + '&';
        }
        return parameters.substr(0, parameters.length - 1);
    }

    reconvert(data: string): {} {
        const parameters = {};

        for (const object of data.split('&')) {

            const key = object.split('=')[0];
            parameters[key] = object.split('=')[1];

        }
        return parameters;
    }
}
