import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';
import {Observable, Subject} from 'rxjs';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Injectable()
export class LoadingService {

    private readonly refreshContent$ = new Subject<boolean>();

    constructor(private readonly loadingController: LoadingController) {
    }

    public onRefreshContent(): Observable<boolean> {
        return this.refreshContent$.asObservable();
    }

    async presentLoadingWithOptions(options?: {
        refreshContent?: boolean,
        id?: string,
    }) {

        if (await this.loadingController.getTop() === undefined) {

            return await this.loadingController.create({
                spinner: 'lines',
                mode: 'md',
                cssClass: 'reservation-platform-loader',
                duration: 6000,
                id: 'loader',
                translucent: true,
                backdropDismiss: false,
                ...(options && options.id ? {id: options.id} : {})
            }).then(a => {
                a.present().then(() => {
                    // a.dismiss();
                    if (options && isNotNullOrUndefined(options.refreshContent)) {
                        this.refreshContent$.next(options.refreshContent);
                    }
                });
            });
        }

        return new Promise<boolean>((resolve) => {
            resolve(false);
        });
    }

    async dismissLoader(id: string = 'loader', all: boolean = false) {

        if (await this.loadingController.getTop() !== undefined) {
            if (all) {
                while (await this.loadingController.getTop() !== undefined) {
                    await this.loadingController.dismiss();
                }
            } else {
                await this.loadingController.dismiss(null, null, id);
            }
        }
        return new Promise<boolean>((resolve) => {
            resolve(false);
        });
    }

}
