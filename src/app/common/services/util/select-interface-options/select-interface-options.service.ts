import {Injectable} from '@angular/core';

@Injectable()
export class SelectInterfaceOptionsService {

    getCustomSelectOptions(header: string, cssClass?: string): any {

        if (!cssClass) {
            cssClass = 'reservation-platform-basic-alert';
        }

        return {header: header, cssClass: cssClass, mode: 'md'};
    }
}
