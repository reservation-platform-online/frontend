import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelectInterfaceOptionsService} from '@app/common/services/util/select-interface-options/select-interface-options.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        SelectInterfaceOptionsService
    ]
})
export class SelectInterfaceOptionsModule {

}
