import {Injectable} from '@angular/core';

@Injectable()
export class DateFormatConverter {

    convert(dateBirth: string): string {
        const date = new Date(dateBirth);
        return `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
    }

    dateToString(date: Date): string {
        const newDate = new Date(date);
        return `${('0' + newDate.getDate()).slice(-2)}.${('0' + (newDate.getMonth() + 1)).slice(-2)}.${newDate.getFullYear()}`;
    }

}
