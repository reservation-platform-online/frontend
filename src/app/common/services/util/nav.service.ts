import {Injectable} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavController, Platform} from '@ionic/angular';

@Injectable()
export class NavService {

    isNative: boolean = false;

    constructor(
        private readonly navController: NavController,
        private readonly activatedRoute: ActivatedRoute,
        private readonly platform: Platform,
    ) {
        this.isNative = this.platform.is('mobileweb') ? false : this.platform.is('ios') || this.platform.is('android');
    }

    public getPrevUrl(config: {
        cut: number
    } = {
        cut: 1
    }): string {
        const urls: string[] = this.activatedRoute.snapshot['_routerState']['url'].split('/');
        if (!urls[0] || urls[0] === '' || urls[0].length === 0) {
            urls.shift();
        }

        return urls.slice(0, urls.length - config.cut).join('/');
    }

    public back(config?: {
        cut: number
    }) {

        const url = this.getPrevUrl(config);

        if (!url || url === '' || url.length === 0) {

            return this.navController.navigateRoot(['/']);

        }

        return this.navController.navigateBack(url);

    }

    public open(urls: string[]) {

        return this.navController.navigateForward([decodeURIComponent(this.getPrevUrl({
            cut: 0
        })), ...urls]);

    }

    public goHome() {

        return this.navController.navigateRoot(['/']);

    }

}
