import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {distinctUntilChanged} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScreenSizeService {

  private isMoreDesktop = new BehaviorSubject<boolean>(false);
  private isDesktop = new BehaviorSubject<boolean>(false);
  private isTablet = new BehaviorSubject<boolean>(false);
  private isMobile = new BehaviorSubject<boolean>(false);

  constructor() { }

  onResize(size: number) {

    if (size < 768) {
      this.isMobile.next(true);
    } else {
      this.isMobile.next(false);
    }

    if (size >= 768 && size < 1220) {
      this.isTablet.next(true);
    } else {
      this.isTablet.next(false);
    }

    if (size >= 992 && size < 1220) {
      this.isDesktop.next(true);
    } else {
      this.isDesktop.next(false);
    }

    if (size >= 1220) {
      this.isMoreDesktop.next(true);
    } else {
      this.isMoreDesktop.next(false);
    }

  }

  get isMoreDesktopView$(): Observable<boolean> {
    return this.isMoreDesktop.asObservable().pipe(distinctUntilChanged());
  }

  get isDesktopView$(): Observable<boolean> {
    return this.isDesktop.asObservable().pipe(distinctUntilChanged());
  }

  get isTabletView$(): Observable<boolean> {
    return this.isTablet.asObservable().pipe(distinctUntilChanged());
  }

  get isMobileView$(): Observable<boolean> {
    return this.isMobile.asObservable().pipe(distinctUntilChanged());
  }

}
