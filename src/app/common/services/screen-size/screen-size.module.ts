import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';



@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule
  ],
  providers: [
      ScreenSizeService
  ]
})
export class ScreenSizeModule { }
