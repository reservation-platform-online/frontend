import { Injectable } from '@angular/core';

@Injectable()
export class HeaderService {

  private title: string = 'Title';

  constructor() { }

  getTitle() {
    return this.title;
  }

  setTitle(title: string) {
    this.title = title;
  }

}
