import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderService} from '@app/common/services/header/header.service';



@NgModule({
  declarations: [],
  providers: [
    HeaderService
  ],
  imports: [
    CommonModule
  ]
})
export class HeaderServiceModule { }
