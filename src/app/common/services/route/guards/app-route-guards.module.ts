import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientAuthRouteGuard} from '@app/common/services/route/guards/client-auth-route-guard.service';
import {ClientRouteGuard} from '@app/common/services/route/guards/client-route-guard.service';


@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        ClientAuthRouteGuard,
        ClientRouteGuard
    ]
})
export class AppRouteGuardsModule {

}
