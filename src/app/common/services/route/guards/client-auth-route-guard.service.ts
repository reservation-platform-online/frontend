import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';

import {Observable} from 'rxjs';

import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {NavController} from '@ionic/angular';
import {map, take} from 'rxjs/operators';


@Injectable()
export class ClientAuthRouteGuard implements CanActivate {

    constructor(
        private readonly store: Store<fromStore.State>,
        private readonly navController: NavController
    ) {
    }

    canActivate(): Observable<boolean> {

        return this.store
            .select(fromStore.selectIsLoggedIn)
            .pipe(
                map((isLoggedIn) => {
                    if (isLoggedIn) {
                        this.navController.navigateRoot(['/']);
                    } else {
                        return true;
                    }
                    return true;
                }),
                take(1)
            );

    }
}
