import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApiConfigService} from './api-config.service';
import {ApiService} from './api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {DateFormatConverter} from '@app/common/services/util/date-format.converter';
import { Device } from '@ionic-native/device/ngx';
import {ClientModule} from '@app/common/services/api/modules/client/client.module';
import {CompanyModule} from '@app/common/services/api/modules/company/company.module';
import {WorkerModule} from '@app/common/services/api/modules/worker/worker.module';
import {NavService} from '@app/common/services/util/nav.service';

const utilProviders = [
    NavService,
    DateFormatConverter,
    FromJsonToParametersConverter
];

@NgModule({
    imports: [
        CommonModule,
        ClientModule,
        CompanyModule,
        WorkerModule
    ],
    providers: [
        Device,
        ApiConfigService,
        ApiService,
        ...utilProviders
    ]
})
export class ApiModule {

}
