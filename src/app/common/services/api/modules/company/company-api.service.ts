import {Injectable} from '@angular/core';
import {ApiService} from '@app/common/services/api/api.service';
import {ApiConfigService} from '@app/common/services/api/api-config.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {take, tap} from 'rxjs/operators';
import {ToastController} from '@ionic/angular';
import {LoadingService} from '@app/common/services/util/loading/loading.service';

@Injectable({
    providedIn: 'root'
})
export class CompanyApiService {

    constructor(private readonly apiService: ApiService,
                private readonly apiConfig: ApiConfigService,
                private readonly toastController: ToastController,
                private readonly loadingService: LoadingService,
                private readonly alertPromptService: AlertPromptService,
                private readonly fromJsonToParametersConverter: FromJsonToParametersConverter) {
    }

    saveImage(image: File): Observable<any> {

        const formData = new FormData();

        formData.append('imageFile', image, '1');


        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.company.uploadImage,
                data: formData
            });


    }

    registerCompany(form: any): Observable<any> {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.companies.create,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(() => {
                        this.alertPromptService.presentAlert('Wykonano pomyślnie.');
                    }),
                take(1)
            );
    }

    putUpdate(form: any): Observable<any> {

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.company.update,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }),
                take(1)
            );
    }

    getItem(): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.company.index
            });
    }

}
