import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {environment} from '@envi/environment';
import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;

@Injectable()
export class CompanyIdService {
    private static readonly ID_KEY = environment.keys.COMPANY_ID;

    private id: string;
    private readonly id$ = new ReplaySubject<string>(1);

    constructor() {
    }

    onId$(): Observable<string> {
        return this.id$.asObservable();
    }

    getIdFromStorage(): void {
        Storage
            .get({
                key: CompanyIdService.ID_KEY
            })
            .then((storedId) => {
                if (storedId) {
                    this.id = storedId.value;
                    this.id$.next(this.id);
                }
            });
    }

    setId(id: string): void {
        this.id = id;
        this.id$.next(id);
        Storage
            .set({
                key: CompanyIdService.ID_KEY,
                value: id
            })
            .then(() => {

            });
    }

    removeId(): void {
        Storage
            .remove({
                key: CompanyIdService.ID_KEY
            })
            .then(() => this.id$.next(null));
        this.id = null;
    }

    getIdName(): string {
        return CompanyIdService.ID_KEY;
    }
}
