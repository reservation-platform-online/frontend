import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {ToastController} from '@ionic/angular';
import {LoadingService} from '@app/common/services/util/loading/loading.service';


@Injectable()
export class ServiceApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly loadingService: LoadingService,
        private readonly toastController: ToastController,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
    ) {
    }

    saveImage(uuid: string, image: File): Observable<any> {

        const formData = new FormData();

        formData.append('imageFile', image, '1');


        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.services.uploadImage,
                urlConfig: {
                    subUrl: uuid
                },
                data: formData
            });


    }

    getList(params?: {
        categoryId?: number
    }): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.services.index,
                params
            });
    }

    getItem(uuid: string): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.services.view,
                urlConfig: {
                    subUrl: uuid
                },
                params: {
                    expand: 'pwsList' // services
                }
            });
    }

    postCreate(form: any): Observable<any> {

        delete form.categoryName;
        delete form.image;

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.services.create,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    putEdit(form: any, uuid: string): Observable<any> {

        delete form.categoryName;

        form.description = decodeURIComponent(form.description);

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.services.update,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                },
                urlConfig: {
                    subUrl: uuid
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    putToggleEnable(uuid: string): Observable<any> {

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.services.update,
                headersOptions: {
                    useContentType: true
                },
                urlConfig: {
                    subUrl: `${uuid}/toggle-enable`
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    postConnectWorker(form: any, uuid): Observable<any> {
        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.services.connectWorker,
                urlConfig: {
                    subUrl: uuid
                },
                data: this.fromJsonToParametersConverter.convert(form),
                // data: form,
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    deleteDisconnectWorker(form: any, uuid): Observable<any> {
        return this.apiService
            .request({
                method: 'delete',
                url: apiPaths.company.services.disconnectWorker,
                urlConfig: {
                    subUrl: uuid
                },
                data: this.fromJsonToParametersConverter.convert(form),
                // data: form,
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

}
