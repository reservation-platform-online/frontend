import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {ToastController} from '@ionic/angular';
import {apiPaths} from '@envi/api-paths';
import {take, tap} from 'rxjs/operators';
import {ApiService} from '@app/common/services/api/api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {WorkTimeInterface} from '@app/common/interfaces/modules/company/work-time/work-time.interface';

@Injectable({
    providedIn: 'root'
})
export class WorkTimeApiService {

    private readonly worker$: ReplaySubject<WorkTimeInterface> = new ReplaySubject<WorkTimeInterface>(1);

    constructor(
        private readonly apiService: ApiService,
        private readonly toastController: ToastController,
        private readonly loadingService: LoadingService,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
    ) {
    }

    onWorker(): Observable<WorkTimeInterface> {
        return this.worker$.asObservable();
    }

    getItem(uuid: string): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.workTime.view,
                urlConfig: {
                    subUrl: uuid,
                },
                params: {
                    expand: 'pwsList' // services
                }
            })
            .pipe(
                tap((result) => {
                    this.worker$.next(result);
                }),
                take(1)
            );
    }

    delete(uuid: any): Observable<any> {
        return this.apiService
            .request({
                method: 'delete',
                url: apiPaths.company.workTime.delete,
                urlConfig: {
                    subUrl: uuid
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                ),
                take(1)
            );
    }

    postCreate(form: any): Observable<any> {
        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.workTime.create,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                ),
                take(1)
            );
    }

    putEdit(form: any, uuid: string): Observable<any> {
        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.workTime.update,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                },
                urlConfig: {
                    subUrl: uuid
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                ),
                take(1)
            );
    }

}
