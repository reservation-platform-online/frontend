import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkTimeApiService} from '@app/common/services/api/modules/company/work-time/work-time-api.service';


@NgModule({
  providers: [
      WorkTimeApiService
  ],
  imports: [
    CommonModule
  ]
})
export class WorkTimeModule { }
