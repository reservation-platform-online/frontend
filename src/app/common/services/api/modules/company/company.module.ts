import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompanyApiService} from '@app/common/services/api/modules/company/company-api.service';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';
import {WorkerModule} from '@app/common/services/api/modules/company/worker/worker.module';
import {ServiceModule} from '@app/common/services/api/modules/company/service/service.module';
import {ReservationModule} from '@app/common/services/api/modules/company/reservation/reservation.module';
import {WorkTimeModule} from '@app/common/services/api/modules/company/work-time/work-time.module';


@NgModule({
  providers: [
      CompanyApiService,
      CompanyIdService
  ],
  imports: [
    CommonModule,
    WorkerModule,
    WorkTimeModule,
    ServiceModule,
    ReservationModule
  ]
})
export class CompanyModule { }
