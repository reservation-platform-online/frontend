import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {CreateInterface} from '@app/common/interfaces/modules/company/reservation/create.interface';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {ToastController} from '@ionic/angular';
import {UpdateInterface} from '@app/common/interfaces/modules/company/reservation/update.interface';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';


@Injectable()
export class ReservationApiService {

    constructor(
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly toastController: ToastController,
        private readonly apiService: ApiService
    ) {
    }

    getList(params: {
        start: string,
        end: string
    }): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.reservations.index,
                params
            });
    }

    getItem(params: BusinessReservationParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.pws.view,
                urlConfig: {
                    subUrl: params.itemId
                },
            });
    }

    postItem(payload: CreateInterface) {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.reservations.create,
                serializer: 'urlencoded',
                headersOptions: {
                    useContentType: true,
                },
                data: this.jsonConverter.convert(payload)
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Rezerwacja została stworzona',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Rezerwacja nie została stworzona',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );

    }

    putItem(payload: UpdateInterface, id: string) {

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.reservations.update,
                urlConfig: {
                  subUrl: id
                },
                serializer: 'urlencoded',
                headersOptions: {
                    useContentType: true,
                },
                data: this.jsonConverter.convert(payload)
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Rezerwacja została zapisana',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Rezerwacja nie została zapisana',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );

    }

    deleteCancel(id: string) {

        return this.apiService
            .request({
                method: 'delete',
                url: apiPaths.company.reservations.delete,
                urlConfig: {
                    subUrl: `/${id}`
                }
            });

    }
}
