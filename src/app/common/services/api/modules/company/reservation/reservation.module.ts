import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {ReservationEffects} from '@app/ngrx-store/reservation/reservation.effects';
import {ReservationService} from '@app/common/services/api/modules/company/reservation/reservation.service';
import {ReservationApiService} from '@app/common/services/api/modules/company/reservation/reservation-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([ReservationEffects])
    ],
    providers: [
        ReservationApiService,
        ReservationService
    ]
})
export class ReservationModule {

}
