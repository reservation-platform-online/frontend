import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ToastController} from '@ionic/angular';
import {apiPaths} from '@envi/api-paths';
import {tap} from 'rxjs/operators';
import {ApiService} from '@app/common/services/api/api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {LoadingService} from '@app/common/services/util/loading/loading.service';

@Injectable({
    providedIn: 'root'
})
export class WorkerApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly toastController: ToastController,
        private readonly loadingService: LoadingService,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
    ) {
    }

    saveImage(uuid: string, image: File): Observable<any> {

        const formData = new FormData();

        formData.append('imageFile', image, '1');


        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.workers.uploadImage,
                urlConfig: {
                    subUrl: uuid
                },
                data: formData
            });


    }

    getList(params?: {
        categoryId?: number
    }): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.workers.index,
                params
            });
    }

    getItem(uuid: string): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.workers.view,
                urlConfig: {
                    subUrl: uuid,
                },
                params: {
                    expand: 'pwsList' // services
                }
            });
    }

    postConnectService(form: any, uuid): Observable<any> {
        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.workers.connectService,
                urlConfig: {
                    subUrl: uuid
                },
                data: this.fromJsonToParametersConverter.convert(form),
                // data: form,
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    deleteDisconnectService(form: any, uuid): Observable<any> {
        return this.apiService
            .request({
                method: 'delete',
                url: apiPaths.company.workers.disconnectService,
                urlConfig: {
                    subUrl: uuid
                },
                data: this.fromJsonToParametersConverter.convert(form),
                // data: form,
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    postCreate(form: any): Observable<any> {
        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.company.workers.create,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

    putEdit(form: any, uuid: string): Observable<any> {
        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.company.workers.update,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                },
                urlConfig: {
                    subUrl: uuid
                }
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );
    }

}
