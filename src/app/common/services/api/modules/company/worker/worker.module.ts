import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkerApiService} from '@app/common/services/api/modules/company/worker/worker-api.service';


@NgModule({
  providers: [
      WorkerApiService
  ],
  imports: [
    CommonModule
  ]
})
export class WorkerModule { }
