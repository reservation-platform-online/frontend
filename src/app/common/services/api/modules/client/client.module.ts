import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticationModule} from '@app/common/services/api/modules/client/authentication/authentication.module';
import {WorkerModule} from '@app/common/services/api/modules/worker/worker.module';
import {RegistrationModule} from '@app/common/services/api/modules/client/registration/registration.module';
import {ProfileModule} from '@app/common/services/api/modules/client/profile/profile.module';
import {ReservationModule} from '@app/common/services/api/modules/client/reservation/reservation.module';
import {CategoryModule} from '@app/common/services/api/modules/client/category/category.module';
import {ServiceModule} from '@app/common/services/api/modules/client/service/service.module';
import {PwsModule} from '@app/common/services/api/modules/client/pws/pws.module';
import {ClientApiService} from '@app/common/services/api/modules/client/client-api.service';
import {BusinessModule} from '@app/common/services/api/modules/client/business/business.module';


@NgModule({
    providers: [
        ClientApiService
    ],
    imports: [
        CommonModule,
        AuthenticationModule,
        WorkerModule,
        RegistrationModule,
        ProfileModule,
        ReservationModule,
        CategoryModule,
        ServiceModule,
        BusinessModule,
        PwsModule
    ]
})
export class ClientModule {
}
