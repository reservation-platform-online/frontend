import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '@app/common/services/api/api.service';
import {apiPaths} from '@envi/api-paths';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';


@Injectable()
export class ClientApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
    ) {
    }

    postReport(params: {
        description: string
    }): Observable<any> {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.report,
                headersOptions: {
                    useContentType: true,
                },
                data: this.fromJsonToParametersConverter.convert(params)
            });
    }

    getPrivacyPolicy(): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.privacyPolicy
            });
    }
}
