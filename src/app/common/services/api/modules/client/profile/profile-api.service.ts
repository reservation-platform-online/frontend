import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {take, tap} from 'rxjs/operators';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {ToastController} from '@ionic/angular';


@Injectable()
export class ProfileApiService {


    constructor(
        private readonly apiService: ApiService,
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly toastController: ToastController,
    ) {
    }

    putUpdate(form: any): Observable<any> {

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.client.user.edit,
                data: this.jsonConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }),
                take(1)
            );
    }

    changePassword$(form: {
        password: string,
        newPassword: string,
        newPasswordRepeat: string
    }): Observable<any> {

        return this.apiService
            .request({
                method: 'put',
                url: apiPaths.client.user.changePassword,
                data: this.jsonConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Wykonano pomyślnie',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Błąd',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }),
            );
    }

    saveImage(image: File): Observable<any> {

        const formData = new FormData();

        formData.append('imageFile', image, '1');


        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.user.uploadImage,
                data: formData
            });


    }

    getProfile(): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.user.me,
                urlConfig: {
                    withVersion: true,
                },
            });
    }

    public connectExternal(data: {
        code: string;
        platform?: 'zoom'
    }): Observable<any> {
        return this.apiService
            .request(
                {
                    url: apiPaths.client.user.connectExternal,
                    method: 'post',
                    serializer: 'urlencoded',
                    headersOptions: {
                        useContentType: true,
                    },
                    data: this.jsonConverter.convert(data)
                }
            );
    }
}
