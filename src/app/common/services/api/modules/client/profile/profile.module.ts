import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {ProfileEffects} from '@app/ngrx-store/profile/profile.effects';
import {ProfileApiService} from '@app/common/services/api/modules/client/profile/profile-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([ProfileEffects])
    ],
    providers: [
        ProfileApiService
    ]
})
export class ProfileModule {

}
