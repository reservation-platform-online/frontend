import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {environment} from '@envi/environment';
import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;

@Injectable()
export class AuthenticationTokenService {
    private static readonly TOKEN_KEY = environment.keys.AUTH_TOKEN;

    private token: string;
    private readonly token$ = new ReplaySubject<string>(1);

    constructor() {
    }

    onToken(): Observable<string> {
        return this.token$.asObservable();
    }

    getTokenFromStorage(): void {
        Storage
            .get({
                key: AuthenticationTokenService.TOKEN_KEY
            })
            .then((storedToken) => {
                if (storedToken) {
                    this.token = storedToken.value;
                    this.token$.next(this.token);
                }
            });
    }

    setToken(token: string): void {
        this.token = token;
        this.token$.next(token);
        Storage
            .set({
                key: AuthenticationTokenService.TOKEN_KEY,
                value: token
            })
            .then(() => {

            });
    }

    removeToken(): void {
        Storage
            .remove({
                key: AuthenticationTokenService.TOKEN_KEY,
            })
            .then(() => this.token$.next(null));
        this.token = null;
    }

    getTokenName(): string {
        return AuthenticationTokenService.TOKEN_KEY;
    }
}
