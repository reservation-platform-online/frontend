import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import {AuthenticationTokenService} from './authentication-token.service';

import {Store} from '@ngrx/store';
import * as fromStore from '../../../../../../ngrx-store';
import * as fromProfile from '../../../../../../ngrx-store/profile/profile.actions';

import {ModalController, NavController} from '@ionic/angular';
import {LoadingService} from '../../../../util/loading/loading.service';
import {CommunicateForGuestComponent} from '@app/common/modules/client/communicate-for-guest/communicate-for-guest.component';
import {Reactive} from '@app/common/cdk/reactive';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';

@Injectable()
export class AuthenticationService extends Reactive implements OnDestroy {

    private readonly loggedIn$ = new BehaviorSubject<boolean>(false);

    constructor(
        private readonly navController: NavController,
        private readonly store: Store<fromStore.State>,
        private readonly authenticationTokenService: AuthenticationTokenService,
        private readonly workerIdService: WorkerIdService,
        private readonly companyIdService: CompanyIdService,
        private readonly loadingService: LoadingService,
        private readonly modalController: ModalController
    ) {
        super();
    }

    public async communicateForGuest() {

        const modal = await this.modalController.create({
            component: CommunicateForGuestComponent,
            id: 'communicate-for-guest',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            backdropDismiss: true,
        });
        return await modal.present();

    }

    public isLoggedIn(): Observable<boolean> {
        return this.loggedIn$.asObservable();
    }

    public logoutLocal(): void {

        this.authenticationTokenService.removeToken();
        this.workerIdService.removeId();
        this.companyIdService.removeId();

        this.store.dispatch(new fromProfile.Init());
        this.loadingService.dismissLoader().then(() => {

            this.loggedIn$.next(false);

        });

    }

    public loginLocal(token: string): void {

        this.loggedIn$.next(true);
        this.authenticationTokenService.setToken(token);
        this.loadingService.dismissLoader();

    }

}
