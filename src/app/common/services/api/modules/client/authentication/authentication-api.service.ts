import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {tap} from 'rxjs/operators';

import {ApiConfigService} from '../../../api-config.service';
import {apiPaths} from '@envi/api-paths';

import {Store} from '@ngrx/store';
import * as fromStore from '../../../../../../ngrx-store';

import {ModalController, NavController, ToastController} from '@ionic/angular';
import {ApiService} from '../../../api.service';
import {LoadingService} from '../../../../util/loading/loading.service';
import {AuthenticateInterface} from '@app/common/interfaces/modules/client/authentication/authenticate.interface';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';

@Injectable()
export class AuthenticationApiService {

    constructor(
        private readonly navController: NavController,
        private readonly apiConfig: ApiConfigService,
        private readonly store: Store<fromStore.State>,
        private readonly toastController: ToastController,
        private readonly authenticationService: AuthenticationService,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter,
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly loadingService: LoadingService,
        private readonly modalController: ModalController,
        private readonly apiService: ApiService
    ) {
    }

    public loginExternal(data: {
        code: string;
        platform?: 'zoom'
    }): Observable<any> {
        return this.apiService
            .request(
                {
                    url: apiPaths.client.auth.loginExternal,
                    method: 'post',
                    serializer: 'urlencoded',
                    headersOptions: {
                        useContentType: true,
                    },
                    data: this.jsonConverter.convert(data)
                }
            )
            .pipe(
                tap(
                    (token: string) => {
                        this.authenticationService.loginLocal(token);
                    },
                ),
            );
    }

    public login(authenticate: AuthenticateInterface): Observable<any> {
        return this.apiService
            .request(
                {
                    url: apiPaths.client.auth.login,
                    method: 'post',
                    serializer: 'urlencoded',
                    headersOptions: {
                        useContentType: true,
                    },
                    data: this.jsonConverter.convert(authenticate)
                }
            )
            .pipe(
                tap(
                    (token: string) => {
                        this.authenticationService.loginLocal(token);
                    },
                ),
            );
    }

    public logout(): Observable<any> {
        return this.apiService
            .request(
                {
                    url: apiPaths.client.user.logout,
                    method: 'delete',
                    headersOptions: {
                        useContentType: true
                    }
                }
            )
            .pipe(
                tap(_ => {
                    this.authenticationService.logoutLocal();
                }, _ => {
                    this.authenticationService.logoutLocal();
                })
            );
    }

    public confirm(param: string): Observable<any> {
        return this.apiService
            .request(
                {
                    url: apiPaths.client.auth.confirmEmail,
                    method: 'put',
                    headersOptions: {
                        useContentType: true
                    },
                    data: this.fromJsonToParametersConverter.convert({
                        verificationToken: param
                    }),
                }
            ).pipe(
                tap(() => {

                    this.toastController.create({
                        color: 'success',
                        message: 'Twój E-mail został potwierdzony!',
                        buttons: [
                            {
                                role: 'cancel',
                                icon: 'close-outline',
                            }
                        ],
                        duration: 5000
                    }).then((toast) => toast.present());

                }));
    }

}
