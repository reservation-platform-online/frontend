import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticationTokenService} from './authentication-token.service';
import {EffectsModule} from '@ngrx/effects';
import {HTTP} from '@ionic-native/http/ngx';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';
import {AuthenticationEffects} from '@app/ngrx-store/authentication/authentication.effects';
import {CommunicateForGuestModule} from '@app/common/modules/client/communicate-for-guest/communicate-for-guest.module';
import {AuthenticationApiService} from '@app/common/services/api/modules/client/authentication/authentication-api.service';


@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([AuthenticationEffects]),
        CommunicateForGuestModule
    ],
    providers: [
        HTTP,
        AuthenticationService,
        AuthenticationApiService,
        AuthenticationTokenService
    ]
})
export class AuthenticationModule {

}
