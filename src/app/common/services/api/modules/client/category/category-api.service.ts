import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {CategoryParamsInterface} from '@app/common/interfaces/modules/client/category/category-params.interface';


@Injectable()
export class CategoryApiService {


    constructor(
        private readonly apiService: ApiService
    ) {
    }

    getList(params: CategoryParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.categories.index,
                params
            });
    }
}
