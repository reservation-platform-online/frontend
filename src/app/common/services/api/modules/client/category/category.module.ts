import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {CategoryEffects} from '@app/ngrx-store/category/category.effects';
import {CategoryApiService} from '@app/common/services/api/modules/client/category/category-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([CategoryEffects])
    ],
    providers: [
        CategoryApiService
    ]
})
export class CategoryModule {

}
