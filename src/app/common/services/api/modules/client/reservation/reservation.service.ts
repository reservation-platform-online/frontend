import {Injectable} from '@angular/core';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {ModalController, ToastController} from '@ionic/angular';
import {Store} from '@ngrx/store';
import * as fromReservation from '@app/ngrx-store/reservation/reservation.actions';
import {ReservationFormComponent} from '@app/common/modules/client/reservation-form/reservation-form.component';
import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {DaySlotsInterface} from '@app/common/interfaces/modules/client/reservation/day-slots.interface';


@Injectable()
export class ReservationService {

    constructor(
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly toastController: ToastController,
        private readonly modalController: ModalController,
        private readonly store: Store
    ) {

    }

    async create($event: { day: DaySlotsInterface; slot: SlotInterface }, item: SearchItemInterface) {

        this.store.dispatch(new fromReservation.InitCreate({
            date: $event.day.date.format('YYYY-MM-DD'),
            end: `${$event.slot.end}`,
            item,
            start: `${$event.slot.start}`
        }));
        // this.navController.navigateForward(['client', 'reservation', 'create']);
        const modal = await this.modalController.create({
            component: ReservationFormComponent,
            id: 'create-reservation',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            backdropDismiss: true,
        });
        return await modal.present();

    }

}
