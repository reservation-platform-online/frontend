import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {ReservationEffects} from '@app/ngrx-store/reservation/reservation.effects';
import {ReservationApiService} from '@app/common/services/api/modules/client/reservation/reservation-api.service';
import {ReservationService} from '@app/common/services/api/modules/client/reservation/reservation.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([ReservationEffects])
    ],
    providers: [
        ReservationApiService,
        ReservationService
    ]
})
export class ReservationModule {

}
