import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {tap} from 'rxjs/operators';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {CreateInterface} from '@app/common/interfaces/modules/client/reservation/create.interface';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {ToastController} from '@ionic/angular';
import {CreateCommentInterface} from '@app/common/interfaces/modules/client/reservation/create-comment.interface';
import {ReservationParamsInterface} from '@app/common/interfaces/modules/client/reservation/reservation-params.interface';


@Injectable()
export class ReservationApiService {

    private readonly reservationIsCreated$ = new ReplaySubject<boolean>(1);

    constructor(
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly toastController: ToastController,
        private readonly apiService: ApiService
    ) {
    }

    onReservationIsCreated(): Observable<boolean> {
        return this.reservationIsCreated$.asObservable();
    }

    clearReservationIsCreated(): void {
        this.reservationIsCreated$.next(false);
    }

    getList(params?: ReservationParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.reservation.index,
                params
            });
    }

    postItem(payload: CreateInterface) {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.reservation.create,
                serializer: 'urlencoded',
                headersOptions: {
                    useContentType: true,
                },
                data: this.jsonConverter.convert(payload)
            }).pipe(
                tap(
                    () => {
                        this.reservationIsCreated$.next(true);
                        this.toastController.create({
                            color: 'success',
                            message: 'Rezerwacja została stworzona',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Rezerwacja nie została stworzona',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                        this.reservationIsCreated$.next(false);
                    }
                )
            );

    }

    createComment$(payload: CreateCommentInterface) {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.reservation.comment,
                serializer: 'urlencoded',
                headersOptions: {
                    useContentType: true,
                },
                data: this.jsonConverter.convert(payload)
            }).pipe(
                tap(
                    () => {
                        this.toastController.create({
                            color: 'success',
                            message: 'Komentarz został wysłany',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }, () => {
                        this.toastController.create({
                            color: 'danger',
                            message: 'Komentarz nie został wysłany',
                            buttons: [
                                {
                                    role: 'cancel',
                                    icon: 'close-outline',
                                }
                            ],
                            duration: 5000
                        }).then((toast) => toast.present());
                    }
                )
            );

    }

    comment$(params: {
        pwsId: string,
        reservationUserId: string
    }) {

        return this.apiService
            .request({
                url: apiPaths.client.reservation.comment,
                params
            });

    }

    deleteCancel(subUrl: string) {

        return this.apiService
            .request({
                method: 'delete',
                url: apiPaths.client.reservation.cancel,
                urlConfig: {
                    subUrl
                }
            });

    }
}
