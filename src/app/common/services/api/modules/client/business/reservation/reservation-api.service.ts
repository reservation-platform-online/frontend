import {Injectable} from '@angular/core';
import {ApiService} from '@app/common/services/api/api.service';
import {Observable} from 'rxjs';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {apiPaths} from '@envi/api-paths';
import {CreateInterface} from '@app/common/interfaces/modules/company/reservation/create.interface';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';


@Injectable()
export class ReservationApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly jsonConverter: FromJsonToParametersConverter,
    ) {
    }

    getList$(params: BusinessReservationParamsInterface): Observable<any> {

        delete params.categoryName;

        return this.apiService
            .request({
                url: apiPaths.company.reservations.index,
                params
            });
    }

    getItem$(params: BusinessReservationParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.pws.view,
                urlConfig: {
                    subUrl: params.itemId
                },
            });
    }

    postItem$(payload: CreateInterface) {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.worker.reservations.create,
                serializer: 'urlencoded',
                headersOptions: {
                    useContentType: true,
                },
                data: this.jsonConverter.convert(payload)
            });

    }


}
