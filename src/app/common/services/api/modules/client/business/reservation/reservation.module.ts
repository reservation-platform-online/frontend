import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {BusinessReservationEffects} from '@app/ngrx-store/business/reservation/reservation.effects';
import {ReservationApiService} from '@app/common/services/api/modules/client/business/reservation/reservation-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([BusinessReservationEffects])
    ],
    providers: [
        ReservationApiService
    ]
})
export class ReservationModule {

}
