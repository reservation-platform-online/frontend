import {Injectable} from '@angular/core';
import {ApiService} from '@app/common/services/api/api.service';
import {Observable} from 'rxjs';
import {BusinessPwsParamsInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-params.interface';
import {apiPaths} from '@envi/api-paths';


@Injectable()
export class PwsApiService {

    constructor(
        private readonly apiService: ApiService,
    ) {
    }

    getList(params: BusinessPwsParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.pws.index,
                params: {
                    expand: 'workTime',
                    ...params
                }
            });
    }

    getItem(params: BusinessPwsParamsInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.pws.view,
                urlConfig: {
                    subUrl: params.itemId
                },
            });
    }


}
