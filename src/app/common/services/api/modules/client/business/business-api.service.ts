import {Injectable} from '@angular/core';
import {ApiService} from '@app/common/services/api/api.service';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';


@Injectable()
export class BusinessApiService {

    constructor(
        private readonly apiService: ApiService,
    ) {
    }

    getItemCompany(): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.company.company.index
            });
    }

    getItemWorker(): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.worker.worker.index
            });
    }


}
