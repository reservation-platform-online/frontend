import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {BusinessEffects} from '@app/ngrx-store/business/business.effects';
import {BusinessApiService} from '@app/common/services/api/modules/client/business/business-api.service';
import {ReservationModule} from '@app/common/services/api/modules/client/business/reservation/reservation.module';
import {PwsModule} from '@app/common/services/api/modules/client/business/pws/pws.module';

@NgModule({
    imports: [
        CommonModule,
        ReservationModule,
        PwsModule,
        EffectsModule.forFeature([BusinessEffects])
    ],
    providers: [
        BusinessApiService
    ]
})
export class BusinessModule {

}
