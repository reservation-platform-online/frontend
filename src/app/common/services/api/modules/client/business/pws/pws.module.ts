import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {BusinessPwsEffects} from '@app/ngrx-store/business/pws/pws.effects';
import {PwsApiService} from '@app/common/services/api/modules/client/business/pws/pws-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([BusinessPwsEffects])
    ],
    providers: [
        PwsApiService
    ]
})
export class PwsModule {

}
