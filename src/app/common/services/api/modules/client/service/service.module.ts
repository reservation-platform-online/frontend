import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {ServiceEffects} from '@app/ngrx-store/service/service.effects';
import {ServiceApiService} from '@app/common/services/api/modules/client/service/service-api.service';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([ServiceEffects])
    ],
    providers: [
        ServiceApiService
    ]
})
export class ServiceModule {

}
