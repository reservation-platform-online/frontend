import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';


@Injectable()
export class ServiceApiService {

    constructor(private readonly apiService: ApiService) {
    }

    getList(params: {
        categoryId: number
    }): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.services.index,
                params
            });
    }
}
