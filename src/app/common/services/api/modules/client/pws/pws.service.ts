import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {SearchCommentsInterface} from '@app/common/interfaces/modules/client/pws/search-comments.interface';


@Injectable()
export class PwsService {

    constructor(
        private readonly apiService: ApiService,
    ) {
    }

    getList(params: SearchInterface): Observable<any> {

        const categoryId = params.category.id;
        delete params.category;

        return this.apiService
            .request({
                url: apiPaths.client.pws.index,
                params: {
                    ...params,
                    categoryId
                }
            });
    }

    getItem(params: SearchInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.pws.view,
                urlConfig: {
                    subUrl: params.itemId
                },
                params: {
                    expand: 'workTime'
                }
            });
    }

    getCalendar(params: SearchInterface): Observable<any> {

        return this.apiService
            .request({
                url: apiPaths.client.pws.calendar,
                urlConfig: {
                    subUrl: `${params.itemId}/calendar`
                },
                params: {
                    start: params.start,
                    end: params.end
                }
            });
    }

    getComments(params: SearchCommentsInterface): Observable<any> {

        const itemId = params.itemId;
        delete params.itemId;

        return this.apiService
            .request({
                url: apiPaths.client.pws.calendar,
                urlConfig: {
                    subUrl: `${itemId}/comments`
                },
                params
            });
    }
}
