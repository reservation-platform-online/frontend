import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';
import {PwsService} from '@app/common/services/api/modules/client/pws/pws.service';
import {PwsEffects} from '@app/ngrx-store/pws/pws.effects';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([PwsEffects])
    ],
    providers: [
        PwsService
    ]
})
export class PwsModule {

}
