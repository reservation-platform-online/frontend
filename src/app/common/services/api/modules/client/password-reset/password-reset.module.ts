import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PasswordResetApiService} from '@app/common/services/api/modules/client/password-reset/password-reset-api.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        PasswordResetApiService
    ]
})
export class PasswordResetModule {

}
