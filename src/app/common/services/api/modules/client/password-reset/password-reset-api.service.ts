import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {ApiService} from '@app/common/services/api/api.service';
import {AlertController, ToastController} from '@ionic/angular';

@Injectable()
export class PasswordResetApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly alertController: AlertController,
        private readonly toastController: ToastController,
    ) {
    }

    public requestPasswordReset$(email: string): Observable<any> {
        const paramEmail = 'email=' + encodeURIComponent(email);

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.auth.resetPassword,
                data: paramEmail,
                headersOptions: {
                    useContentType: true
                }
            });
    }

    public async forgotPassword(): Promise<void> {

        const alert = await this.alertController.create({
            cssClass: 'reservation-platform-basic-alert',
            header: 'Reset hasła',
            inputs: [{
                name: 'email',
                placeholder: 'E-mail',
                type: 'email'
            }],
            buttons: [{
                cssClass: 'cancel-button',
                text: 'Anuluj',
                handler: data => {
                }
            },
                {
                    cssClass: 'ok-button',
                    text: 'Wyślij',
                    handler: data => {

                        if (data.email && data.email.length > 0) {

                            const isCorrect = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(data.email);

                            if (isCorrect) {
                                this.requestPasswordReset$(data.email).subscribe((result) => {
                                    this.showToast(`Wykonano pomyślnie, sprawdź swój E-mail - ${data.email}`);
                                    alert.dismiss();
                                }, () => {
                                    this.showToast('Spróbuj ponownie', 'warning');
                                });
                            } else {
                                this.showToast('E-mail - nie prawidłowy', 'danger');
                            }

                        } else {
                            this.showToast('Wpisz E-mail', 'danger');
                        }

                        return false;

                    }

                }

            ]
        });

        alert.present();

    }

    private showToast(
        message: string,
        color: string = 'success'
    ): void {

        this.toastController.create({
            color,
            message,
            buttons: [
                {
                    role: 'cancel',
                    icon: 'close-outline',
                }
            ],
            duration: 5000
        }).then((toast) => toast.present());
    }

}
