import {Injectable} from '@angular/core';
import {ApiConfigService} from '@app/common/services/api/api-config.service';
import {apiPaths} from '@envi/api-paths';
import {Observable} from 'rxjs';
import {take, tap} from 'rxjs/operators';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {ApiService} from '@app/common/services/api/api.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {AgreementInterface} from '@app/common/interfaces/modules/client/registration/agreement.interface';

@Injectable()
export class RegistrationApiService {

    constructor(
        private readonly apiService: ApiService,
        private readonly apiConfig: ApiConfigService,
        private readonly alertPromptService: AlertPromptService,
        private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
    ) {
    }

    getAgreementList(): Observable<AgreementInterface[]> {

        return this.apiService
            .request({
                url: apiPaths.client.auth.agreements,
            });
    }

    registerAccount(form: any): Observable<any> {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.auth.registration,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(() => {
                        this.alertPromptService.presentAlert('Wykonano pomyślnie.');
                    }),
                take(1)
            );
    }

    registerAccountAndCompany(form: any): Observable<any> {

        return this.apiService
            .request({
                method: 'post',
                url: apiPaths.client.auth.registrationCompany,
                data: this.fromJsonToParametersConverter.convert(form),
                headersOptions: {
                    useContentType: true
                }
            })
            .pipe(
                tap(() => {
                        this.alertPromptService.presentAlert('Wykonano pomyślnie.');
                    }),
                take(1)
            );
    }

}
