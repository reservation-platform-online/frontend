import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegistrationApiService} from '@app/common/services/api/modules/client/registration/registration-api.service';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        RegistrationApiService
    ]
})
export class RegistrationModule {

}
