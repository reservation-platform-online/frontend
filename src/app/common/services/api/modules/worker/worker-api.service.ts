import {Injectable} from '@angular/core';
import {ApiService} from '@app/common/services/api/api.service';
import {ApiConfigService} from '@app/common/services/api/api-config.service';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {Observable} from 'rxjs';
import {apiPaths} from '@envi/api-paths';
import {take, tap} from 'rxjs/operators';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class WorkerApiService {

  constructor(
      private readonly apiService: ApiService,
      private readonly apiConfig: ApiConfigService,
      private readonly toastController: ToastController,
      private readonly fromJsonToParametersConverter: FromJsonToParametersConverter
  ) {

  }

  saveImage(image: File): Observable<any> {

    const formData = new FormData();

    formData.append('imageFile', image, '1');


    return this.apiService
        .request({
          method: 'post',
          url: apiPaths.worker.worker.uploadImage,
          data: formData
        });


  }

  putUpdate(form: any): Observable<any> {

    return this.apiService
        .request({
          method: 'put',
          url: apiPaths.worker.worker.update,
          data: this.fromJsonToParametersConverter.convert(form),
          headersOptions: {
            useContentType: true
          }
        })
        .pipe(
            tap(
                () => {
                  this.toastController.create({
                    color: 'success',
                    message: 'Wykonano pomyślnie',
                    buttons: [
                      {
                        role: 'cancel',
                        icon: 'close-outline',
                      }
                    ],
                    duration: 5000
                  }).then((toast) => toast.present());
                }, () => {
                  this.toastController.create({
                    color: 'danger',
                    message: 'Błąd',
                    buttons: [
                      {
                        role: 'cancel',
                        icon: 'close-outline',
                      }
                    ],
                    duration: 5000
                  }).then((toast) => toast.present());
                }),
            take(1)
        );
  }

  putToggleEnable(): Observable<any> {

    return this.apiService
        .request({
          method: 'put',
          url: apiPaths.worker.worker.toggleEnable,
          headersOptions: {
            useContentType: true
          }
        })
        .pipe(
            tap(
                () => {
                  this.toastController.create({
                    color: 'success',
                    message: 'Wykonano pomyślnie',
                    buttons: [
                      {
                        role: 'cancel',
                        icon: 'close-outline',
                      }
                    ],
                    duration: 5000
                  }).then((toast) => toast.present());
                }, () => {
                  this.toastController.create({
                    color: 'danger',
                    message: 'Błąd',
                    buttons: [
                      {
                        role: 'cancel',
                        icon: 'close-outline',
                      }
                    ],
                    duration: 5000
                  }).then((toast) => toast.present());
                }),
            take(1)
        );
  }

  getItem(): Observable<any> {

      return this.apiService
          .request({
              url: apiPaths.worker.worker.index
          });
  }


}
