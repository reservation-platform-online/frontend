import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';
import {WorkerApiService} from '@app/common/services/api/modules/worker/worker-api.service';


@NgModule({
    providers: [
        WorkerApiService,
        WorkerIdService
    ],
    imports: [
        CommonModule
    ]
})
export class WorkerModule {
}
