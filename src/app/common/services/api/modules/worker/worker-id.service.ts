import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {environment} from '@envi/environment';
import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;

@Injectable()
export class WorkerIdService {
    private static readonly ID_KEY = environment.keys.WORKER_ID;

    private id: string;
    private readonly id$ = new ReplaySubject<string>(1);

    constructor() {
    }

    onId(): Observable<string> {
        return this.id$.asObservable();
    }

    getIdFromStorage(): void {
        Storage
            .get({
                key: WorkerIdService.ID_KEY
            })
            .then((storedId) => {
                if (storedId) {
                    this.id = storedId.value;
                    this.id$.next(this.id);
                }
            });
    }

    setId(id: string): void {
        this.id = id;
        this.id$.next(id);
        Storage
            .set({
                key: WorkerIdService.ID_KEY,
                value: id
            })
            .then(() => {

            });
    }

    removeId(): void {
        Storage
            .remove({
                key: WorkerIdService.ID_KEY
            })
            .then(() => this.id$.next(null));
        this.id = null;
    }

    getIdName(): string {
        return WorkerIdService.ID_KEY;
    }
}
