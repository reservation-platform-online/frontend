import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiConfigService} from '@app/common/services/api/api-config.service';
import {Platform, ToastController} from '@ionic/angular';
import {HTTP} from '@ionic-native/http/ngx';
import {ReplaySubject} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';
import {FromJsonToParametersConverter} from '@app/common/services/util/from-json-to-parameters.converter';
import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';
import {ApiErrorHandler} from '@app/common/services/api/api-error-handler';
import {RequestOptionsInterface} from '@app/common/interfaces/api/request-options.interface';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {environment} from '@envi/environment';
import {Logout} from '@app/ngrx-store/authentication/authentication.actions';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Injectable()
export class ApiService extends ApiErrorHandler {

    static idToastDisconnect = 'ToastDisconnect';

    private readonly isNative: boolean = false;

    public getIsNative(): boolean {
        return this.isNative;
    }

    constructor(
        private readonly apiConfigService: ApiConfigService,
        private readonly platform: Platform,
        private readonly httpClient: HttpClient,
        private readonly loadingService: LoadingService,
        private readonly jsonConverter: FromJsonToParametersConverter,
        private readonly nativeHttp: HTTP,
        private readonly toastController: ToastController,
        private readonly store: Store<fromStore.State>,
        authenticationService: AuthenticationService,
        alertPrompt: AlertPromptService
    ) {
        super(authenticationService, alertPrompt);
        this.isNative = this.platform.is('mobileweb') ? false : this.platform.is('ios') || this.platform.is('android');
    }

    /**
     * @param options
     */
    request(options: RequestOptionsInterface) {

        const request$: ReplaySubject<any> = new ReplaySubject<any>(1);

        if (!options.method) {
            options.method = 'get';
        }

        options.url = this.apiConfigService.getUrl(options.url, options.urlConfig);

        const checkParams = (params) => {
            for (const key of Object.keys(params)) {
                if ((typeof params[key]) === 'object' && params[key]?.length > 0) {
                    params[`${key}[]`] = params[key];
                }
            }
            return params;
        };

        const localOptions: any = {
            headers: this.apiConfigService.buildHeaders(),
            observe: 'response',
            data: {},
            ...({
                headers: this.apiConfigService.buildHeaders(options.headersOptions),
                ...(options.data ? {data: options.data} : {}),
                ...(options.serializer ? {serializer: options.serializer} : {}),
                ...(options.responseType ? {responseType: options.responseType} : {}),
                ...(options.params ? {params: checkParams(options.params)} : {}),
            }),
        };

        const makeRequest = () => {

            if (this.isNative) {

                delete localOptions.observe;
                localOptions.method = options.method;

                if (localOptions.data && (typeof localOptions.data) === 'string') {
                    localOptions.data = this.jsonConverter.reconvert(localOptions.data);
                }

                this.nativeHttp.sendRequest(options.url, localOptions).then((result) => {
                    request$.next(result);
                }, (error) => {
                    request$.error(error);
                }).catch((error) => {
                    this.checkError(error);
                    request$.error(error);
                }).finally(() => {
                    request$.complete();
                });

            } else {

                if (localOptions.data) {

                    localOptions.body = options.data;
                    delete localOptions.data;
                }

                if (localOptions.serializer) {
                    delete localOptions.serializer;
                }

                this.httpClient.request(options.method, options.url, localOptions).pipe(this.takeUntil()).subscribe((result) => {
                    request$.next(result);
                }, (error) => {
                    this.checkError(error);
                    request$.error(error);
                }, () => {
                    request$.complete();
                });

            }

        };

        if (!isNotNullOrUndefined(options.showLoader)) {

            const allowedMethodsType = ['post', 'put', 'upload', 'download'];
            options.showLoader = allowedMethodsType.findIndex((value) => value === options.method) > -1;

        }

        if (options.showLoader) {

            this.loadingService.presentLoadingWithOptions().then(() => {
                makeRequest();
            });

        } else {

            makeRequest();

        }

        return request$.asObservable().pipe(
            map((result) => {
                if (result) {
                    if (result.data) {
                        if (typeof result.data === 'string') {
                            try {
                                return JSON.parse(result.data);
                            } catch (e) {
                                return result.data;
                            }
                        }
                        return result.data;
                    }
                    if (result.body) {
                        if (typeof result.body === 'string') {
                            try {
                                return JSON.parse(result.body);
                            } catch (e) {
                                return result.body;
                            }
                        }
                        return result.body;
                    }
                }
            }),
            tap(() => {
                    this.loadingService.dismissLoader();
                },
                () => {
                    this.loadingService.dismissLoader();
                },
                () => {
                    this.loadingService.dismissLoader();
                }),
            take(1)
        );

    }

    private checkError(error): void {

        if (!environment.production) {
            console.error('Request has error', error);
        }
        this.loadingService.dismissLoader();
        if (this.checkConnect(error.status)) {
            if (this.check500(error.status)) {
                if (error.status === 401) {
                    this.store.dispatch(new Logout());
                } else {
                    this.handleError(error);
                }
            }
        }

    }

    private checkConnect(status): boolean {

        if ([504, -3, 0].includes(status)) {

            this.toastController.getTop().then((toast) => {
                if (toast && toast.id === ApiService.idToastDisconnect) {

                } else {

                    this.toastController.create({
                        message: 'Brak połączenia ze serwerem',
                        color: 'warning',
                        position: 'top',
                        buttons: [
                            {
                                role: 'cancel',
                                icon: 'close-outline',
                            }
                        ],
                        id: ApiService.idToastDisconnect,
                        duration: 3000,
                    }).then((newToast) => {
                        newToast.present();
                    });

                }

            });

            return false;

        } else {
            return true;
        }

    }

    private check500(status): boolean {

        if ([500].includes(status)) {

            this.toastController.getTop().then((toast) => {
                if (toast && toast.id === ApiService.idToastDisconnect) {

                } else {

                    this.toastController.create({
                        message: 'Wystąpił nieoczekiwany błąd',
                        color: 'warning',
                        position: 'top',
                        buttons: [
                            {
                                role: 'cancel',
                                icon: 'close-outline',
                            }
                        ],
                        id: ApiService.idToastDisconnect,
                        duration: 3000,
                    }).then((newToast) => {
                        newToast.present();
                    });

                }

            });

            return false;

        } else {
            return true;
        }
    }
}
