import {HttpErrorResponse} from '@angular/common/http';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {Reactive} from '@app/common/cdk/reactive';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';


export abstract class ApiErrorHandler extends Reactive {

    protected constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly alertPrompt: AlertPromptService
    ) {
        super();
    }

    protected handleError(error: HttpErrorResponse): void {
        if (error.status === 401) {
            this.authenticationService.logoutLocal();
            throw error;
        } else if (error.status === 400) {
            this.showBadRequestMessage(error);
        } else if (error.status === 406) {
            this.handleInvalidVersionError(error.error);
        } else if (error.status === 504) {
            // this.show504(error, options, request$);
        } else if (error.error && error.error.error) {
            this.showPrompt(error.error.error);
        } else if (error && error.status) {
            this.showPrompt(error.status.toString());
        } else {
            this.showPrompt(error.toString());
        }
    }

    protected handleInvalidVersionError(response): void {
        console.error(response);
    }

    protected getAlertPrompt(): AlertPromptService {
        return this.alertPrompt;
    }

    protected showPrompt(text: string): void {
        this.alertPrompt.presentAlert(text);
    }

    private showBadRequestMessage(error: HttpErrorResponse): void {
        let errorMessage = '';

        if (typeof error.error === 'string') {
            errorMessage = error.error;
        } else {
            errorMessage = Object.values(error.error)[1] as string;
        }
        if (errorMessage) {
            if (errorMessage.indexOf('username') > -1) {
                const authErrorText = 'Błędne hasło i/lub login';
                this.showPrompt(authErrorText);
            } else {
                this.showPrompt(errorMessage);
            }
        } else if (error.error && error.error.error) {
            this.showPrompt(error.error.error);
        } else if (Object.keys(error.error).length > 0) {
            for (const errorItem of Object.keys(error.error)) {
                if ((typeof error.error[errorItem]) === 'object' && Object.keys(error.error[errorItem]).length > 0) {
                    for (const errorSubItem of Object.keys(error.error[errorItem])) {
                        this.showPrompt(error.error[errorItem][errorSubItem]);
                    }
                } else if ((typeof error.error[errorItem]) === 'string') {
                    this.showPrompt(error.error[errorItem]);
                }
            }
        }
    }

}
