import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '@envi/environment';
import {Reactive} from '../../cdk/reactive';
import {AuthenticationTokenService} from './modules/client/authentication/authentication-token.service';
import {Device} from '@ionic-native/device/ngx';
import * as DeviceUUID from 'device-uuid';
import {Platform} from '@ionic/angular';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';

@Injectable()
export class ApiConfigService extends Reactive implements OnDestroy {

    private token: string;
    private companyId: string;
    private workerId: string;

    private readonly isNative: boolean;

    constructor(
        private readonly authToken: AuthenticationTokenService,
        private readonly companyIdService: CompanyIdService,
        private readonly workerIdService: WorkerIdService,
        private readonly platform: Platform,
        private readonly device: Device
    ) {
        super();
        this.isNative = this.platform.is('mobileweb') ? false : this.platform.is('ios') || this.platform.is('android');
        this.authToken
            .onToken()
            .pipe(this.takeUntil())
            .subscribe((token: string) => {
                this.token = token;
            });
        this.companyIdService
            .onId$()
            .pipe(this.takeUntil())
            .subscribe((companyId: string) => {
                this.companyId = companyId;
            });
        this.workerIdService
            .onId()
            .pipe(this.takeUntil())
            .subscribe((workerId: string) => {
                this.workerId = workerId;
            });
    }

    getUrl(url: string,
           config?: {
               subUrl?: string;
               withVersion?: boolean,
               cityId?: number
           }): string {

        let link = environment.host + url;

        if (!environment.production && !this.isNative) {

            link = environment.hostProxy + url;

        }

        if (config) {

            if (config.subUrl) {
                link += config.subUrl;
            }

            if (config.withVersion) {

                if (config.subUrl) {
                    link += '&';
                } else {
                    link += '?';
                }

                if (config.cityId) {
                    link += `&city_id=${config.cityId}`;
                }
            }
        }

        return link;
    }

    buildHeaders(options?: {
        useContentType?: boolean;
        disableCache?: boolean;
        useContentTypeJson?: boolean;
    }) {

        const headers = {
            'Client-Id': environment.clientId,
            'Device-Uuid': this.isNative ? this.device.uuid : new DeviceUUID.DeviceUUID().get()
        };

        if (options) {

            if (options.useContentType) {
                headers['Content-Type'] = 'application/x-www-form-urlencoded';
            }

            if (options.useContentTypeJson) {
                headers['Content-Type'] = 'application/json';
            }

            if (options.disableCache) {
                headers['Cache-Control'] = 'no-cache';
                headers['Pragma'] = 'no-cache';
                headers['Expires'] = 'Sat, 01 Jan 2000 00:00:00 GMT';
            }

        }

        if (this.token) {
            headers['Authorization'] = `Bearer ${this.token}`;
        }

        if (this.companyId) {
            headers['Company-Id'] = this.companyId;
        }

        if (this.workerId) {
            headers['Worker-Id'] = this.workerId;
        }

        return headers;
    }

}
