import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    Renderer2,
    SimpleChanges
} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ReservationListInterface} from '@app/common/interfaces/modules/company/reservation/reservation-list.interface';
import {Observable} from 'rxjs';
import {Reactive} from '@app/common/cdk/reactive';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {Store} from '@ngrx/store';
import {ReservationInterface} from '@app/common/interfaces/modules/company/reservation/reservation.interface';
import {ReservationUserInterface} from '@app/common/interfaces/modules/client/reservation/reservation-user.interface';
import * as moment from 'moment';
import {DaySlotsInterface} from '@app/common/interfaces/modules/client/reservation/day-slots.interface';
import {ReservationService} from '@app/common/services/api/modules/client/reservation/reservation.service';
import {AuthenticationService} from '@app/common/services/api/modules/client/authentication/authentication.service';
import {SlotInterface} from '@app/common/interfaces/modules/client/reservation/slot.interface';
import {map, tap} from 'rxjs/operators';
import {ReservationFormComponent} from '@app/common/modules/client/reservation-form/reservation-form.component';
import {Tools} from '@app/common/tools/tools';

@Component({
    selector: 'app-reservation-list-with-days',
    templateUrl: './reservation-list-with-days.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReservationListWithDaysComponent extends Reactive implements OnInit, OnChanges {

    @Input()
    public reservationList$: Observable<ReservationListInterface>;

    @Input()
    daysCount: number = 7;

    @Input()
    widthDay: number = 50;

    @Input()
    inModal: boolean = false;

    @Output()
    refresh = new EventEmitter();

    @Output()
    onStartDate = new EventEmitter();

    @Output()
    onEndDate = new EventEmitter();

    public startDate: moment.Moment;
    public endDate: moment.Moment;

    days: DaySlotsInterface[];

    dayConfig: {
        width: string,
        textAlign: string,
    } = {
        width: `100%`,
        textAlign: 'center'
    };

    public currentDate: moment.Moment;
    public namesOfDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    private content;
    private prevBtn;
    private nextBtn;
    private loader;
    private daysWithSlots;

    private initialize: boolean = false;
    private reservationList: ReservationListInterface;

    private selectedReservation: ReservationInterface;

    private static isToday(date: moment.Moment): boolean {
        return moment().isSame(moment(date), 'day');
    }

    private static isTomorrow(date: moment.Moment): boolean {
        return ReservationListWithDaysComponent.isToday(date.clone().subtract(1));
    }

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly modalController: ModalController,
        private readonly store: Store,
        private readonly alertPromptService: AlertPromptService,
        private readonly elementRef: ElementRef,
        private readonly reservationService: ReservationService,
        private readonly authenticationService: AuthenticationService,
        private readonly renderer2: Renderer2,
    ) {
        super();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.reservationList$) {

            this.reservationList$ = this.reservationList$.pipe(
                this.takeUntil(),
                map((reservationList) => Tools.copyObject(reservationList)),
                tap((reservationList: ReservationListInterface) => {

                    if (reservationList) {

                        this.reservationList = reservationList;

                    }

                    if (this.initialize) {

                        this.generateCalendar();

                }

            })) as Observable<ReservationListInterface>;

            this.changeDetectorRef.detectChanges();

        }
    }

    ngOnInit() {

        moment.locale('pl');
        this.namesOfDays = moment.weekdaysShort(true);
        this.currentDate = moment().locale('pl');

        setTimeout(() => {

            this.content = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content');

            this.daysCount = Math.floor((this.content.clientWidth - 36) / this.widthDay);

            const endDate = moment(this.currentDate).add(this.daysCount - 1, 'days');

            this.onStartDate.emit(this.currentDate);
            this.onEndDate.emit(endDate);

            this.prevBtn = this.elementRef.nativeElement.querySelector('.calendar-days-slots__header__prev-day');
            this.nextBtn = this.elementRef.nativeElement.querySelector('.calendar-days-slots__header__next-day');
            this.loader = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content__loader');
            this.daysWithSlots = this.elementRef.nativeElement.querySelector('.calendar-days-slots__content__days-with-slots');

            this.generateCalendar();

            this.initialize = true;

        }, 1000);

    }

    trackByFn(reservation: any) {
        return reservation && reservation.id;
    }

    getColorByStatus(status: number) {
        return ['danger', 'primary', 'success'][status];
    }

    openUser(user: ReservationUserInterface) {

        console.log(user);

        // this.modalController.create({
        //     component: UserModalComponent,
        //     componentProps: {
        //         user: user
        //     },
        //     cssClass: 'ion-page-without-padding-top'
        // }).then((modal) => {
        //     modal.present();
        // });

    }

    private generateCalendar(): void {

        this.days = [];
        const weekStart = this.currentDate.startOf('day');

        for (let i = 0; i <= this.daysCount - 1; i++) {

            const date = moment(weekStart).add(i, 'day');
            const dayName = ReservationListWithDaysComponent.isToday(date) ? '<strong>DZIŚ</strong>' : ReservationListWithDaysComponent.isTomorrow(date) ? 'Jutro' : date.format('ddd');
            const dayNumber = date.format('DD.MM');

            this.days.push({
                date,
                dayName,
                dayNumber,
                reservations: this.getReservationList(date)
            });

        }

        this.renderer2.removeClass(this.loader, 'show');
        this.renderer2.addClass(this.daysWithSlots, 'show');

        this.changeDetectorRef.detectChanges();

    }

    nextDays() {

        // TODO max reservation date check

        this.renderer2.removeClass(this.daysWithSlots, 'show');
        this.renderer2.addClass(this.loader, 'show');
        this.currentDate = moment(this.currentDate).add(this.daysCount, 'days');

        const endDate = moment(this.currentDate).add(this.daysCount - 1, 'days');

        this.onStartDate.emit(this.currentDate);
        this.onEndDate.emit(endDate);

    }

    prevDays() {

        this.renderer2.removeClass(this.daysWithSlots, 'show');
        this.renderer2.addClass(this.loader, 'show');

        this.currentDate = moment(this.currentDate).subtract(this.daysCount, 'days');

        const endDate = moment(this.currentDate).add(this.daysCount - 1, 'days');

        this.onStartDate.emit(this.currentDate);
        this.onEndDate.emit(endDate);

    }


    /**
     *
     * @param day
     * @param slot
     */
    async selectSlot(day: DaySlotsInterface, slot: SlotInterface) {

        console.log(day, slot);

    }

    /**
     *
     * trackByFn
     */

    /**
     *
     * @param item
     */
    trackByDayFn(item: any) {
        return item && item.start;
    }

    private getReservationList(day: moment.Moment) {

        let list = [];

        if (this.reservationList) {

            list = this.reservationList.models.filter((reservation) => {
                return moment(reservation.date).isSame(day);
            });

        }

        return list;

    }

    selectReservation(reservation: ReservationInterface) {
        this.selectedReservation = reservation;
        this.modalController.create({
            component: ReservationFormComponent,
            id: 'view-reservation',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            backdropDismiss: true,
            componentProps: {
                isBusiness: true,
                reservation,
            }
        }).then((modal) => {
            modal.present();
            modal.onDidDismiss().then(() => {
                // TODO check if is was changed
               this.refresh.emit();
            });
        });
    }

}
