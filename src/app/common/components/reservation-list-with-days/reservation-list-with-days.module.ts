import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {ReservationCardModule} from '@app/common/components/reservation-card/reservation-card.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {ReservationListWithDaysComponent} from '@app/common/components/reservation-list-with-days/reservation-list-with-days.component';
import {ImgModule} from '@app/common/ui/img/img.module';
import {FilterComponent} from '@app/common/components/reservation-list-with-days/filter/filter.component';
import {InputModule} from '@app/common/ui/input/input.module';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        ReservationListWithDaysComponent,
        FilterComponent
    ],
    exports: [
        ReservationListWithDaysComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        StringTimeConvertModule,
        ReservationStatusModule,
        ReservationCardModule,
        DefaultContainerModule,
        ImgModule,
        InputModule,
        ReactiveFormsModule
    ]
})
export class ReservationListWithDaysModule {
}
