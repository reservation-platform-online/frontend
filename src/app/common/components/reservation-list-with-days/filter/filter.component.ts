import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html'
})
export class FilterComponent implements OnInit {

  @Input()
  filter: {
    serviceId?: string,
    status?: number,
    charge?: number,
    categoryId?: number
  };

  public form: FormGroup;

  constructor(
      private readonly formBuilder: FormBuilder,
      private readonly modalController: ModalController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  save() {

    this.dismissModal(this.form.value);

  }

  private initForm() {

    this.form = this.formBuilder.group({
      status: [null],
      charge: [null],
    });

    if (this.filter) {

      for (const key of Object.keys(this.filter)) {

        this.form.controls[key].setValue(this.filter[key]);

      }

    }

  }

  async dismissModal(data = null) {
    await this.modalController.dismiss(data, 'cancel');
  }

}
