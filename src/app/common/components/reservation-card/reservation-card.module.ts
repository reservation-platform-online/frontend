import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkerComponent} from '@app/common/modules/client/worker/worker.component';
import {IonicModule} from '@ionic/angular';
import {BarRatingModule} from 'ngx-bar-rating';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {RouterModule} from '@angular/router';
import {ReservationCardComponent} from '@app/common/components/reservation-card/reservation-card.component';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';
import {ImgModule} from '@app/common/ui/img/img.module';



@NgModule({
  declarations: [ReservationCardComponent],
  exports: [
      ReservationCardComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        BarRatingModule,
        StringTimeConvertModule,
        RouterModule,
        ReservationStatusModule,
        ImgModule
    ]
})
export class ReservationCardModule { }
