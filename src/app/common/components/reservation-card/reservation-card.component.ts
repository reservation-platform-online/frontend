import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ReservationInterface} from '@app/common/interfaces/modules/client/reservation/reservation.interface';
import {ROLE} from '@app/common/interfaces/api/role.enum';
import {RESERVATION_STATUS} from '@app/common/services/api/modules/client/reservation/reservation.status';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Store} from '@ngrx/store';
import * as moment from 'moment';
import {OpenComment} from '@app/ngrx-store/reservation/reservation.actions';

@Component({
  selector: 'app-reservation-component',
  templateUrl: './reservation-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReservationCardComponent implements OnInit {

  @Input()
  role: ROLE = ROLE.CLIENT;

  @Input()
  item: ReservationInterface;

  @Input()
  isHistory: boolean = false;

  @Input()
  commentIsBlocked: boolean = false;

  @Input()
  autoCheckCommentIsBlocked: boolean = true;

  @Output()
  open = new EventEmitter<ReservationInterface>();

  @Output()
  openCalendar = new EventEmitter<boolean>();

  @Output()
  comment = new EventEmitter<ReservationInterface>();

  public readonly roles = ROLE;

  public timer: Date;

  public hideTimer: boolean = false;

  constructor(
      private readonly navService: NavService,
      private readonly store: Store,
      private readonly changeDetectorRef: ChangeDetectorRef,
      private readonly modalController: ModalController,
      private readonly navController: NavController,
      private readonly screenSizeService: ScreenSizeService
  ) {
  }

  onIsMobile(): Observable<boolean> {
    return this.screenSizeService.isMobileView$;
  }

  ngOnInit() {

    clearInterval();

    if (this.item) {

      if (this.item.paymentIsImportant) {

        // TODO need finish is not good!

        const time = this.item.timerTimeForPay.split(':');

        const now = moment().locale('pl');

        const expires = moment(this.item.createdAt).locale('pl');
        expires.add(Number(time[0]), 'hours');
        expires.add(Number(time[1]), 'minutes');
        expires.add(Number(time[2]), 'seconds');

        if (now.isAfter(expires)) {

          this.hideTimer = true;

        } else {

          now.add(1, 'hours');

          this.timer = new Date(expires.diff(now));

        }


      }

      if (this.autoCheckCommentIsBlocked) {
        this.commentIsBlocked = ![
          RESERVATION_STATUS.STATUS_FINISHED,
        ].includes(this.item.status);
      }

    }

  }

  openReservation() {
    this.open.emit(this.item);
  }

  createComment() {
    this.comment.emit(this.item);
  }

  colorStatus() {
    if (this.item) {
      if (isNotNullOrUndefined(this.item.status)) {
        switch (this.item.status) {
          case RESERVATION_STATUS.STATUS_USER_CANCELED:
            return 'danger';
          case RESERVATION_STATUS.STATUS_ENABLED:
            return 'primary';
          case RESERVATION_STATUS.STATUS_CONFIRMED:
            return 'success';
          case RESERVATION_STATUS.STATUS_FINISHED:
            return 'success';
          case RESERVATION_STATUS.STATUS_RECEPTION_CANCELED:
            return 'danger';
          case RESERVATION_STATUS.STATUS_PAYMENT_TIME_OUT:
            return 'danger';
          case RESERVATION_STATUS.STATUS_USER_WORKER_DID_NOT_COME:
            return 'danger';
        }
      }
    }

    return 'default';
  }

  reCreate() {
    this.openCalendar.emit(true);
    this.store.dispatch(new fromPws.Params({
      itemId: this.item.pwsId
    }));
    this.store.dispatch(new fromPws.RefreshItem());
  }

  openComment() {
    this.store.dispatch(new OpenComment({
      pwsId: this.item.pwsId,
      reservationUserId: this.item.reservationUser.id,
    }));
  }

  openPws() {
    this.navController.navigateForward(['/category', this.item.service.categoryName, 'service', this.item.pwsId]);
  }
}
