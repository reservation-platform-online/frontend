import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {AgreementsComponent} from '@app/common/components/agreements/agreements.component';
import {IonicModule} from '@ionic/angular';
import {InputModule} from '@app/common/ui/input/input.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        InputModule
    ],
    declarations: [
        AgreementsComponent
    ],
    exports: [
        AgreementsComponent
    ]
})
export class AgreementsModule {

}
