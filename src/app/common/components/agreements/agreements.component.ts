import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {FormItemCheckerTool} from '@app/common/tools/form-item-checker.tool';
import {AgreementInterface} from '@app/common/interfaces/modules/client/registration/agreement.interface';
import {Reactive} from '@app/common/cdk/reactive';
import {BinaryTrueValidator} from '@app/common/tools/validators.tools';

@Component({
    selector: 'app-agreements',
    template: `
        <ng-container *ngIf="agreements">

            <form [formGroup]="agreementsForm">

                <div *ngFor="let agreement of agreements; let index = index"
                     formArrayName="agreements">

                    <div [formGroupName]="index">

                        <ion-list lines="none">

                            <app-input [placeholder]="agreement.title"
                                       [position]="undefined"
                                       lines="none"
                                       [isRequired]="agreement.isRequired"
                                       type="toggle"
                                       id="req-checkbox-{{agreement.id}}"
                                       [formControlName]="'checked'">
                            </app-input>

                            <ion-item>

                                <ion-text color="medium" [innerHTML]="agreement.description">

                                </ion-text>

                            </ion-item>

                        </ion-list>

                    </div>

                </div>
            </form>

        </ng-container>
    `,
    encapsulation: ViewEncapsulation.None,
    host: {
        '[class.hidden]': 'hidden'
    }
})
export class AgreementsComponent extends Reactive implements OnChanges {

    // [inputInvalid]="isInvalid(index.toString(), 'checked')"

    @Input()
    hidden: boolean;

    @Input()
    agreements: AgreementInterface[];

    @Input()
    accountAgreements: AgreementInterface[];

    @Input()
    submitted: boolean;

    @Output()
    onValue = new EventEmitter();

    @Output()
    agreementsValid = new EventEmitter<boolean>();

    agreementsForm: FormGroup;

    constructor(
        private readonly formBuilder: FormBuilder,
    ) {
        super();
        this.agreementsForm = this.formBuilder.group({
            agreements: this.formBuilder.array([])
        });
        this.agreementsForm.valueChanges.pipe(this.takeUntil()).subscribe((form) => {
            if (form['agreements']?.length === this.agreements?.length) {
                const agreements = form['agreements'];
                for (const agreement of agreements) {
                    agreement['checked'] = !!agreement['checked'] ? 1 : 0;
                }
                this.onValue.emit(JSON.stringify(agreements));
                this.updateValidity();
            }
        });
    }

    get agreementsArrayForm(): FormArray {
        return this.agreementsForm.get('agreements') as FormArray;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.submitted) {
            (this.agreementsForm.controls['agreements'] as FormArray).controls.forEach((a) => {
                a.markAllAsTouched();
                a.updateValueAndValidity();
            });
        }
        if (changes.agreements) {
            if (this.agreements) {
                this.createAgreementsArray(this.agreements);
            }
        }

        if (changes.accountAgreements) {
            if (this.accountAgreements) {
                this.agreements = this.accountAgreements;
                this.createAgreementsArray(this.accountAgreements);
            }
        }
    }

    isInvalid(formControlName: string, fieldName: string): boolean {
        const agreement = this.agreementsArrayForm.controls[formControlName];
        return FormItemCheckerTool.isInvalid(this.submitted, agreement, fieldName);
    }

    private updateValidity(): void {
        const agreementsFormValid = this.agreementsArrayForm.valid;

        this.agreementsValid.emit(agreementsFormValid);
    }

    private createAgreementsArray(agreements: AgreementInterface[]): void {
        this.agreementsForm.controls['agreements'].setValue([]);
        agreements.forEach((agreement: AgreementInterface) => {
            this.addAgreement(agreement);
        });
    }

    private addAgreement(agreement: AgreementInterface): void {
        const checked = !!agreement.checked;

        const agreementForm = this.formBuilder.group({
            checked: [checked, agreement.isRequired ? [BinaryTrueValidator] : []],
            id: agreement.id
        });

        this.agreementsArrayForm.push(agreementForm);
    }
}
