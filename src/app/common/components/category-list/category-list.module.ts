import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CategoryListComponent} from '@app/common/components/category-list/category-list.component';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {CategoryModule} from '@app/common/components/category/category.module';



@NgModule({
  declarations: [CategoryListComponent],
  exports: [
    CategoryListComponent
  ],
  imports: [
    CommonModule,
    DefaultContainerModule,
    CategoryModule
  ]
})
export class CategoryListModule { }
