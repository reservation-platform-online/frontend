import {Component, Input, OnInit} from '@angular/core';
import {combineLatest, ReplaySubject} from 'rxjs';
import {Reactive} from '@app/common/cdk/reactive';
import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';
import {Tools} from '@app/common/tools/tools';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {selectCategoryTreeList, selectPwsParams} from '@app/ngrx-store';
import {Store} from '@ngrx/store';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import * as CategoryActions from '@app/ngrx-store/category/category.actions';
import {NavController} from '@ionic/angular';
import {CategoryTreeListInterface} from '@app/common/interfaces/modules/client/category/category-tree-list.interface';

@Component({
  selector: 'app-category-list-component',
  templateUrl: './category-list.component.html'
})
export class CategoryListComponent extends Reactive implements OnInit {

  @Input()
  lastLikeBtn: boolean = false;

  public readonly categoryList$: ReplaySubject<CategoryTreeListInterface> = new ReplaySubject<CategoryTreeListInterface>();
  private categoryListBuffer: CategoryTreeListInterface;
  private searchParams: SearchInterface;

  constructor(
      private readonly store: Store,
      private readonly navController: NavController,
  ) {
    super();
  }

  ngOnInit() {

    combineLatest(
        this.store.select(selectPwsParams).pipe(this.takeUntil()),
        this.store.select(selectCategoryTreeList).pipe(this.takeUntil())
    ).pipe(this.takeUntil()).subscribe(([searchParams, categoryList]) => {
      this.searchParams = searchParams;

      if (categoryList) {

        this.categoryListBuffer = Tools.copyObject(categoryList);

        this.searchCategory(searchParams['search']);

      }

    });

  }

  refreshContent() {
    this.store.dispatch(new CategoryActions.Load());
  }

  private searchCategory(searched: string) {

    if (this.categoryListBuffer) {

      let item = Tools.copyObject(this.categoryListBuffer);

      item = Tools.filteringBySearchValue(searched, item.models);

      this.categoryList$.next(item);

    }

  }

  ionViewDidLeave() {
    this.unsubscribe();
  }

  selectCategory(child: CategoryInterface) {

    this.navController.navigateForward(['/category', child.name]).then(() => {

      this.store.dispatch(new fromPws.Params({
        category: child,
        search: ''
      }));

    });

  }

}
