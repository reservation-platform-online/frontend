import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';
import {selectCategoryList} from '@app/ngrx-store';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {Reactive} from '@app/common/cdk/reactive';
import {Store} from '@ngrx/store';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {SearchItemServiceInterface} from '@app/common/interfaces/modules/company/pws/search-item-service.interface';

@Component({
  selector: 'app-company-view-service',
  templateUrl: './service.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceComponent extends Reactive implements OnInit {

  @Input()
  service: ServiceInterface | SearchItemServiceInterface;

  private categoryList: CategoryListInterface;

  constructor(
      private readonly store: Store,
      private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  ngOnInit() {

    this.store.select(selectCategoryList).pipe(this.takeUntil()).subscribe((categories: CategoryListInterface) => {
      this.categoryList = categories;
      this.changeDetectorRef.detectChanges();
    });

  }

  getCategory(categoryId: number): CategoryInterface {
    return {
      name: '-',
      ...this.categoryList.models.find((item) => item.id === categoryId) // TODO check ...null
    };
  }

}
