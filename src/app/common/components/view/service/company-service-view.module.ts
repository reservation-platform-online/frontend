import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServiceComponent} from './service.component';
import {IonicModule} from '@ionic/angular';
import {DefaultValueConvertModule} from '@app/common/pipes/convert/default-value/default-value-convert.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {MaxReservationDateConvertModule} from '@app/common/pipes/convert/max-reservation-date/max-reservation-date-convert.module';


@NgModule({
    declarations: [ServiceComponent],
    exports: [
    ServiceComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        DefaultValueConvertModule,
        StringTimeConvertModule,
        ImgModule,
        MaxReservationDateConvertModule
    ]
})
export class CompanyServiceViewModule { }
