import {Component, Input, OnInit} from '@angular/core';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';

@Component({
  selector: 'app-company-view-worker',
  templateUrl: './worker.component.html'
})
export class WorkerComponent implements OnInit {

  @Input()
  worker: WorkerInterface;

  constructor() { }

  ngOnInit() {}

  getSrc(src: string) {
    return src ?? '../../../../../assets/images/default-img.jpg';
  }

}
