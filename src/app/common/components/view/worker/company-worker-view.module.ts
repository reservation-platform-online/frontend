import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkerComponent} from './worker.component';
import {IonicModule} from '@ionic/angular';
import {ImgModule} from '@app/common/ui/img/img.module';


@NgModule({
    declarations: [WorkerComponent],
    exports: [
        WorkerComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ImgModule
    ]
})
export class CompanyWorkerViewModule { }
