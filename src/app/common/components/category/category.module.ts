import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CategoryComponent} from '@app/common/components/category/category.component';
import {IonicModule} from '@ionic/angular';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';



@NgModule({
  declarations: [CategoryComponent],
  exports: [
    CategoryComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        DefaultContainerModule
    ]
})
export class CategoryModule { }
