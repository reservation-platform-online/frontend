import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';
import {CategoryTreeInterface} from '@app/common/interfaces/modules/client/category/category-tree.interface';
import {CategoryTreeListInterface} from '@app/common/interfaces/modules/client/category/category-tree-list.interface';

@Component({
  selector: 'app-category-component',
  templateUrl: './category.component.html'
})
export class CategoryComponent {

  @Input()
  categoryList: CategoryTreeListInterface;

  @Input()
  lastLikeBtn: boolean = false;

  @Input()
  UIList: boolean = false;

  @Input()
  level: number = 0;

  @Output()
  selectedCategory = new EventEmitter();

  trackByFn(item: CategoryInterface) {
    return item && item.id;
  }

  selectCategory(child: CategoryInterface) {
    this.selectedCategory.emit(child);
  }

  findChildWithService(model: CategoryTreeInterface) {
    if (this.lastLikeBtn) {
      // return model.children.findIndex((child) => child.children.length === 0) > -1;
      return model.children.length === 0;
    } else {
      return model.children.findIndex((child) => child.numberOfServices > 0) > -1;
    }
  }

  checkBtn(child: CategoryTreeInterface) {
    if (this.lastLikeBtn) {
      return child.children.length === 0;
    } else {
      return child.numberOfServices > 0;
    }
  }

}
