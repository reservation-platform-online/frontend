// export const EMAIL_PATTERN = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
export const FIRST_NAME_PATTERN = /^[A-Ą-Z-Ź'][^0-9_!¡?÷?.¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
export const LAST_NAME_PATTERN = /^[A-Ą-Z-Ź'][^0-9_!¡?÷?.¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
export const EMAIL_PATTERN = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
export const PHONE_PATTERN = /^[1-9][0-9]*([.][0-9]{2}|)$/;
