import {FormGroup} from '@angular/forms';

export class PasswordTool {

    /**
     *
     * @param group
     *
     */
    static checkPasswords(group: FormGroup) {

        let password = 'password';
        let passwordRepeat = 'passwordRepeat';

        if (Object.keys(group.controls).includes('newPassword')) {

            password = 'newPassword';
            passwordRepeat = 'newPasswordRepeat';

        }

        if (group.controls[passwordRepeat].value.toString().length > 5) {

            const isConfirmation = group.controls[password].value === group.controls[passwordRepeat].value;

            if (!isConfirmation) {
                group.controls[passwordRepeat].setErrors({MatchPassword: true});
            }

            return isConfirmation ? null : {MatchPassword: true};

        } else {

            return null;

        }

    }

}
