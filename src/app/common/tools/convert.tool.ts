import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

export class ConvertTool {

    static b64toBlob(dataURI) {

        const byteString = atob(dataURI.split(',')[1]);
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);

        for (let i = 0; i < byteString.length; i++) {

            ia[i] = byteString.charCodeAt(i);

        }

        return new Blob([ab], { type: 'image/jpeg' });

    }

    static blobToFile(theBlob: Blob, fileName: string): File {

        const b: any = theBlob;
        // A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;

        // Cast to a File() type
        return theBlob as File;

    }

    static clearObject<T>(object: T): T {

        console.assert(object !== null);

        Object.keys(object).forEach((key) => {
            if (!isNotNullOrUndefined(object[key]) || object[key].toString().length === 0) {
                delete object[key];
            }
        });

        return object as T;

    }

    static parseDDMMYYYY(str): Date {
        const parts = str.split('.');
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

}
