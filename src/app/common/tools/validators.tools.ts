import {AbstractControl, ValidatorFn} from '@angular/forms';

export const PhoneValidator: ValidatorFn = (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
        return;
    } else {

        const phoneReg = /^[0-9]{9}$/;
        const phone = control.value;

        return phoneReg.test(phone) ? null : {notValid: true};
    }
};

export const PostalCodeValidator: ValidatorFn = (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
        return {notValid: true};
    } else {

        const postalCodeReg = /^[0-9]{2}-[0-9]{3}$/;
        const postalCode = control.value;

        return postalCodeReg.test(postalCode) ? null : {notValid: true};
    }
};

export const BinaryTrueValidator: ValidatorFn = (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
        return {notValid: true};
    } else {

        return control.value === 1 ? null : {notValid: true};
    }
};

export const SelectedPersonValidator: ValidatorFn = (control: AbstractControl): { [key: string]: any } => {
    if (!control.value) {
        return {notValid: true};
    } else {

        return control.value === 'null' ? {notValid: true} : null;
    }
};
