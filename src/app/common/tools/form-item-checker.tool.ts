
export class FormItemCheckerTool {

    static minLength(submitted: boolean, form: any, formControlName: string): boolean {
        return submitted && form.controls[formControlName].minLength;
    }

    static isInvalid(submitted: boolean, form: any, formControlName: string): boolean {
        return submitted && form.controls[formControlName].invalid;
    }

    static doesPasswordMatch(submitted: boolean, form: any, newPassword: string, newPasswordRe: string): boolean {
        return submitted && form.controls[newPassword].invalid ||
            submitted && !this.checkIfNewPasswordMatches(form, newPassword, newPasswordRe);
    }

    static checkIfNewPasswordMatches(form: any, newPassword: string, newPasswordRe: string): boolean {
        return form.controls[newPassword].value === form.controls[newPasswordRe].value;
    }
}
