import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {CategoryTreeInterface} from '@app/common/interfaces/modules/client/category/category-tree.interface';

export class Tools {

    static copyObject(data) {
        if (data) {
            return JSON.parse(JSON.stringify(data));
        }
        return data;
    }

    static getKeys(item: any, removeKeyWithPrefix: string = null) {
        const keys = Object.keys(item);
        if (removeKeyWithPrefix) {
            return keys.filter((key) => !key.startsWith(removeKeyWithPrefix));
        }
        return keys;
    }

    /**
     * Default check is same date
     * @param firstDate
     * @param secondDate
     * @param config
     */
    static isSameDate(firstDate: Date, secondDate: Date, config?: {
        checkDate?: boolean;
        checkMonth?: boolean;
    }): boolean {

        let checkDate: boolean = true;
        let checkMonth: boolean = true;

        if (config) {
            checkDate = config.checkDate ? config.checkDate : checkDate;
            checkMonth = config.checkMonth ? config.checkMonth : checkMonth;
        }

        if (firstDate.getFullYear() === secondDate.getFullYear()) {

            if (checkMonth) {

                if (firstDate.getMonth() === secondDate.getMonth()) {

                    if (checkDate) {

                        if (firstDate.getDate() === secondDate.getDate()) {

                            return true;

                        }

                    } else {

                        return true;

                    }

                }

            } else {

                return true;

            }

        }

        return false;

    }

    static iOSDate(stringDate) {

        if (typeof stringDate === 'object' || stringDate === undefined) {

            return typeof stringDate === 'object' ? stringDate : undefined;

        }

        if (stringDate.toString().indexOf(' ') !== -1 || stringDate.toString().indexOf('T') !== -1) {

            const spl = stringDate.toString().split(stringDate.toString().indexOf(' ') !== -1 ? ' ' : 'T');
            const date = spl[0].split('-');
            const time = spl[1].substring(0, 8).split(':');
            const year = date[0];
            const month = date[1];
            const day = date[2];
            const h = time[0];
            const m = time[1];
            const s = time[2];

            return new Date(Number(year), Number(month) - 1, Number(day), Number(h), Number(m), Number(s));

        } else {

            const date = stringDate.toString().split(stringDate.toString().indexOf('-') !== -1 ? '-' : '.');
            const year = date[0];
            const month = date[1];
            const day = date[2];

            return new Date(Number(year), Number(month) - 1, Number(day));

        }

    }

    static clearObject(params) {
        if (params) {
            const object = this.copyObject(params);
            for (const key of Object.keys(object)) {

                if (!isNotNullOrUndefined(object[key]) || object[key] === 'null' || object[key] === 'undefined') {
                    delete object[key];
                }

            }
            return object;

        }

        return params;
    }

    static filteringBySearchValue(searched: string, children: CategoryTreeInterface[]) {

        if (searched && searched.length > 0) {

            const returnFindChildren = (childList: CategoryTreeInterface[]) => {

                const newChildren = [];

                if (childList && childList.length > 0) {

                    for (const child of childList) {

                        if (child.name.toLowerCase().search(decodeURIComponent(decodeURIComponent(searched)).toLowerCase()) > -1) {

                            newChildren.push(child);

                        } else {

                            child.children = returnFindChildren(child.children);

                            if (child.children.length > 0) {

                                newChildren.push(child);

                            }

                        }

                    }

                }

                return newChildren;

            };

            const item = returnFindChildren(children);

            return {
                models: item,
                total: item.length
            };

        }

        return {
            models: children,
            total: children.length
        };

    }

    static getItemBySearchValue(searched: string, children: CategoryTreeInterface[]) {

        if (searched && searched.length > 0) {

            const returnFindChildren = (childLIst: CategoryTreeInterface[]) => {

                if (childLIst && childLIst.length > 0) {

                    let result = null;

                    for (const child of childLIst) {

                        if (child.name.toLowerCase().search(decodeURIComponent(decodeURIComponent(searched)).toLowerCase()) > -1) {

                            result = child;

                        } else {

                            result = returnFindChildren(child.children);

                        }

                        if (result) {

                            return result;

                        }

                    }

                }

            };

            return returnFindChildren(children);

        }

        return null;

    }
}
