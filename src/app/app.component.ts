import {Component, OnInit, ViewEncapsulation} from '@angular/core';

import {NavController, Platform, ToastController} from '@ionic/angular';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {map, switchMap, take} from 'rxjs/operators';
import {KeyboardInfo, Plugins} from '@capacitor/core';
import {Reactive} from '@app/common/cdk/reactive';
import {AuthenticationTokenService} from '@app/common/services/api/modules/client/authentication/authentication-token.service';
import {LoginSuccess} from '@app/ngrx-store/authentication/authentication.actions';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {AppMinimize} from '@ionic-native/app-minimize/ngx';
import * as CategoryActions from '@app/ngrx-store/category/category.actions';
import * as ProfileActions from '@app/ngrx-store/profile/profile.actions';
import * as ReservationActions from '@app/ngrx-store/reservation/reservation.actions';
import {NavService} from '@app/common/services/util/nav.service';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';
import {
    BusinessCompanyLoad,
    BusinessCompanyLoadSuccess,
    BusinessWorkerLoad,
    BusinessWorkerLoadSuccess
} from '@app/ngrx-store/business/business.actions';
import {BusinessPwsItemLoadSuccess, BusinessPwsListLoadSuccess} from '@app/ngrx-store/business/pws/pws.actions';
import {
    BusinessReservationItemLoadSuccess,
    BusinessReservationListLoadSuccess
} from '@app/ngrx-store/business/reservation/reservation.actions';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {Badge} from '@ionic-native/badge/ngx';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Observable} from 'rxjs';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';

const {Keyboard} = Plugins;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    encapsulation: ViewEncapsulation.None,
    providers: [AppMinimize]
})
export class AppComponent extends Reactive implements OnInit {

    constructor(
        private readonly navService: NavService,
        private readonly platform: Platform,
        private readonly companyIdService: CompanyIdService,
        private readonly workerIdService: WorkerIdService,
        private readonly authenticationTokenService: AuthenticationTokenService,
        private readonly store: Store<fromStore.State>,
        private readonly statusBar: StatusBar,
        private readonly navController: NavController,
        private readonly screenSizeService: ScreenSizeService,
        private readonly toastController: ToastController,
        private readonly splashScreen: SplashScreen,
        private readonly appMinimize: AppMinimize,
        private readonly badge: Badge,
        private readonly alertPromptService: AlertPromptService
    ) {
        super();

        this.initializeApp();

    }

    public onIsMobile(): Observable<boolean> {
        return this.screenSizeService.isMobileView$;
    }

    private initializeApp(): void {

        this.platform.ready().then(() => {

            this.screenSizeService.onResize(this.platform.width());

            if (this.navService.isNative) {

                this.splashScreen.hide();

                this.statusBar.styleDefault();
                this.statusBar.overlaysWebView(false);

                this.badge.clear();

                this.initKeyboard();

                this.platform.backButton.subscribe(() => {

                    this.navController.back();

                    const path = String(this.platform.url()).split('/#/');
                    if (path.length > 0) {
                        const page = path[1].split('/')[1];
                        if (page) {
                            if (page === 'home') {
                                this.alertPromptService.presentAlert('Czy na pewno chcesz wyjść z aplikacji?', {
                                    buttons: [
                                        {
                                            text: 'Nie',
                                            handler: () => {
                                            }
                                        },
                                        {
                                            text: 'Tak',
                                            handler: () => {
                                                this.appMinimize.minimize();
                                            }
                                        }
                                    ]
                                });
                            }
                        }
                    }
                });

            }

            // Init toast, need when app is disconnects with internat
            this.toastController.create({
                animated: false
            }).then((toast) => {
                toast.present();
                toast.dismiss();
            });

        });
    }

    ngOnInit() {

        this.store.select(fromStore.selectProfileData).pipe(this.takeUntil()).subscribe((result: ProfileInterface) => {

            if (result) {

                if (result.jobs && result.jobs.length > 0) {

                    this.setWorkerId(result.jobs[0].id);
                    this.store.dispatch(new BusinessWorkerLoad());

                }

                if (result.companies && result.companies.length > 0) {

                    this.setCompanyId(result.companies[0].company.id);
                    this.store.dispatch(new BusinessCompanyLoad());

                }

            }

        });

        this.store.dispatch(new fromPws.Params({
            category: null,
            companyId: null,
            serviceId: null,
            pointId: null,

            // categoryId: 'category_1',
            // companyId: null,
            // serviceId: 'cfd25fa6-6ec8-482e-8789-0afaea8573b2',
            // categoryId: 'subcategory_3',
            // pointId: null,

            count: 32,
            page: 1,
            search: '',
            orderBy: 'price',
            sort: 'ASC',

            start: null,
            end: null,

        }));

        // this.store.dispatch(new fromSearch.InitParams({
        //     categoryId: 'category_1',
        //     companyId: null,
        //     serviceId: 'cfd25fa6-6ec8-482e-8789-0afaea8573b2',
        //     pointId: null,
        //
        //     count: 25,
        //     page: 1,
        //     search: '',
        //     orderBy: 'price',
        //     sort: 'ASC',
        //     start: '28-12-2020',
        //     end: '07-01-2021'
        //
        // }));


        // this.clientTabService
        //     .onTab()
        //     .pipe(
        //         skip(1),
        //         this.takeUntil()
        //     )
        //     .subscribe((tab: string) => {
        //         this.selectedTab = tab
        //         this.changeDetectorRef.detectChanges()
        //     })

        // Get start data from server
        this.store.dispatch(new CategoryActions.Load());

        // Get data from storage
        this.authenticationTokenService.getTokenFromStorage();
        this.workerIdService.getIdFromStorage();
        this.companyIdService.getIdFromStorage();

        this.authenticationTokenService
            .onToken()
            .pipe(
                map((token: string) => {
                    if (token) {
                        this.store.dispatch(new LoginSuccess({token}));
                    }
                    return token;
                }),
                take(1),
                switchMap(() => this.store.select(fromStore.selectIsLoggedIn)),
                this.takeUntil()
            )
            .subscribe((loggedIn) => {
                if (loggedIn) {
                    this.store.dispatch(new ProfileActions.Load());

                } else {
                    // Dispatch all profile data and set null
                    this.store.dispatch(new ReservationActions.LoadSuccess(null));
                    this.store.dispatch(new ReservationActions.LoadHistorySuccess(null));
                    this.store.dispatch(new BusinessCompanyLoadSuccess(null));
                    this.store.dispatch(new BusinessWorkerLoadSuccess(null));
                    this.store.dispatch(new BusinessPwsItemLoadSuccess(null));
                    this.store.dispatch(new BusinessPwsListLoadSuccess(null));
                    this.store.dispatch(new BusinessReservationListLoadSuccess(null));
                    this.store.dispatch(new BusinessReservationItemLoadSuccess(null));
                }
            });

        if (this.platform.platforms()
            .findIndex((platform => ['desktop', 'tablet', 'mobileweb']
                .includes(platform))) > -1) {

        }

    }

    private initKeyboard(): void {

        Keyboard.addListener('keyboardWillShow', (info: KeyboardInfo) => {
            const ionContents = Array.from(document.getElementsByClassName('ion-page'));
            for (const content of ionContents) {
                content.classList.add('padding-bottom-keyboard');
                content['style'].setProperty('--keyboard-height', `${info.keyboardHeight}px`);
            }
        });

        Keyboard.addListener('keyboardWillHide', () => {
            const ionContents = Array.from(document.getElementsByClassName('ion-page'));
            for (const content of ionContents) {
                content.classList.remove('padding-bottom-keyboard');
            }
        });

    }

    private setWorkerId(id: string): void {
        this.workerIdService.setId(id);
    }

    private setCompanyId(id: string): void {
        this.companyIdService.setId(id);
    }
}
