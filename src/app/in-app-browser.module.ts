import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';

@NgModule({
    imports: [
        CommonModule,
        IonicModule
    ],

})
export class InAppBrowserModule {

    static forRoot(): ModuleWithProviders<InAppBrowserModule> {
        return {
            ngModule: InAppBrowserModule,
            providers: [
                InAppBrowser
            ]
        };
    }

    static forChild(): ModuleWithProviders<InAppBrowserModule> {
        return {
            ngModule: InAppBrowserModule,
            providers: []
        };
    }

}
