import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '@envi/environment';
import {EffectsModule} from '@ngrx/effects';
import {reducers} from './ngrx-store';
import {ApiModule} from './common/services/api/api.module';
import {InAppBrowserModule} from '@app/in-app-browser.module';
import {HttpClientModule} from '@angular/common/http';
import {HTTP} from '@ionic-native/http/ngx';
import {LoadingModule} from '@app/common/services/util/loading/loading.module';
import {AlertPromptModule} from '@app/common/services/util/alert-prompt/alert-prompt.module';
import {FormItemCheckerTool} from '@app/common/tools/form-item-checker.tool';
import {FCM} from '@ionic-native/fcm/ngx';
import {Badge} from '@ionic-native/badge/ngx';
import {ServiceWorkerModule} from '@angular/service-worker';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {SideMenuModule} from '@app/common/modules/shell/side-menu/side-menu.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {ScreenSizeModule} from '@app/common/services/screen-size/screen-size.module';
import {BarRatingModule} from 'ngx-bar-rating';
import {AppRouteGuardsModule} from '@app/common/services/route/guards/app-route-guards.module';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {ToUpModule} from '@app/common/modules/shell/to-up/to-up.module';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IosArrowIconModule} from '@app/common/ui/icons/arrow/ios-arrow-icon.module';
import {TopBarModule} from '@app/common/modules/shell/top-bar/top-bar.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

const store = [
  StoreModule.forRoot(reducers),
  StoreDevtoolsModule.instrument({
    maxAge: 25,
    logOnly: environment.production // Restrict extension to log-only mode
  }),
  EffectsModule.forRoot([])
];

const api = [
    ApiModule,
];

const services = [
    ScreenSizeModule,
];

const util = [
    AlertPromptModule,
    LoadingModule,
    HeaderModule,
    SideMenuModule,
    ImgModule,
    AngularEditorModule,
    TopBarModule,
    FooterModule,
];

const pipes = [
    StringTimeConvertModule.forRoot(),
    WeekdayNameModule.forRoot()
];


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
    imports: [
        ...api,
        ...store,
        BrowserModule,
        IonicModule.forRoot(),
        InAppBrowserModule.forRoot(),
        AppRouteGuardsModule,
        HttpClientModule,
        AppRoutingModule,
        BarRatingModule,
        ...util,
        ...services,
        ...pipes,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        ToUpModule,
        CommonModule,
        FormsModule,
        IosArrowIconModule
    ],
  providers: [
      StatusBar,
      SplashScreen,
      Badge,
      FCM,
      HTTP,
      SplashScreen,
      FormItemCheckerTool,
      {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
