import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {ClientAuthRouteGuard} from '@app/common/services/route/guards/client-auth-route-guard.service';
import {ClientRouteGuard} from '@app/common/services/route/guards/client-route-guard.service';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
    },
    {
        path: 'confirm/:verificationCode',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
    },
    {
        path: 'documents',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/documents/documents.module').then(m => m.DocumentsPageModule),
                },
                {
                    path: 'regulamin',
                    loadChildren: () => import('./pages/documents/pages/regulamin/regulamin.module')
                        .then(m => m.RegulaminPageModule)
                },
                {
                    path: 'knowledge-base',
                    loadChildren: () => import('./pages/documents/pages/knowledge-base/knowledge-base.module').then(m => m.KnowledgeBasePageModule)
                }
            ]
    },
    {
        path: 'category/:categoryName',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/service-list/service-list.module').then(m => m.ServiceListPageModule),
                },
                {
                    path: 'service/:uuid',
                    loadChildren: () => import('./pages/service/service.module').then(m => m.ServicePageModule)
                },
            ]
    },
    {
        path: 'service',
        children:
            [
                {
                    path: ':uuid',
                    loadChildren: () => import('./pages/service/service.module').then(m => m.ServicePageModule)
                }
            ]
    },
    {
        path: 'auth',
        children:
            [
                {
                    path: 'login',
                    loadChildren: () => import('./pages/auth/pages/login/login.module').then(m => m.LoginPageModule),
                    canActivate: [ClientAuthRouteGuard]
                },
                {
                    path: 'login/:code',
                    loadChildren: () => import('./pages/auth/pages/login/login.module').then(m => m.LoginPageModule),
                    canActivate: [ClientAuthRouteGuard]
                },
                {
                    path: 'registration',
                    loadChildren: () =>
                        import('./pages/auth/pages/registration/registration.module').then(m => m.RegistrationPageModule),
                    canActivate: [ClientAuthRouteGuard]
                },
                {
                    path: 'registration-company',
                    loadChildren: () =>
                        import('./pages/auth/pages/registration-company/registration-company.module')
                            .then(m => m.RegistrationCompanyPageModule),
                    canActivate: [ClientRouteGuard]
                },
            ]
    },
    {
        path: 'contact',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/contact/contact.module').then(m => m.ContactPageModule)
                }
            ]
    },
    {
        path: 'support',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportPageModule)
                }
            ]
    },
    {
        path: 'profile',
        canActivate: [ClientRouteGuard],
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule)
                }
            ]
    },
    {
        path: 'report',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/report/report.module').then(m => m.ReportPageModule)
                }
            ]
    },
    {
        path: 'about',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/about/about.module').then(m => m.AboutPageModule)
                }
            ]
    },
    {
        path: 'worker',
        children:
            [
                {
                    path: ':uuid',
                    loadChildren: () => import('./pages/worker/worker.module').then(m => m.WorkerPageModule)
                }
            ]
    },
    {
        path: 'business',
        canActivate: [ClientRouteGuard],
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/business/business.module').then(m => m.BusinessPageModule)
                },
                {
                    path: 'service/:id',
                    loadChildren: () => import('./pages/business/pages/service/service.module').then(m => m.ServicePageModule)
                },
                {
                    path: 'service-list',
                    loadChildren: () => import('./pages/business/pages/service-list/service-list.module')
                        .then(m => m.ServiceListPageModule)
                },
                {
                    path: 'service-list/service/:id',
                    loadChildren: () => import('./pages/business/pages/service/service.module').then(m => m.ServicePageModule)
                },
                {
                    path: 'calendar',
                    loadChildren: () => import('./pages/business/pages/calendar/calendar.module')
                        .then(m => m.CalendarPageModule)
                }
            ]
    },
    {
        path: 'companies',
        children:
            [
                {
                    path: '',
                    loadChildren: () => import('./pages/company-list/company-list.module').then(m => m.CompanyListPageModule)
                },
                {
                    path: ':uuid',
                    loadChildren: () => import('./pages/company/company.module').then(m => m.CompanyPageModule)
                }
            ]
    },
    // {
    //     path: '**',
    //     redirectTo: '/home',
    //     pathMatch: 'full'
    // }

    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    },

];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            {
                scrollPositionRestoration: 'enabled',
                preloadingStrategy: PreloadAllModules,
                useHash: false
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
