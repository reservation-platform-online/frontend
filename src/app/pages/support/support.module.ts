import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {SupportPageRoutingModule} from './support-routing.module';

import {SupportPage} from './support.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SupportPageRoutingModule,
        HeaderModule,
        FooterModule
    ],
  declarations: [SupportPage]
})
export class SupportPageModule {}
