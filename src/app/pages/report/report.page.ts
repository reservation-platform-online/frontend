import {Component, OnInit} from '@angular/core';
import {HeaderService} from '@app/common/services/header/header.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastController} from '@ionic/angular';
import {ClientApiService} from '@app/common/services/api/modules/client/client-api.service';
import {Reactive} from '@app/common/cdk/reactive';

@Component({
    selector: 'app-report',
    templateUrl: './report.page.html'
})
export class ReportPage extends Reactive implements OnInit {

    form: FormGroup;
    submitted = false;

    constructor(
        private readonly headerService: HeaderService,
        private readonly clientApiService: ClientApiService,
        private readonly formBuilder: FormBuilder,
        private readonly toastController: ToastController
    ) {

        super();
        this.headerService.setTitle('Zgłoszenie usterkę');
        this.initForm();

    }

    initForm() {

        this.form = this.formBuilder.group({

            description: ['', Validators.required],

        });

    }

    ngOnInit(): void {

    }

    sendReport() {

        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {


            const param = this.form.value;
            delete param.isSubmitted;
            param['description'] = param['description'].trim();

            this.clientApiService.postReport(param)
                .pipe(
                    this.takeUntil()
                )
                .subscribe((_) => {
                    this.form.markAsUntouched();
                    this.form.updateValueAndValidity();
                    this.toastController.create({
                        header: 'Raport został wysłany',
                        buttons: [
                            {
                                role: 'cancel',
                                icon: 'close-outline',
                            }
                        ],
                        color: 'success',
                        duration: 3500
                    }).then((toast) => toast.present());

                    this.initForm();
                });

        }

    }

}
