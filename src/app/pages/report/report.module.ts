import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ReportPageRoutingModule} from './report-routing.module';

import {ReportPage} from './report.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {InputModule} from '@app/common/ui/input/input.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReportPageRoutingModule,
        HeaderModule,
        ReactiveFormsModule,
        InputModule,
        FooterModule,
        BannerModule
    ],
  declarations: [ReportPage]
})
export class ReportPageModule {}
