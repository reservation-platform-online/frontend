import {Component, OnInit} from '@angular/core';
import {HeaderService} from '@app/common/services/header/header.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html'
})
export class AboutPage implements OnInit {

  constructor(
      private readonly headerService: HeaderService
  ) {
    this.headerService.setTitle('O nas');
  }

  ngOnInit() {
  }

}
