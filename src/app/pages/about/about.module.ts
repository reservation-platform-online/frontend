import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {AboutPageRoutingModule} from './about-routing.module';

import {AboutPage} from './about.page';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AboutPageRoutingModule,
        BannerModule,
        FooterModule,
        HeaderModule
    ],
  declarations: [AboutPage]
})
export class AboutPageModule {}
