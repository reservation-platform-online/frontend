import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ServiceListPageRoutingModule} from './service-list-routing.module';

import {ServiceListPage} from './service-list.page';
import {PwsListModule} from '@app/common/modules/client/pws-list/pws-list.module';
import {SearchModule} from '@app/common/modules/client/search/search.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ServiceListPageRoutingModule,
        PwsListModule,
        SearchModule,
        HeaderModule,
        BannerModule,
        FooterModule
    ],
  declarations: [ServiceListPage]
})
export class ServiceListPageModule {}
