import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {HeaderService} from '@app/common/services/header/header.service';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Reactive} from '@app/common/cdk/reactive';
import {selectCategoryList} from '@app/ngrx-store';
import {Tools} from '@app/common/tools/tools';
import {combineLatest} from 'rxjs';

@Component({
    selector: 'app-slots',
    templateUrl: './service-list.page.html'
})
export class ServiceListPage extends Reactive implements OnInit {

    constructor(
        private readonly store: Store,
        private readonly modalController: ModalController,
        private readonly activatedRoute: ActivatedRoute,
        private readonly headerService: HeaderService
    ) {
        super();
        this.headerService.setTitle('reservation-platform');
    }

    ionViewWillEnter() {

        // this.refreshContent(null);

    }

    ngOnInit() {


        combineLatest(
            this.activatedRoute.params.pipe(this.takeUntil()),
            this.store.select(selectCategoryList).pipe(this.takeUntil())
        ).pipe(this.takeUntil()).subscribe(([params, categoryList]) => {

            if (params['categoryName'] && categoryList) {

                const found = Tools.getItemBySearchValue(params['categoryName'], categoryList['models']);

                if (found) {


                    this.store.dispatch(new fromPws.Params({
                        category: found
                    }));

                } else {

                    console.log(params['categoryName'], categoryList);

                }


            }

        });

    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    refreshContent(event = null): void {
        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }
        this.store.dispatch(new fromPws.RefreshList());

    }
}
