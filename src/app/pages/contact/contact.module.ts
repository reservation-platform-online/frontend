import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ContactPageRoutingModule} from './contact-routing.module';

import {ContactPage} from './contact.page';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ContactPageRoutingModule,
        FooterModule,
        HeaderModule
    ],
    declarations: [ContactPage]
})
export class ContactPageModule {
}
