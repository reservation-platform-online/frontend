import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromPws from '@app/ngrx-store/pws/pws.actions';
import {Params, ParamsCalendar, ParamsComments} from '@app/ngrx-store/pws/pws.actions';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {selectCategoryList, selectPwsItem, selectPwsParams} from '@app/ngrx-store';
import {Reactive} from '@app/common/cdk/reactive';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {Observable} from 'rxjs';
import {HeaderService} from '@app/common/services/header/header.service';
import {NavService} from '@app/common/services/util/nav.service';
import {tap} from 'rxjs/operators';
import {CategoryInterface} from '@app/common/interfaces/modules/client/category/category.interface';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {ModalController} from '@ionic/angular';
import * as moment from 'moment';
import {CalendarDaysSlotsComponent} from '@app/common/modules/calendar-days-slots/calendar-days-slots.component';


@Component({
    selector: 'app-view',
    templateUrl: './service.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicePage extends Reactive implements OnInit {

    public item: SearchItemInterface;
    public title: string = 'Loading...';

    private categoryList: CategoryListInterface;

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly navService: NavService,
        private readonly headerService: HeaderService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly modalController: ModalController,
        private readonly screenSizeService: ScreenSizeService,
        private readonly store: Store,
    ) {
        super();
        this.headerService.setTitle('Podgląd usługi');
    }

    public get item$(): Observable<SearchItemInterface> {
        return this.store.select(selectPwsItem).pipe(this.takeUntil(), tap((item: SearchItemInterface) => {
            if (!this.item && item) {
                this.item = item;
                this.store.dispatch(new Params({
                    category: this.getCategory(item.service.categoryId),
                }));
                this.store.dispatch(new ParamsCalendar({
                    itemId: this.item.id,
                    start: moment().locale('pl').format('YYYY-MM-DD'),
                    end: moment().locale('pl').clone().add(7, 'days').format('YYYY-MM-DD'),
                }));
                this.store.dispatch(new ParamsComments({
                    itemId: this.item.id,
                    page: 1,
                    count: 10,
                    expand: 'user'
                }));
                this.changeDetectorRef.detectChanges();
            }
        })) as Observable<SearchItemInterface>;
    }

    onIsMobile(): Observable<boolean> {
        return this.screenSizeService.isMobileView$;
    }

    ngOnInit() {

        this.store.select(selectCategoryList).pipe(this.takeUntil()).subscribe((categories: CategoryListInterface) => {
            this.categoryList = categories;
        });

        this.store.select(selectPwsParams).pipe(this.takeUntil()).subscribe((searchParams: SearchInterface) => {

            if (searchParams && searchParams.itemId) {
                this.store.dispatch(new fromPws.RefreshItem());

                this.refreshContent(null);

            }

        });

        this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params: {
            uuid: string
        }) => {
            if (params && params.uuid) {
                this.store.dispatch(new fromPws.Params({
                    itemId: params.uuid
                }));
            }
        });
    }

    getCategory(categoryId: number): CategoryInterface {
        return {
            name: '-',
            ...this.categoryList.models.find((item) => item.id === categoryId), // TODO check ...null
        };
    }

    public refreshContent(event): void {
        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }
        // this.store.dispatch(new fromPws.RefreshItem());
    }

    back() {

        this.clearStreams();

        this.navService.back({
            cut: 2
        });
    }

    ionViewDidLeave() {
        this.clearStreams();
    }

    openCalendar() {

        this.modalController.create({
            component: CalendarDaysSlotsComponent,
            id: 'calendar-select-slot',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            componentProps: {
                item: this.item,
                inModal: true
            },
            backdropDismiss: true,
        }).then((modal) => {
            modal.present();
            // this.store.dispatch(new fromPws.RefreshCalendar());
            modal.onWillDismiss().then(() => {
                this.store.dispatch(new fromPws.LoadCalendarSuccess(null));
            });

        });

    }

    bookmark() {

    }

    private clearStreams() {

        this.store.dispatch(new fromPws.Params({
            itemId: null
        }));

        this.store.dispatch(new fromPws.LoadItemSuccess(null));
        this.store.dispatch(new fromPws.ParamsCalendar({
            itemId: null,
            start: null,
            end: null
        }));
        this.store.dispatch(new fromPws.LoadCalendarSuccess(null));
        this.store.dispatch(new fromPws.ParamsComments({
            itemId: null,
            page: 1,
            count: 10
        }));
        this.store.dispatch(new fromPws.LoadCommentsSuccess(null));

    }
}
