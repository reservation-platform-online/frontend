import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ViewPageRoutingModule} from './service-routing.module';

import {ServicePage} from './service.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {BarRatingModule} from 'ngx-bar-rating';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {DefaultValueConvertModule} from '@app/common/pipes/convert/default-value/default-value-convert.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {CalendarDaysSlotsModule} from '@app/common/modules/calendar-days-slots/calendar-days-slots.module';
import {ClientServiceCommentListModule} from '@app/common/modules/client/comment-list/client-service-comment-list.module';
import {ImgModule} from '@app/common/ui/img/img.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ViewPageRoutingModule,
        HeaderModule,
        BannerModule,
        DefaultContainerModule,
        BarRatingModule,
        StringTimeConvertModule,
        DefaultValueConvertModule,
        FooterModule,
        CalendarDaysSlotsModule,
        ClientServiceCommentListModule,
        ImgModule
    ],
  declarations: [ServicePage]
})
export class ServicePageModule {}
