import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {DocumentsPageRoutingModule} from './documents-routing.module';

import {DocumentsPage} from './documents.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentsPageRoutingModule,
    HeaderModule,
    BannerModule,
    FooterModule
  ],
  declarations: [DocumentsPage]
})
export class DocumentsPageModule {
}
