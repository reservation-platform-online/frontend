import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {RegulaminPage} from './regulamin.page';

const routes: Routes = [
    {
        path: '',
        component: RegulaminPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RegulaminPageRoutingModule {
}
