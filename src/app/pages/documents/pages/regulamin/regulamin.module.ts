import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegulaminPageRoutingModule} from './regulamin-routing.module';

import {RegulaminPage} from './regulamin.page';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegulaminPageRoutingModule,
        FooterModule,
        HeaderModule
    ],
    declarations: [RegulaminPage]
})
export class RegulaminPageModule {
}
