import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {KnowledgeBasePageRoutingModule} from './knowledge-base-routing.module';

import {KnowledgeBasePage} from './knowledge-base.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        KnowledgeBasePageRoutingModule,
        HeaderModule,
        FooterModule
    ],
    declarations: [KnowledgeBasePage]
})
export class KnowledgeBasePageModule {
}
