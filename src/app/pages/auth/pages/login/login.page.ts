import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as fromStore from '@app/ngrx-store';
import {Store} from '@ngrx/store';
import {NavController} from '@ionic/angular';
import {Reactive} from '@app/common/cdk/reactive';


@Component({
    selector: 'app-auth-login-page',
    template: `
        <ion-content>
            <app-header></app-header>

            <app-client-login-form></app-client-login-form>

            <app-footer></app-footer>

        </ion-content>
    `,
    encapsulation: ViewEncapsulation.None
})
export class LoginPage extends Reactive implements OnInit {

    constructor(
        private readonly store: Store<fromStore.State>,
        private readonly navController: NavController
    ) {
        super();

    }

    ngOnInit() {

        this.store
            .select(fromStore.selectIsLoggedIn)
            .pipe(
                this.takeUntil()
            ).subscribe((isLoggedIn: boolean) => {
            if (isLoggedIn) {
                this.navController.navigateRoot(['/']);
            }
        });

    }

}
