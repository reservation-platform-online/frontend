import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {LoginPageRoutingModule} from './login-routing.module';

import {LoginPage} from './login.page';
import {PasswordResetModule} from '@app/common/services/api/modules/client/password-reset/password-reset.module';
import {InputModule} from '@app/common/ui/input/input.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {LoginFormModule} from '@app/common/modules/client/login-form/login-form.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LoginPageRoutingModule,
        ReactiveFormsModule,
        PasswordResetModule,
        InputModule,
        HeaderModule,
        LoginFormModule,
        BannerModule,
        FooterModule
    ],
    declarations: [LoginPage]
})
export class LoginPageModule {
}
