import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegistrationCompanyPageRoutingModule} from './registration-company-routing.module';

import {RegistrationCompanyPage} from './registration-company.page';
import {InputModule} from '@app/common/ui/input/input.module';
import {SelectInterfaceOptionsModule} from '@app/common/services/util/select-interface-options/select-interface-options.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {ClientCompanyFormServiceModule} from '@app/common/modules/client/company-form/service/client-company-form-service.module';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {BarRatingModule} from 'ngx-bar-rating';
import {ClientCompanyFormWorkTimeModule} from '@app/common/modules/client/company-form/work-time/client-company-form-work-time.module';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegistrationCompanyPageRoutingModule,
        ReactiveFormsModule,
        SelectInterfaceOptionsModule,
        InputModule,
        HeaderModule,
        BannerModule,
        ClientCompanyFormServiceModule,
        StringTimeConvertModule,
        ClientCompanyFormWorkTimeModule,
        BarRatingModule,
        ImgModule,
        WeekdayNameModule,
        FooterModule
    ],
    declarations: [
        RegistrationCompanyPage
    ]
})
export class RegistrationCompanyPageModule {
}
