import {Component, OnInit} from '@angular/core';
import {HeaderService} from '@app/common/services/header/header.service';
import {FormBuilder} from '@angular/forms';
import {ModalController, NavController, Platform} from '@ionic/angular';
import * as fromStore from '@app/ngrx-store';
import {Store} from '@ngrx/store';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {RegistrationApiService} from '@app/common/services/api/modules/client/registration/registration-api.service';
import {CompanyApiService} from '@app/common/services/api/modules/company/company-api.service';
import {Reactive} from '@app/common/cdk/reactive';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ServiceComponent} from '@app/common/modules/client/company-form/service/service.component';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {WorkTimeComponent} from '@app/common/modules/client/company-form/work-time/work-time.component';
import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';
import {v4 as uuidv4} from 'uuid';
import {WorkTimeInterface} from '@app/common/interfaces/modules/company/work-time/work-time.interface';
import {typeCompanyList} from '@envi/data/type-company-id';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {ConvertTool} from '@app/common/tools/convert.tool';
import {WorkerApiService} from '@app/common/services/api/modules/company/worker/worker-api.service';
import {WorkTimeApiService} from '@app/common/services/api/modules/company/work-time/work-time-api.service';
import {combineLatest} from 'rxjs';
import {Tools} from '@app/common/tools/tools';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';
import * as ProfileActions from '@app/ngrx-store/profile/profile.actions';
import {environment} from '@envi/environment';

@Component({
    selector: 'app-registration-company',
    templateUrl: './registration-company.page.html'
})
export class RegistrationCompanyPage extends Reactive implements OnInit {

    isLoggedIn: boolean = false;

    public typeCompanyList: {
        id: number,
        name: string
    }[] = [];

    public profile: ProfileInterface;

    public selectedSegment: 'singel' | 'company' = 'singel';
    public readonly pwsList: PwsInterface[] = [];
    public selectedPws: PwsInterface = null;
    private workTime: WorkTimeInterface = null;

    constructor(
        private readonly headerService: HeaderService,
        private readonly formBuilder: FormBuilder,
        private readonly navCtrl: NavController,
        private readonly store: Store,
        private readonly loadingService: LoadingService,
        private readonly alertPromptService: AlertPromptService,
        private readonly modalController: ModalController,
        private readonly platform: Platform,
        private readonly registrationApiService: RegistrationApiService,
        private readonly serviceApiService: ServiceApiService,
        private readonly workerApiService: WorkerApiService,
        private readonly workTimeApiService: WorkTimeApiService,
        private readonly companyIdService: CompanyIdService,
        private readonly workerIdService: WorkerIdService,
        private readonly apiCompanyService: CompanyApiService,
    ) {

        super();

        this.typeCompanyList = environment.typeCompanyList;

    }

    ngOnInit() {

        this.headerService.setTitle('Dodawanie');

        this.store.select(fromStore.selectProfileData).pipe(this.takeUntil()).subscribe((profile: ProfileInterface) => {

            this.profile = profile;

        });

        this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((result: boolean) => {

            this.isLoggedIn = result;

        });
    }

    openLoginPage(): void {
        this.navCtrl.navigateBack(['auth', 'login']);
    }

    segmentChanged($event: any) {
        this.selectedSegment = $event.detail.value;
    }

    openServiceModal() {


        this.modalController.create({
            component: ServiceComponent,
            componentProps: {},
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result && result.data) {

                    result.data.id = uuidv4();

                    const pws: PwsInterface = {
                        companyId: '',
                        enabled: 0,
                        id: uuidv4(),
                        limitReservationPerSlot: 0,
                        pointId: '',
                        service: result.data as ServiceInterface,
                        status: 0,
                        workTimeList: [],
                        worker: {
                            companyId: null,
                            enabled: 1,
                            firstName: this.profile.firstName,
                            id: uuidv4(),
                            userEmail: this.profile.email,
                            lastName: this.profile.lastName,
                            photo: this.profile.photo,
                            position: null,
                            score: 0,
                            status: 1

                        }
                    };
                    this.pwsList.push(pws);
                    this.addWorkTime(pws);
                }
            });
        });
    }

    deleteService(pws: PwsInterface) {

        this.alertPromptService.presentAlert('Czy na pewno chcesz usunąć usługę?', {
            buttons: [
                {
                    text: 'Nie',
                    handler: () => {

                    }
                },
                {
                    text: 'Tak',
                    handler: () => {
                        this.pwsList.splice(this.pwsList.findIndex((serviceFind) => serviceFind.id === pws.id), 1);
                    }
                }
            ]
        });
    }

    addWorkTime(pws: PwsInterface) {
        this.selectedPws = pws;

        this.modalController.create({
            component: WorkTimeComponent,
            componentProps: {
                pws
            },
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result && result.data) {
                    this.selectedPws.workTimeList.push(result.data);
                }
            });
        });

    }

    openForm(pws: PwsInterface, workTime: WorkTimeInterface) {
        this.selectedPws = pws;
        this.workTime = workTime;

        this.modalController.create({
            component: WorkTimeComponent,
            componentProps: {
                pws,
                workTime
            },
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result) {
                    if (result.data) {
                        if (result.data === 'delete') {
                            this.selectedPws
                                .workTimeList
                                .splice(this.selectedPws
                                    .workTimeList
                                    .findIndex(
                                        (workTimeFind) => workTimeFind.id === this.workTime.id
                                    ), 1);
                        } else {
                            const workTimeFound = this.selectedPws
                                .workTimeList
                                .find(
                                    (workTimeFind) => workTimeFind.id === this.workTime.id
                                );
                            for (const key of Object.keys(workTimeFound)) {
                                workTimeFound[key] = result.data[key];
                            }

                        }
                    }
                }
            });
        });
    }

    save() {
        // TODO create company
        let companyId: string = null;
        let serviceId: string = null;
        let workerId: string = null;

        if (this.isLoggedIn) {
            this.apiCompanyService
                .registerCompany({
                    typeCompanyId: typeCompanyList[0].id,
                    name: `${this.profile.firstName} ${this.profile.lastName}`
                })
                .pipe(
                    this.takeUntil()
                )
                .subscribe((result: string) => {

                    companyId = result['companyId'];
                    this.companyIdService.setId(companyId);

                    for (const pws of this.pwsList) {

                        combineLatest(
                            this.serviceApiService
                                .postCreate(Tools.copyObject(pws.service))
                                .pipe(
                                    this.takeUntil()
                                ),
                            this.workerApiService
                                .postCreate(Tools.copyObject(pws.worker))
                                .pipe(
                                    this.takeUntil()
                                )
                        ).subscribe(([service, worker]) => {
                            if (service) {
                                serviceId = service as string;
                                this.saveUploadImage(serviceId, pws.service.image);
                            }
                            if (worker) {
                                workerId = worker as string;
                            }
                            if (workerId && serviceId) {

                                this.workerIdService.setId(workerId);
                                this.store.dispatch(new ProfileActions.Load());

                                this.serviceApiService.postConnectWorker({
                                    workers: `${workerId}`
                                }, serviceId).pipe(this.takeUntil()).subscribe(async (pwsIds) => {

                                    for (const workTIme of pws.workTimeList) {

                                        workTIme['business__point_worker_service_id'] = pwsIds;
                                        workTIme['business__worker_id'] = workerId;

                                        delete workTIme.pwsId;

                                        await this.workTimeApiService
                                            .postCreate(workTIme)
                                            .pipe(
                                                this.takeUntil()
                                            )
                                            .subscribe();

                                    }

                                    this.navCtrl.navigateRoot(['/business']);

                                });

                            }
                        });

                    }

                });

        }

    }

    private saveUploadImage(uuid: string, croppedImage) {

        this.serviceApiService
            .saveImage(
                uuid,
                ConvertTool
                    .blobToFile(
                        ConvertTool.b64toBlob(croppedImage),
                        uuid
                    )
            ).pipe(this.takeUntil()).subscribe();
    }

}
