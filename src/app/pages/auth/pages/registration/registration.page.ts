import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {IonSelect, NavController, Platform} from '@ionic/angular';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Reactive} from '@app/common/cdk/reactive';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {SelectInterfaceOptionsService} from '@app/common/services/util/select-interface-options/select-interface-options.service';
import {PhoneValidator} from '@app/common/tools/validators.tools';
import {EMAIL_PATTERN, FIRST_NAME_PATTERN, LAST_NAME_PATTERN} from '@app/common/tools/patterns.tool';
import {FormItemCheckerTool} from '@app/common/tools/form-item-checker.tool';
import {RegistrationApiService} from '@app/common/services/api/modules/client/registration/registration-api.service';
import {AuthenticateInterface} from '@app/common/interfaces/modules/client/authentication/authenticate.interface';
import {Login} from '@app/ngrx-store/authentication/authentication.actions';
import {map} from 'rxjs/operators';
import {HeaderService} from '@app/common/services/header/header.service';
import {AgreementInterface} from '@app/common/interfaces/modules/client/registration/agreement.interface';

@Component({
    selector: 'app-auth-registration-page',
    templateUrl: './registration.page.html',
    encapsulation: ViewEncapsulation.None
})
export class RegistrationPage extends Reactive implements OnInit {

    @ViewChild(IonSelect)
    select: IonSelect;

    public form: FormGroup;

    public agreementList: AgreementInterface[];
    private agreementsValid: boolean = false;

    constructor(
        private readonly iab: InAppBrowser,
        private readonly navController: NavController,
        private readonly formBuilder: FormBuilder,
        private readonly alertPrompt: AlertPromptService,
        private readonly platform: Platform,
        private readonly store: Store<fromStore.State>,
        private readonly registrationApiService: RegistrationApiService,
        private readonly headerService: HeaderService,
        private readonly elementRef: ElementRef,
        private readonly selectInterfaceOptions: SelectInterfaceOptionsService
    ) {
        super();
        this.headerService.setTitle('Rejestracja');

        this.form = this.formBuilder.group({
                // email: ['test@example.com', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
                // phone: ['000000000', [Validators.required, PhoneValidator]],
                // password: ['123456', [Validators.required, Validators.minLength(6)]],
                // password2: ['123456', [Validators.required, Validators.minLength(6)]],
                // firstName: ['Test', [Validators.required, Validators.pattern(FIRST_NAME_PATTERN)]],
                // lastName: ['Test', [Validators.required, Validators.pattern(LAST_NAME_PATTERN)]],
                // agreements: [this.formBuilder.array([])],
                // dateBirth: [''],
                email: [null, [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
                phone: [null, [Validators.required, PhoneValidator]],
                password: [null, [Validators.required, Validators.minLength(6)]],
                password2: [null, [Validators.required, Validators.minLength(6)]],
                firstName: [null, [Validators.required, Validators.pattern(FIRST_NAME_PATTERN)]],
                lastName: [null, [Validators.required, Validators.pattern(LAST_NAME_PATTERN)]],
                agreements: [this.formBuilder.array([])],
                dateBirth: [''],
            }
        );
    }

    ngOnInit(): void {

        this.store
            .select(fromStore.selectIsLoggedIn)
            .pipe(
                this.takeUntil()
            ).subscribe((isLoggedIn: boolean) => {
            if (isLoggedIn) {
                this.navController.navigateRoot(['/']);
            }
        });

        this.registrationApiService.getAgreementList().pipe(this.takeUntil()).subscribe((agreementList: AgreementInterface[]) => {
            this.agreementList = agreementList;
        });

    }

    ionViewWillEnter() {
        this.platform.backButton.subscribe(() => {
            this.navController.navigateBack(['auth', 'login']);
        });
    }

    openLoginPage(): void {
        this.navController.navigateBack(['auth', 'login']);
    }

    back() {
        this.navController.pop();
    }

    getAgreements(agreements: AgreementInterface[]): void {
        this.form.controls['agreements'].setValue(agreements);
    }

    getAgreementsValidity(valid: boolean): void {
        this.agreementsValid = valid;
    }

    registration(): void {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid && this.agreementsValid) {
            this.registrationApiService
                .registerAccount(this.form.value)
                .pipe(
                    map(() => {
                        const email = this.form.controls['email'].value;
                        const password = this.form.controls['password'].value;

                        return {
                            email,
                            password
                        };
                    }),
                    this.takeUntil()
                )
                .subscribe((authenticate: AuthenticateInterface) => {
                    this.store.dispatch(new Login(authenticate));
                    this.form.markAsUntouched();
                    this.form.updateValueAndValidity();
                });
        } else {
            const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }
    }

    isInvalid(formControlName: string): boolean {
        return FormItemCheckerTool.isInvalid(this.form.touched, this.form, formControlName);
    }

    getSelectInterfaceOptions(header: string): any {
        return this.selectInterfaceOptions.getCustomSelectOptions(header);
    }

    doesPasswordMatch(): boolean {
        return FormItemCheckerTool.doesPasswordMatch(
            this.form.touched,
            this.form,
            'password',
            'password2'
        );
    }

    getPasswordCheckerMessage(): string {
        return this.checkIfNewPasswordMatches() ? 'Podaj hasło' : 'Hasła muszą się zgadzać';
    }

    private checkIfNewPasswordMatches(): boolean {
        return FormItemCheckerTool.checkIfNewPasswordMatches(
            this.form,
            'password',
            'password2'
        );
    }
}
