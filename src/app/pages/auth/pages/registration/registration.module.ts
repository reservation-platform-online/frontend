import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegistrationPageRoutingModule} from './registration-routing.module';

import {RegistrationPage} from './registration.page';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {SelectInterfaceOptionsModule} from '@app/common/services/util/select-interface-options/select-interface-options.module';
import {InputModule} from '@app/common/ui/input/input.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {AgreementsModule} from '@app/common/components/agreements/agreements.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RegistrationPageRoutingModule,
        SelectInterfaceOptionsModule,
        InputModule,
        HeaderModule,
        BannerModule,
        AgreementsModule,
        FooterModule
    ],
    providers: [
        InAppBrowser
    ],
    declarations: [
        RegistrationPage
    ]
})
export class RegistrationPageModule {
}
