import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {PopoverComponent} from './components/popover/popover.component';
import {Observable, ReplaySubject} from 'rxjs';
import {Reactive} from '@app/common/cdk/reactive';
import {NavService} from '@app/common/services/util/nav.service';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {ROLE} from '@app/common/interfaces/api/role.enum';
import {map, withLatestFrom} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {selectBusinessPwsListStateList, selectCategoryList} from '@app/ngrx-store';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {BusinessPwsListInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-list.interface';
import {BusinessPwsListLoadNextPage, BusinessPwsListRefresh} from '@app/ngrx-store/business/pws/pws.actions';
import {Tools} from '@app/common/tools/tools';
import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';

@Component({
    selector: 'app-service-list',
    templateUrl: './service-list.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServiceListPage extends Reactive implements OnInit {

    @Input()
    useLikeComponent: boolean = false;

    public selectedPws: PwsInterface = null;

    public readonly roles = ROLE;

    private readonly pwsList$: ReplaySubject<BusinessPwsListInterface> = new ReplaySubject<BusinessPwsListInterface>(1);

    constructor(
        private readonly popoverController: PopoverController,
        private readonly navService: NavService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly store: Store,
    ) {
        super();
    }

    get onPwsList$(): Observable<BusinessPwsListInterface> {
        return this.pwsList$.asObservable();
    }

    ngOnInit() {

        if (!this.useLikeComponent) {

            this.refreshContent();

        }

        this.store.select(selectBusinessPwsListStateList).pipe(
            this.takeUntil(),
            withLatestFrom(this.store.select(selectCategoryList).pipe(this.takeUntil())),
            map(([pwsList, categoryList]: [BusinessPwsListInterface, CategoryListInterface]) => {
                pwsList = Tools.copyObject(pwsList);
                if (pwsList && categoryList) {
                    for (const pws of pwsList.models) {
                        pws.service.categoryName = categoryList.models.find((item) => item.id === pws.service.categoryId).name;
                    }
                }
                return pwsList;
            })).subscribe((pwsList: BusinessPwsListInterface) => {
            // TODO add next page
            this.pwsList$.next(pwsList);

            this.changeDetectorRef.detectChanges();
        });

    }

    public refreshContent(event = null): void {

        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }

        this.store.dispatch(new BusinessPwsListRefresh());

    }

    public openPopover(ev: any): void {
        this.popoverController.create({
            component: PopoverComponent,
            event: ev,
            mode: 'md',
            translucent: true
        }).then((popover) => {
            popover.present();
        });
    }

    back() {
        this.navService.back();
    }

    loadData() {
        this.store.dispatch(new BusinessPwsListLoadNextPage());
    }

    trackByFn(service: SearchItemInterface) {
        return service && service.id;
    }

}
