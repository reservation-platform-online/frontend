import {Component, OnInit} from '@angular/core';
import {ModalController, Platform, PopoverController} from '@ionic/angular';
import {ServiceComponent} from '@app/common/modules/client/company-form/service/service.component';
import {PwsInterface} from '@app/common/interfaces/modules/company/worker/pws.interface';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {combineLatest} from 'rxjs';
import {Tools} from '@app/common/tools/tools';
import {ConvertTool} from '@app/common/tools/convert.tool';
import {RegistrationApiService} from '@app/common/services/api/modules/client/registration/registration-api.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {WorkerApiService} from '@app/common/services/api/modules/company/worker/worker-api.service';
import {v4 as uuidv4} from 'uuid';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import * as fromStore from '@app/ngrx-store';
import {Reactive} from '@app/common/cdk/reactive';
import {Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import {BusinessPwsListRefresh} from '@app/ngrx-store/business/pws/pws.actions';

@Component({
  selector: 'app-popover',
  template: `
    <ion-list>
      <ion-item lines="none" button (click)="openForm()">
        Dodaj
      </ion-item>
    </ion-list>
  `
})
export class PopoverComponent extends Reactive implements OnInit {

  public profile: ProfileInterface;

  constructor(
      private readonly modalController: ModalController,
      private readonly popoverController: PopoverController,
      private readonly platform: Platform,
      private readonly store: Store,
      private readonly registrationService: RegistrationApiService,
      private readonly serviceApiService: ServiceApiService,
      private readonly workerApiService: WorkerApiService,
  ) {
    super();
  }

  ngOnInit() {

    this.store.select(fromStore.selectProfileData).pipe(this.takeUntil()).subscribe((profile: ProfileInterface) => {

      this.profile = profile;
      // this.openServiceModal();

    });

  }

  public openForm(): void {

    this.dismissClick();

    this.modalController.create({
      component: ServiceComponent,
      componentProps: {},
      cssClass: 'ion-page-without-padding-top'
    }).then((modal) => {
      modal.present();

      modal.onWillDismiss().then((result) => {
        if (result && result.data) {

          result.data.id = uuidv4();

          const pws: PwsInterface = {
            companyId: '',
            enabled: 0,
            id: uuidv4(),
            limitReservationPerSlot: 0,
            pointId: '',
            service: result.data as ServiceInterface,
            status: 0,
            workTimeList: [],
            worker: {
              companyId: null,
              enabled: 1,
              firstName: this.profile.firstName,
              id: uuidv4(),
              userEmail: this.profile.email,
              lastName: this.profile.lastName,
              photo: this.profile.photo,
              position: null,
              score: 0,
              status: 1
            }
          };

          this.save(pws);
        }
      });
    });
  }

  private save(pws): void {

    let serviceId: string = null;
    let workerId: string = null;

    combineLatest(
        this.serviceApiService
            .postCreate(Tools.copyObject(pws.service))
            .pipe(
                this.takeUntil(),
                map((service) => {
                  if (service) {
                    serviceId = `${service}`;
                    this.saveUploadImage(serviceId, pws.service.image);
                  }
                  return serviceId;
                })
            ),
        this.workerApiService
            .postCreate(Tools.copyObject(pws.worker))
            .pipe(
                this.takeUntil(),
                map((worker) => {
                  if (worker) {
                    workerId = `${worker}`;
                  }
                  return workerId;
                })
            )
    ).pipe(
        this.takeUntil(),
        map(([worker, service]) => {

          if (worker && service) {

            this.serviceApiService.postConnectWorker({
              workers: `${workerId}`
            }, serviceId)
                .pipe(
                    this.takeUntil()
                )
                .subscribe((_) => {
                  this.store.dispatch(new BusinessPwsListRefresh());
                });

          }

          return [worker, service];
        })
    ).subscribe();

  }

  private saveUploadImage(uuid: string, croppedImage): void {
    this.serviceApiService.saveImage(uuid, ConvertTool.blobToFile(ConvertTool.b64toBlob(croppedImage), uuid)).pipe(this.takeUntil()).subscribe();
  }

  async dismissClick() {
    await this.popoverController.dismiss();
  }

}
