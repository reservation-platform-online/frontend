import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ServiceListPageRoutingModule} from './service-list-routing.module';

import {ServiceListPage} from './service-list.page';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {PopoverComponent} from '@app/pages/business/pages/service-list/components/popover/popover.component';
import {WorkerModule} from '@app/common/modules/client/worker/worker.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {BarRatingModule} from 'ngx-bar-rating';
import {StringTimeConvertModule} from '@app/common/pipes/convert/string-time/string-time-convert.module';
import {WeekdayNameModule} from '@app/common/pipes/weekday-name/weekday-name.module';
import {WorkTimeModalModule} from '@app/common/modal/client/business/work-time-modal/work-time-modal.module';
import {FormReservationModalModule} from '@app/common/modal/client/business/form-reservation-modal/form-reservation-modal.module';
import {CalendarDaysSlotsModule} from '@app/common/modules/calendar-days-slots/calendar-days-slots.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ServiceListPageRoutingModule,
        FooterModule,
        DefaultContainerModule,
        HeaderModule,
        WorkerModule,
        ImgModule,
        BarRatingModule,
        StringTimeConvertModule,
        WeekdayNameModule,
        WorkTimeModalModule,
        FormReservationModalModule,
        CalendarDaysSlotsModule,
    ],
    exports: [
        ServiceListPage
    ],
    declarations: [
        ServiceListPage,
        PopoverComponent
    ]
})
export class ServiceListPageModule {}
