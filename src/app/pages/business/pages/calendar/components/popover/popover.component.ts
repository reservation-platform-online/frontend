import {Component} from '@angular/core';
import {ModalController, PopoverController} from '@ionic/angular';
import {FormReservationModalComponent} from '@app/common/modal/client/business/form-reservation-modal/form-reservation-modal.component';

@Component({
    selector: 'app-popover',
    template: `
        <ion-list>
            <ion-item disabled lines="none" button (click)="openForm()">
                <ion-icon name="custom-lock-closed-outline"></ion-icon>
                Dodaj
            </ion-item>
            <ion-item disabled lines="none" button>
                <ion-icon name="custom-lock-closed-outline"></ion-icon>
                Eksportuj do kalendarza
            </ion-item>
        </ion-list>
    `,
})
export class PopoverComponent {

    constructor(
        private readonly modalController: ModalController,
        private readonly popoverController: PopoverController,
    ) {
    }

    public openForm(): void {

        this.modalController.create({
            component: FormReservationModalComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present().then(() => {
                this.dismissClick();
            });

        });
    }

    dismissClick() {
        return this.popoverController.dismiss();
    }

}
