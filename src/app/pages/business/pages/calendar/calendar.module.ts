import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CalendarPageRoutingModule} from './calendar-routing.module';

import {CalendarPage} from './calendar.page';
import {ReservationListWithDaysModule} from '@app/common/components/reservation-list-with-days/reservation-list-with-days.module';
import {PopoverComponent} from '@app/pages/business/pages/calendar/components/popover/popover.component';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {ReservationStatusModule} from '@app/common/pipes/reservation/status/status.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CalendarPageRoutingModule,
        ReservationListWithDaysModule,
        HeaderModule,
        FooterModule,
        ReservationStatusModule
    ],
    exports: [
        CalendarPage
    ],
    declarations: [
        CalendarPage,
        PopoverComponent
    ]
})
export class CalendarPageModule {
}
