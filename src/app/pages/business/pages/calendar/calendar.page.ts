import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import * as moment from 'moment';
import {Observable} from 'rxjs';
import {ReservationListInterface} from '@app/common/interfaces/modules/company/reservation/reservation-list.interface';
import {ModalController, NavController, PopoverController} from '@ionic/angular';
import {PopoverComponent} from '@app/pages/business/pages/calendar/components/popover/popover.component';
import {FilterComponent} from '@app/common/components/reservation-list-with-days/filter/filter.component';
import {Store} from '@ngrx/store';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {
    selectBusinessReservationStateList,
    selectBusinessReservationStateParams
} from '@app/ngrx-store/business/reservation/reservation.index';
import {BusinessReservationListRefresh, BusinessReservationParams} from '@app/ngrx-store/business/reservation/reservation.actions';
import {CategoryModalComponent} from '@app/common/modal/category-modal/category-modal.component';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {environment} from '@envi/environment';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarPage extends Reactive implements OnInit, OnChanges {

    @Input()
    useLikeComponent: boolean = false;

    @Input()
    refresh: boolean = false;

    public startDate: moment.Moment;
    public endDate: moment.Moment;
    private filter: {
        serviceId?: string,
        status?: number,
        charge?: number,
        categoryId?: number
    } = null;

    public params: BusinessReservationParamsInterface;

    constructor(
        private readonly navService: NavController,
        private readonly popoverController: PopoverController,
        private readonly store: Store,
        private readonly alertPromptService: AlertPromptService,
        private readonly modalController: ModalController,
        private readonly changeDetectorRef: ChangeDetectorRef
    ) {
        super();
    }

    ngOnChanges(changes: SimpleChanges): void {

        if (changes.refresh) {
            if (this.refresh) {
                this.refreshContent();
                this.refresh = false;
            }
        }

    }

    get reservationList$(): Observable<ReservationListInterface> {

        return this.store.select(selectBusinessReservationStateList).pipe(this.takeUntil()) as Observable<ReservationListInterface>;

    }

    public openFilter(): void {

        this.modalController.create({
            component: FilterComponent,
            componentProps: {
                filter: this.filter
            },
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                console.log(result);
                // TODO emit filter
            });
        });

    }

    ngOnInit() {

        this.store.select(selectBusinessReservationStateParams)
            .pipe(
                this.takeUntil()
            )
            .subscribe((params: BusinessReservationParamsInterface) => {
                this.params = params;
            });

        this.startDate = moment().locale('pl');
        this.endDate = moment().add(7, 'days').locale('pl');
    }

    public refreshContent(event = null): void {
        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }

        if (this.startDate && this.endDate) {

            this.store.dispatch(new BusinessReservationParams({
                ...this.params,
                start: this.startDate.format('YYYY-MM-DD'),
                end: this.endDate.format('YYYY-MM-DD')
            }));
            this.store.dispatch(new BusinessReservationListRefresh());

        }

        this.changeDetectorRef.detectChanges();
    }

    public selectEndDate($event: any): void {
        this.endDate = $event;
        this.refreshContent();
    }

    public selectStartDate($event: any): void {
        this.startDate = $event;

    }

    public openPopover(ev: any): void {
        this.popoverController.create({
            component: PopoverComponent,
            event: ev,
            mode: 'md',
            translucent: true
        }).then((popover) => {
            popover.present();
        });
    }

    public back(): void {
        this.navService.back();
    }

    openModalCategory() {

        this.modalController.create({
            component: CategoryModalComponent,
            componentProps: {
                showAllCategoryBtn: !!this.params?.categoryId
            },
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result && result.data) {
                    this.store.dispatch(new BusinessReservationParams({
                        ...this.params,
                        categoryName: result.data.name,
                        categoryId: result.data.id
                    }));
                    this.changeDetectorRef.detectChanges();
                    this.refreshContent();
                }
            });
        });

    }

    openAlertStatusList() {

        const inputs = [];

        const statusList = environment?.data?.reservation?.status ?? [];
        for (let i = 0; i < statusList.length; i++) {

            inputs.push({
                name: 'status',
                label: statusList[i],
                type: 'checkbox',
                value: i,
                checked: this.params.statusList.includes(i)
            });

        }

        this.alertPromptService.presentAlert('', {
            header: 'Wybierasz statusy',
            inputs,
            buttons: [
                {
                    cssClass: 'cancel-button',
                    text: 'Anuluj',
                    handler: _ => {
                    }
                },
                {
                    cssClass: 'ok-button',
                    text: 'Dalej',
                    handler: result => {
                        this.store.dispatch(new BusinessReservationParams({
                            ...this.params,
                            statusList: result
                        }));
                        this.changeDetectorRef.detectChanges();
                        this.refreshContent();
                        return true;

                    }
                }
            ]
        });
    }
}
