import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController, NavController} from '@ionic/angular';
import {Store} from '@ngrx/store';
import {selectCategoryList} from '@app/ngrx-store';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceInterface} from '@app/common/interfaces/modules/company/service/service.interface';
import {Tools} from '@app/common/tools/tools';
import {CategoryModalComponent} from '@app/common/modal/category-modal/category-modal.component';
import {LoadingService} from '@app/common/services/util/loading/loading.service';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ConvertTool} from '@app/common/tools/convert.tool';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html'
})
export class FormComponent extends Reactive implements OnInit {

    @Input()
    public service: ServiceInterface;

    public form: FormGroup;

    public submitted: boolean = false;
    private categories: CategoryListInterface;

    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '0',
        maxHeight: 'auto',
        width: 'auto',
        minWidth: '0',
        translate: 'yes',
        enableToolbar: true,
        showToolbar: true,
        placeholder: 'Wpisz opis swojej usługi...',
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        uploadWithCredentials: false,
        sanitize: false,
        outline: false,
        toolbarPosition: 'top',
        toolbarHiddenButtons: [
            [
                'underline',
                'strikeThrough',
                'subscript',
                'superscript',
                'justifyLeft',
                'justifyCenter',
                'justifyRight',
                'justifyFull',
                'indent',
                'outdent',
                'insertUnorderedList',
                'insertOrderedList',
                'heading',
                'fontName'
            ],
            [
                'fontSize',
                'customClasses',
                'insertImage',
                'insertVideo',
                'insertHorizontalRule'
            ]
        ]
    };

    //
    // Start: Image cropper
    //

    @ViewChild('bannerInput')
    bannerInputVar: ElementRef;

    imageChangedEvent: any = null;
    croppedImage: any = null;

    //
    // Finish: Image cropper
    //

    public selectedSegment: 'basic' | 'advanced' = 'basic';

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly loadingService: LoadingService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
        private readonly elementRef: ElementRef
    ) {
        super();

    }

    segmentChanged($event: any) {
        this.selectedSegment = $event.detail.value;
    }

    //
    // Start: Image cropper
    //

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }

    imageLoaded() {
        // show cropper
    }

    cropperReady() {
        // cropper ready
    }

    loadImageFailed() {
        // TODO request for save logo
        // show message
    }

    //
    // Finish: Image cropper
    //

    ngOnInit() {

        this.loadingService.presentLoadingWithOptions({
            id: 'LoadingCompanyServiceForm'
        }).then(() => {


            this.store.select(selectCategoryList).pipe(this.takeUntil()).subscribe((categories: CategoryListInterface) => {
                this.categories = categories;
            });

            if (this.service) {

                this.initForm();

            }

        });
    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    openFileSelect() {

        // @ts-ignore
        document.querySelector('.select-file-input input').click();
        // todo write for native

    }

    create() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const value = Tools.clearObject(this.form.value);
            delete value.isSubmitted;

            this.serviceApiService
                .putEdit(value, this.service.id)
                .pipe(
                    this.takeUntil()
                )
                .subscribe((result) => {
                    this.form.markAsUntouched();
                    this.form.updateValueAndValidity();
                    if (result) {
                        this.saveUploadImage();
                    }
                });
        } else {
            const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

    }

    private checkCategoryField() {

        if (this.form.controls['categoryId'] && this.form.controls['categoryId'].value && this.categories) {

            const category = this.categories.models.find((item) => item.id === Number(this.form.controls['categoryId'].value));
            this.form.controls['categoryId'].setValue(category.id);
            this.form.controls['categoryName'].setValue(category.name);

        }

    }

    private initForm(): void {

        this.form = this.formBuilder.group({
            categoryName: ['', [Validators.required]],
            categoryId: [this.service.categoryId, [Validators.required]],
            name: [this.service.name, [Validators.required]],
            description: [this.service.description],
            price: [this.service.price, [Validators.required]],
            priceTo: [this.service.priceTo],
            discount: [this.service.discount],
            discountIsPercent: [this.service.discountIsPercent],
            earliestReservationTime: [this.service.earliestReservationTime],
            earliestReservationDate: [this.service.earliestReservationDate],
            lastReservationDate: [this.service.lastReservationDate],
            paymentIsImportant: [this.service.paymentIsImportant],
            interval: [this.service.interval],
            image: [true, [Validators.required]],
            timerTimeForPay: [this.service.timerTimeForPay]
        });

        this.checkCategoryField();

        this.loadingService.dismissLoader('LoadingCompanyServiceForm');

    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    openModalCategory() {

        this.modalController.create({
            component: CategoryModalComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

            modal.onWillDismiss().then((result) => {
                if (result && result.data) {
                    this.form.get('categoryName').setValue(result.data.name);
                    this.form.get('categoryId').setValue(result.data.id);
                }
            });
        });

    }

    private saveUploadImage() {
        if (this.croppedImage) {

            this.serviceApiService.saveImage(
                this.service.id,
                ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), this.service.id)).subscribe(() => {

                this.clearNewImg();

                this.dismissModal();

            });
        } else {

            this.dismissModal();
        }

    }

    clearNewImg() {

        this.croppedImage = null;
        this.imageChangedEvent = null;
        if (this.bannerInputVar) {
            this.bannerInputVar.nativeElement.value = '';

        }

    }

}
