import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ServicePageRoutingModule} from './service-routing.module';

import {ServicePage} from './service.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {CompanyServiceViewModule} from '@app/common/components/view/service/company-service-view.module';
import {FormComponent} from '@app/pages/business/pages/service/components/form/form.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {InputModule} from '@app/common/ui/input/input.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {AngularEditorModule} from '@kolkov/angular-editor';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ServicePageRoutingModule,
        HeaderModule,
        FooterModule,
        DefaultContainerModule,
        CompanyServiceViewModule,
        ImageCropperModule,
        InputModule,
        ReactiveFormsModule,
        ImgModule,
        AngularEditorModule
    ],
  declarations: [
      ServicePage,
      FormComponent
  ]
})
export class ServicePageModule {}
