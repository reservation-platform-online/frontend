import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {HeaderService} from '@app/common/services/header/header.service';
import {ActivatedRoute} from '@angular/router';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {Observable} from 'rxjs';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {FormComponent} from '@app/pages/business/pages/service/components/form/form.component';
import {Store} from '@ngrx/store';
import {selectBusinessPwsItemStateItem, selectBusinessReservationStateParams} from '@app/ngrx-store';
import {map} from 'rxjs/operators';
import {BusinessPwsItemLoad, BusinessPwsParams} from '@app/ngrx-store/business/pws/pws.actions';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {Tools} from '@app/common/tools/tools';
import {SearchItemServiceInterface} from '@app/common/interfaces/modules/client/pws/search-item-service.interface';

@Component({
    selector: 'app-service',
    templateUrl: './service.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicePage extends Reactive implements OnInit {

    private params: BusinessReservationParamsInterface;

    constructor(
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly serviceApiService: ServiceApiService,
        private readonly headerService: HeaderService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly store: Store,
        private readonly navService: NavService,
        private readonly alertPromptService: AlertPromptService,
        private readonly activatedRoute: ActivatedRoute,
    ) {
        super();
    }

    get onPws$(): Observable<SearchItemInterface> {
        return this.store.select(selectBusinessPwsItemStateItem)
            .pipe(
                this.takeUntil(),
                map((item) => Tools.copyObject(item))
            ) as Observable<SearchItemInterface>;
    }

    ngOnInit() {
        this.headerService.setTitle('Usługi');
        this.store.select(selectBusinessReservationStateParams)
            .pipe(
                this.takeUntil()
            )
            .subscribe((params: BusinessReservationParamsInterface) => {
                this.params = Tools.copyObject(params);
            });

        this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {

            if (params['id']) {

                this.params['itemId'] = params['id'];
                this.store.dispatch(new BusinessPwsParams({
                    ...this.params,
                }));
                this.refreshContent();

            }

        });

    }

    public refreshContent($event: any = null): void {
        if ($event) {
            $event.target.complete();
        }
        this.store.dispatch(new BusinessPwsItemLoad());
        this.changeDetectorRef.detectChanges();
    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    changeEnabled(item: SearchItemInterface) {

        if (item.service.enabled) {
            item.service.enabled = 1;
        } else {
            item.service.enabled = 0;
        }

        if (item.service.enabled === 1) {
            this.alertPromptService.presentAlert('Czy na pewno chcesz odłączyć pracownika?', {
                buttons: [
                    {
                        text: 'Nie',
                        handler: () => {
                            item.service.enabled = 1;
                            this.changeDetectorRef.detectChanges();
                        }
                    },
                    {
                        text: 'Tak',
                        handler: () => {
                            this.serviceApiService.putToggleEnable(item.service.id).pipe(this.takeUntil()).subscribe(() => {
                                item.service.enabled = 0;
                            }, () => {
                                item.service.enabled = 1;
                            });
                        }
                    }
                ]
            });

        } else if (item.service.enabled === 0) {

            this.serviceApiService.putToggleEnable(item.service.id).pipe(this.takeUntil()).subscribe(() => {
                item.service.enabled = 1;
            }, () => {
                item.service.enabled = 0;
            });

        }

    }

    openEdit(service: SearchItemServiceInterface) {

        this.modalController.create({
            component: FormComponent,
            cssClass: 'ion-page-without-padding-top',
            componentProps: {
                service
            }
        }).then((modal) => {
            modal.present().then(() => {
            });

            modal.onDidDismiss().then(() => {
                this.refreshContent();
            });

        });

    }

    back() {
        this.navService.back({
            cut: 2
        });
    }

}
