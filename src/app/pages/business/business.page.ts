import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ScreenSizeService} from '@app/common/services/screen-size/screen-size.service';
import {Observable} from 'rxjs';
import {WorkerIdService} from '@app/common/services/api/modules/worker/worker-id.service';
import * as fromStore from '@app/ngrx-store';
import {selectBusinessReservationStateParams} from '@app/ngrx-store';
import {Store} from '@ngrx/store';
import {CompanyIdService} from '@app/common/services/api/modules/company/company-id.service';
import {Reactive} from '@app/common/cdk/reactive';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';
import {ModalController, PopoverController} from '@ionic/angular';
import {ROLE} from '@app/common/interfaces/api/role.enum';
import {PopoverComponent} from '@app/pages/business/components/popover/popover.component';
import {NavService} from '@app/common/services/util/nav.service';
import {FormReservationModalComponent} from '@app/common/modal/client/business/form-reservation-modal/form-reservation-modal.component';
import {BusinessWorkerLoad} from '@app/ngrx-store/business/business.actions';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import * as moment from 'moment';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {BusinessPwsListRefresh} from '@app/ngrx-store/business/pws/pws.actions';
import {BusinessReservationListRefresh, BusinessReservationParams} from '@app/ngrx-store/business/reservation/reservation.actions';

@Component({
    selector: 'app-business',
    templateUrl: './business.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BusinessPage extends Reactive implements OnInit {

    public selectedPage: 'services' | 'reservations' = 'reservations';

    public refreshComponent: boolean = false;

    public readonly roles = ROLE;

    constructor(
        private readonly store: Store,
        private readonly popoverController: PopoverController,
        private readonly workerIdService: WorkerIdService,
        private readonly companyIdService: CompanyIdService,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly screenSizeService: ScreenSizeService
    ) {

        super();

    }

    get worker$(): Observable<WorkerInterface> {
        return this.store.select(fromStore.selectBusinessWorkerStateWorker).pipe(this.takeUntil()) as Observable<WorkerInterface>;
    }

    ionViewWillEnter() {

        this.refreshContent(null);

    }

    selectPage(services: 'services' | 'reservations') {
        this.selectedPage = services;
        this.refreshContent();
    }

    ngOnInit() {

        this.store.select(selectBusinessReservationStateParams).pipe(this.takeUntil()).subscribe((params: BusinessReservationParamsInterface) => {
            if (!isNotNullOrUndefined(params.start)) {
                this.store.dispatch(new BusinessReservationParams({
                    ...params,
                    start: moment().locale('pl').format('YYYY-MM-DD'),
                    end: moment().add(7, 'days').locale('pl').format('YYYY-MM-DD')
                }));
            }
        });


        this.store.select(fromStore.selectIsLoggedIn).pipe(this.takeUntil()).subscribe((isLoggedIn: boolean) => {

            if (!isLoggedIn) {

                this.navService.goHome();

            }

        });

    }

    get onIsMobile$(): Observable<boolean> {
        return this.screenSizeService.isMobileView$;
    }

    public refreshContent(event = null): void {
        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }

        this.store.dispatch(new BusinessWorkerLoad());

        if (this.navService.isNative) {

        } else {

            switch (this.selectedPage) {
                case 'services':
                    this.store.dispatch(new BusinessPwsListRefresh());
                    break;
                case 'reservations':
                    this.store.dispatch(new BusinessReservationListRefresh());
                    this.refreshComponent = true;
                    this.changeDetectorRef.detectChanges();
                    this.refreshComponent = false;
                    break;
            }

        }

        this.changeDetectorRef.detectChanges();
    }

    openPopover(ev: any): void {
        this.popoverController.create({
            component: PopoverComponent,
            event: ev,
            mode: 'md',
            translucent: true
        }).then((popover) => {
            popover.present();
        });
    }

    public getColorItemMenu(selectedPage: 'services' | 'reservations'): string {
        return this.selectedPage === selectedPage ? 'dark' : 'default';
    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    public openReservationForm(): void {

        this.modalController.create({
            component: FormReservationModalComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present().then(() => {
            });

        });

    }
}
