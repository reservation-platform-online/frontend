import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HeaderService} from '@app/common/services/header/header.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ConvertTool} from '@app/common/tools/convert.tool';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';
import {selectBusinessWorkerStateWorker} from '@app/ngrx-store';
import {Tools} from '@app/common/tools/tools';
import {WorkerApiService} from '@app/common/services/api/modules/worker/worker-api.service';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {BusinessWorkerLoad} from '@app/ngrx-store/business/business.actions';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html'
})
export class FormComponent extends Reactive implements OnInit {

    public form: FormGroup;

    public submitted: boolean = false;

    //
    // Start: Image cropper
    //

    @ViewChild('bannerInput')
    bannerInputVar: ElementRef;

    imageChangedEvent: any = null;
    croppedImage: any = null;

    //
    // Finish: Image cropper
    //

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly headerService: HeaderService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
        private readonly elementRef: ElementRef,
        private readonly workerApiService: WorkerApiService
    ) {
        super();
        this.headerService.setTitle('Edycja profilu');

    }

    //
    // Start: Image cropper
    //

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }

    imageLoaded() {
        // show cropper
    }

    cropperReady() {
        // cropper ready
    }

    loadImageFailed() {
        // TODO request for save logo
        // show message
    }

    openFileSelect() {

        // @ts-ignore
        document.querySelector('.select-file-input input').click();
        // todo write for native

    }

    //
    // Finish: Image cropper
    //

    ngOnInit() {

    }

    get worker$(): Observable<WorkerInterface> {
        return this.store.select(selectBusinessWorkerStateWorker).pipe(this.takeUntil(), map((item: WorkerInterface) => {
            item = Tools.copyObject(item);
            if (item) {
                this.initForm(item);
            }
            return item;
        })) as Observable<WorkerInterface>;
    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    refreshContent($event: any = null) {
        if ($event) {
            $event.target.complete();
        }
        this.store.dispatch(new BusinessWorkerLoad());
    }

    save() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const value = Tools.copyObject(this.form.value);
            delete value.isSubmitted;
            this.workerApiService.putUpdate(value).pipe(this.takeUntil()).subscribe(() => { // TODO check, why send old data ?
                this.saveUploadImage();
                this.form.markAsUntouched();
                this.form.updateValueAndValidity();
            });
        } else {
            const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

    }

    private initForm(item: WorkerInterface) {

        if (!isNotNullOrUndefined(this.form)) {

            this.form = this.formBuilder.group({
                firstName: [item.firstName, [Validators.required]],
                lastName: [item.lastName, [Validators.required]],
            });

            this.form.valueChanges.subscribe(console.log);
            this.form.controls['firstName'].valueChanges.subscribe((firstName) => console.log(firstName));

        }

    }

    private saveUploadImage() {
        if (this.croppedImage) {

            this.workerApiService.saveImage(ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), 'company')).subscribe(() => {

                this.clearNewImg();

                this.dismissModal().then(() => {
                    this.refreshContent();
                });

            });
        } else {

            this.dismissModal().then(() => {
                this.refreshContent();
            });

        }

    }

    clearNewImg() {

        this.croppedImage = null;
        this.imageChangedEvent = null;
        if (this.bannerInputVar) {
            this.bannerInputVar.nativeElement.value = '';

        }

    }

}
