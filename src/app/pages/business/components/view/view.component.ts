import {Component, ElementRef, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder} from '@angular/forms';
import {HeaderService} from '@app/common/services/header/header.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {WorkerApiService} from '@app/common/services/api/modules/worker/worker-api.service';
import {WorkerInterface} from '@app/common/interfaces/modules/company/worker/worker.interface';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';
import {selectBusinessWorkerStateWorker} from '@app/ngrx-store';
import {Tools} from '@app/common/tools/tools';

@Component({
    selector: 'app-form',
    templateUrl: './view.component.html'
})
export class ViewComponent extends Reactive implements OnInit {

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly headerService: HeaderService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
        private readonly elementRef: ElementRef,
        private readonly alertPromptService: AlertPromptService,
        private readonly workerApiService: WorkerApiService
    ) {
        super();
        this.headerService.setTitle('Edycja profilu');

    }

    ngOnInit() {

    }

    get worker$(): Observable<WorkerInterface> {
        return this.store.select(selectBusinessWorkerStateWorker).pipe(this.takeUntil(), map(item => Tools.copyObject(item))) as Observable<WorkerInterface>;
    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    refreshContent($event: any = null) {
        if ($event) {
            $event.target.complete();
        }
        this.workerApiService.getItem().pipe(this.takeUntil()).subscribe();
    }

    changeEnabled(item: WorkerInterface) {

        if (item.enabled) {
            item.enabled = 1;
        } else {
            item.enabled = 0;
        }

        if (item.enabled === 1) {
            this.alertPromptService.presentAlert('Czy na pewno chcesz wyłączyć się?', {
                buttons: [
                    {
                        text: 'Nie',
                        handler: () => {
                            item.enabled = 1;
                        }
                    },
                    {
                        text: 'Tak',
                        handler: () => {
                            this.workerApiService.putToggleEnable().pipe(this.takeUntil()).subscribe(() => {
                                item.enabled = 0;
                            });
                        }
                    }
                ]
            });

        } else if (item.enabled === 0) {

            this.workerApiService.putToggleEnable().pipe(this.takeUntil()).subscribe(() => {
                item.enabled = 1;
            });

        }

    }

}
