import {Component, OnInit} from '@angular/core';
import {ModalController, PopoverController} from '@ionic/angular';
import {ViewComponent} from '@app/pages/business/components/view/view.component';
import {FormComponent} from '@app/pages/business/components/form/form.component';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html'
})
export class PopoverComponent implements OnInit {

  constructor(
      private readonly modalController: ModalController,
      private readonly popoverController: PopoverController,
  ) { }

  ngOnInit() {}

  openForm() {

    this.modalController.create({
      component: FormComponent,
      cssClass: 'ion-page-without-padding-top'
    }).then((modal) => {
      modal.present().then(() => {
        this.dismissClick();
      });

    });
  }

  openView() {

    this.modalController.create({
      component: ViewComponent,
      cssClass: 'ion-page-without-padding-top'
    }).then((modal) => {
      modal.present().then(() => {
        this.dismissClick();
      });

    });
  }

  async dismissClick() {
    await this.popoverController.dismiss();
  }

}
