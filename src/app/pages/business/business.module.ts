import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {BusinessPageRoutingModule} from './business-routing.module';

import {BusinessPage} from './business.page';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {PopoverComponent} from '@app/pages/business/components/popover/popover.component';
import {FormComponent} from '@app/pages/business/components/form/form.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {InputModule} from '@app/common/ui/input/input.module';
import {ViewComponent} from '@app/pages/business/components/view/view.component';
import {ReservationListWithDaysModule} from '@app/common/components/reservation-list-with-days/reservation-list-with-days.module';
import {ServiceListPageModule} from '@app/pages/business/pages/service-list/service-list.module';
import {CalendarPageModule} from '@app/pages/business/pages/calendar/calendar.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        BusinessPageRoutingModule,
        FooterModule,
        HeaderModule,
        ImgModule,
        DefaultContainerModule,
        ImageCropperModule,
        ReactiveFormsModule,
        InputModule,
        ReservationListWithDaysModule,
        ServiceListPageModule,
        CalendarPageModule
    ],
    declarations: [
        BusinessPage,
        FormComponent,
        PopoverComponent,
        ViewComponent
    ]
})
export class BusinessPageModule {
}
