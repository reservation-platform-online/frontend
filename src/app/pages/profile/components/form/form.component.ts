import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HeaderService} from '@app/common/services/header/header.service';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {Store} from '@ngrx/store';
import {selectProfileData} from '@app/ngrx-store';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {ConvertTool} from '@app/common/tools/convert.tool';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {ProfileApiService} from '@app/common/services/api/modules/client/profile/profile-api.service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html'
})
export class FormComponent extends Reactive implements OnInit {

    public form: FormGroup;

    public submitted: boolean = false;

    public profile$: Observable<ProfileInterface>;

    //
    // Start: Image cropper
    //

    @ViewChild('bannerInput')
    bannerInputVar: ElementRef;

    imageChangedEvent: any = null;
    croppedImage: any = null;

    //
    // Finish: Image cropper
    //

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly headerService: HeaderService,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
        private readonly elementRef: ElementRef,
        private readonly profileApiService: ProfileApiService
    ) {
        super();
        this.headerService.setTitle('Edycja profilu nadawcy');

    }

    //
    // Start: Image cropper
    //

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }

    imageLoaded() {
        // show cropper
    }

    cropperReady() {
        // cropper ready
    }

    loadImageFailed() {
        // TODO request for save logo
        // show message
    }

    openFileSelect() {

        // @ts-ignore
        document.querySelector('.select-file-input input').click();
        // todo write for native

    }

    //
    // Finish: Image cropper
    //

    ngOnInit() {

        this.profile$ = (this.store.select(selectProfileData).pipe(
            this.takeUntil(),
            tap((profile: ProfileInterface) => {
                this.initForm(profile);
            })) as Observable<ProfileInterface>);
    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    refreshContent($event: any = null) {
        if ($event) {
            $event.target.complete();
        }
        this.profileApiService.getProfile().pipe(this.takeUntil()).subscribe();
    }


    save() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const value = this.form.value;
            delete value.isSubmitted;
            this.profileApiService.putUpdate(value).pipe(this.takeUntil()).subscribe(() => {
                this.form.markAsUntouched();
                this.form.updateValueAndValidity();
                this.saveUploadImage();
            });
        } else {
            const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

    }

    private initForm(profile: ProfileInterface) {

        this.form = this.formBuilder.group({
            phone: [profile.phone],
            birthday: [profile.birthday],
            firstName: [profile.firstName, [Validators.required]],
            lastName: [profile.lastName, [Validators.required]],
        });

    }

    private saveUploadImage() {
        if (this.croppedImage) {

            this.profileApiService.saveImage(ConvertTool.blobToFile(ConvertTool.b64toBlob(this.croppedImage), 'company')).subscribe(() => {

                this.clearNewImg();

                this.dismissModal().then(() => {
                    this.refreshContent();
                });

            });
        } else {

            this.dismissModal().then(() => {
                this.refreshContent();
            });

        }

    }

    clearNewImg() {

        this.croppedImage = null;
        this.imageChangedEvent = null;
        if (this.bannerInputVar) {
            this.bannerInputVar.nativeElement.value = '';

        }

    }

}
