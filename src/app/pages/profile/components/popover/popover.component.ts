import {Component, OnInit} from '@angular/core';
import {ModalController, PopoverController} from '@ionic/angular';
import {FormComponent} from '@app/pages/profile/components/form/form.component';
import {ChangePasswordComponent} from '@app/pages/profile/components/change-password/change-password.component';
import {AlertPromptService} from '@app/common/services/util/alert-prompt/alert-prompt.service';

@Component({
    selector: 'app-popover',
    templateUrl: './popover.component.html'
})
export class PopoverComponent implements OnInit {

    constructor(
        private readonly modalController: ModalController,
        private readonly alertPromptService: AlertPromptService,
        private readonly popoverController: PopoverController,
    ) {
    }

    ngOnInit() {
    }

    openForm() {
        this.dismissClick();
        this.modalController.create({
            component: FormComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

        });
    }

    openChangePassword() {
        this.dismissClick();
        this.modalController.create({
            component: ChangePasswordComponent,
            cssClass: 'ion-page-without-padding-top'
        }).then((modal) => {
            modal.present();

        });
    }

    async dismissClick() {
        await this.popoverController.dismiss();
    }

    openDeleteAccount() {
        const message: string = '<p>Twoje dane zostaną usunięte oraz ' +
            'połączone konta z innych serwisów zostaną wylogowane i odinstalowane, ' +
            'nie będziesz mieć możliwość do odświeżania danych, co to znaczy?</p> ' +
            '<p>Następne logowanie przez inny serwis spowoduję tworzeniu nowego konta lub nie będzie możliwości ' +
            'logowania się przez login i haśło do póki nie założysz nowego konta przez strone z formularzem rejestracjnym</p>';
        
        this.alertPromptService.presentAlert(message, {
            header: 'Czy na pewno chcesz usunąć konto?',
        });
    }
}
