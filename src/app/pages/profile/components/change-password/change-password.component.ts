import {Component, ElementRef, OnInit} from '@angular/core';
import {Reactive} from '@app/common/cdk/reactive';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController, NavController} from '@ionic/angular';
import {NavService} from '@app/common/services/util/nav.service';
import {ServiceApiService} from '@app/common/services/api/modules/company/service/service-api.service';
import {Store} from '@ngrx/store';
import {ProfileApiService} from '@app/common/services/api/modules/client/profile/profile-api.service';
import {PasswordTool} from '@app/common/tools/password.tool';
import {Tools} from '@app/common/tools/tools';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent extends Reactive implements OnInit {

    public form: FormGroup;

    public submitted: boolean = false;

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly navController: NavController,
        private readonly modalController: ModalController,
        private readonly navService: NavService,
        private readonly serviceApiService: ServiceApiService,
        private readonly store: Store,
        private readonly elementRef: ElementRef,
        private readonly profileApiService: ProfileApiService
    ) {
        super();

    }

    ngOnInit() {

        this.initForm();

    }

    async dismissModal() {
        await this.modalController.dismiss(null, 'cancel');
    }

    refreshContent($event: any = null) {
        if ($event) {
            $event.target.complete();
        }
        this.profileApiService.getProfile().pipe(this.takeUntil()).subscribe();
    }

    save() {
        this.form.markAsTouched();
        this.form.updateValueAndValidity();
        if (this.form.valid) {
            const value = Tools.copyObject(this.form.value);
            delete value.isSubmitted;
            this.profileApiService.changePassword$(value).subscribe((_) => {
                this.form.markAsUntouched();
                this.form.updateValueAndValidity();
                this.dismissModal();
            });
        } else {
            const firstInvalid = this.elementRef.nativeElement.querySelector('.form-item.ng-invalid');
            firstInvalid.scrollIntoView({behavior: 'smooth', block: 'start'});
        }

    }

    private initForm() {

        this.form = this.formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(6)]],
            newPassword: ['', [Validators.required, Validators.minLength(6)]],
            newPasswordRepeat: ['', [Validators.required, Validators.minLength(6)]],
        }, {validators: PasswordTool.checkPasswords});

    }

}
