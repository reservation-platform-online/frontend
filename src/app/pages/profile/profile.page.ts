import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '@app/ngrx-store';
import {selectCategoryList, selectProfileData, selectPwsItem, selectReservationList, selectReservationListHistory} from '@app/ngrx-store';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {Reactive} from '@app/common/cdk/reactive';
import {HeaderService} from '@app/common/services/header/header.service';
import {ActivatedRoute} from '@angular/router';
import {ConnectExternal} from '@app/ngrx-store/profile/profile.actions';
import {ModalController, NavController, PopoverController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {ReservationListInterface} from '@app/common/interfaces/modules/client/reservation/reservation-list.interface';
import {map, withLatestFrom} from 'rxjs/operators';
import {ReservationInterface} from '@app/common/interfaces/modules/client/reservation/reservation.interface';
import * as ReservationActions from '@app/ngrx-store/reservation/reservation.actions';
import * as fromReservation from '@app/ngrx-store/reservation/reservation.actions';
import {ReservationFormComponent} from '@app/common/modules/client/reservation-form/reservation-form.component';
import {CommentFormComponent} from '@app/common/modal/client/comment-form/comment-form.component';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {ParamsCalendar} from '@app/ngrx-store/pws/pws.actions';
import * as moment from 'moment';
import {CalendarDaysSlotsComponent} from '@app/common/modules/calendar-days-slots/calendar-days-slots.component';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {Tools} from '@app/common/tools/tools';
import {PopoverComponent} from '@app/pages/profile/components/popover/popover.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfilePage extends Reactive implements OnInit {

    public profile: ProfileInterface;

    public status = {
        active: 1,
        confirmedEmail: 2
    };

    // Reservation


    public selectedSegment: 'active' | 'history' = 'active';

    public loading: boolean = true;

    skatingPasses: any;

    private selectedReservation: ReservationInterface;
    private openCalendar: boolean = false;

    constructor(
        private readonly store: Store,
        private readonly activatedRoute: ActivatedRoute,
        private readonly modalController: ModalController,
        private readonly navController: NavController,
        private readonly popoverController: PopoverController,
        private readonly headerService: HeaderService
    ) {
        super();
        this.headerService.setTitle('Profil');
    }

    public get historyItem$(): Observable<ReservationListInterface> {

        return this.store.select(selectReservationListHistory)
            .pipe(this.takeUntil()) as Observable<ReservationListInterface>;

    }

    public get activeItem$(): Observable<ReservationListInterface> {

        return this.store.select(selectReservationList).pipe(
            this.takeUntil(),
            withLatestFrom(this.store.select(selectCategoryList).pipe(this.takeUntil())),
            map(([reservationList, categoryList]: [ReservationListInterface, CategoryListInterface]) => {
                reservationList = Tools.copyObject(reservationList);
                if (reservationList && categoryList) {
                    for (const reservation of reservationList.models) {
                        reservation.service.categoryName = categoryList.models.find((item) => item.id === Number(`${reservation.service.categoryId}`)).name;
                    }
                }
                return reservationList;
            })
        ) as Observable<ReservationListInterface>;

    }

    // Reservation

    ionViewWillEnter() {

        this.refreshContent();

        this.modalController.getTop().then((result) => {

            if (result) {

                this.modalController.dismiss();

            }

        });

    }

    ngOnInit() {

        this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {

            if (params['code']) {
                if (['active', 'history'].includes(params['code'])) {
                    this.selectedSegment = params['code'];
                } else {
                    this.store.dispatch(new ConnectExternal({
                        code: atob(params['code']),
                    }));
                }
            }

            switch (this.selectedSegment) {
                case 'active':
                    this.store.dispatch(new fromReservation.Params({

                        count: 32,
                        page: 1,
                        active: 1

                    }));
                    break;
                case 'history':
                    this.store.dispatch(new fromReservation.ParamsHistory({

                        count: 32,
                        page: 1,
                        active: 0

                    }));
                    break;
            }

        });

        this.store.select(selectProfileData).pipe(this.takeUntil()).subscribe((profile: ProfileInterface) => {

            this.profile = profile;

        });

        this.store
            .select(fromStore.selectIsLoggedIn)
            .pipe(
                this.takeUntil()
            ).subscribe((isLoggedIn: boolean) => {
            if (!isLoggedIn) {
                this.navController.navigateRoot(['/']);
            }
        });

        // Reservation

        this.store.select(selectPwsItem).pipe(this.takeUntil()).subscribe((item: SearchItemInterface) => {

            if (item && this.openCalendar) {

                this.openCalendar = false;
                this.store.dispatch(new ParamsCalendar({
                    itemId: item.id,
                    start: moment().locale('pl').format('YYYY-MM-DD'),
                    end: moment().locale('pl').clone().add(7, 'days').format('YYYY-MM-DD'),
                }));

                this.modalController.create({
                    component: CalendarDaysSlotsComponent,
                    id: 'calendar-select-slot',
                    cssClass: 'ion-page-without-padding-top',
                    swipeToClose: true,
                    componentProps: {
                        item,
                        inModal: true
                    },
                    backdropDismiss: true,
                }).then((modal) => {
                    modal.present();
                    // this.store.dispatch(new fromPws.RefreshCalendar());
                    modal.onWillDismiss().then(() => {
                    });

                });

            }

        });

        this.refreshContent();


    }

    trackByFn(item: ReservationInterface) {
        return item && item.id;
    }

    refreshContent(event = null): void {
        if (event) {
            event.target.complete();
        }

        switch (this.selectedSegment) {
            case 'active':
                this.store.dispatch(new ReservationActions.RefreshList());
                break;
            case 'history':
                this.store.dispatch(new ReservationActions.RefreshListHistory());
                break;
        }
    }

    selectReservation(reservation: ReservationInterface) {
        this.selectedReservation = reservation;
        this.modalController.create({
            component: ReservationFormComponent,
            id: 'view-reservation',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            backdropDismiss: true,
            componentProps: {
                reservation,
            }
        }).then((modal) => {
            modal.present();
        });
    }

    createComment(reservation: ReservationInterface) {
        this.selectedReservation = reservation;
        this.modalController.create({
            component: CommentFormComponent,
            id: 'comment-reservation',
            cssClass: 'ion-page-without-padding-top',
            swipeToClose: true,
            backdropDismiss: true,
            componentProps: {
                reservation,
            }
        }).then((modal) => {
            modal.present();
            modal.onDidDismiss().then(() => {
                this.refreshContent();
            });
        });
    }

    segmentChanged($event: any) {

        this.selectedSegment = $event.detail.value;
        this.refreshContent();

    }

    loadData() {
        switch (this.selectedSegment) {
            case 'active':
                this.store.dispatch(new ReservationActions.LoadNextPage());
                break;
            case 'history':
                this.store.dispatch(new ReservationActions.LoadHistoryNextPage());
                break;
        }
    }

    onOpenCalendar($event: boolean) {
        this.openCalendar = $event;
    }

    openPopover(ev: any): void {
        this.popoverController.create({
            component: PopoverComponent,
            event: ev,
            mode: 'md',
            translucent: true
        }).then((popover) => {
            popover.present();
        });
    }
}
