import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ProfilePageRoutingModule} from './profile-routing.module';

import {ProfilePage} from './profile.page';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {DefaultValueConvertModule} from '@app/common/pipes/convert/default-value/default-value-convert.module';
import {FormComponent} from '@app/pages/profile/components/form/form.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {InputModule} from '@app/common/ui/input/input.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {ImgModule} from '@app/common/ui/img/img.module';
import {ReservationCardModule} from '@app/common/components/reservation-card/reservation-card.module';
import {ChangePasswordComponent} from '@app/pages/profile/components/change-password/change-password.component';
import {CommentModalModule} from '@app/common/modal/client/comment/comment-form.module';
import {PopoverComponent} from '@app/pages/profile/components/popover/popover.component';
import {CommentFormModule} from '@app/common/modal/client/comment-form/comment-form.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProfilePageRoutingModule,
        HeaderModule,
        FooterModule,
        DefaultValueConvertModule,
        ImageCropperModule,
        InputModule,
        ReactiveFormsModule,
        DefaultContainerModule,
        CommentFormModule,
        CommentModalModule,
        ImgModule,
        ReservationCardModule
    ],
    declarations: [
        ProfilePage,
        ChangePasswordComponent,
        FormComponent,
        PopoverComponent
    ]
})
export class ProfilePageModule {}
