import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {Reactive} from '@app/common/cdk/reactive';
import {HeaderService} from '@app/common/services/header/header.service';
import * as CategoryActions from '@app/ngrx-store/category/category.actions';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {AuthenticationApiService} from '@app/common/services/api/modules/client/authentication/authentication-api.service';
import {NavController} from '@ionic/angular';
import {LoginExternal} from '@app/ngrx-store/authentication/authentication.actions';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    encapsulation: ViewEncapsulation.None
})
export class HomePage extends Reactive implements OnInit {

    constructor(
        private readonly store: Store,
        private readonly activatedRoute: ActivatedRoute,
        private readonly navController: NavController,
        private readonly authenticationApiService: AuthenticationApiService,
        private readonly headerService: HeaderService
    ) {
        super();
        this.headerService.setTitle('reservation-platform');
    }

    ngOnInit() {

        this.activatedRoute.queryParamMap.pipe(this.takeUntil()).subscribe((queryParamMap: ParamMap) => {
            const params = queryParamMap['params'];
            if (params) {
                if (params['code']) {
                    this.store.dispatch(new LoginExternal({
                        code: params['code'],
                    }));
                }
            }
        });

        this.activatedRoute.params.pipe(this.takeUntil()).subscribe((params) => {

            if (params) {

                if (params['verificationCode']) {

                    this.authenticationApiService.confirm(params['verificationCode']).subscribe(() => {
                        this.navController.navigateRoot(['/']);
                    });

                }

            }

        });

    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

    refreshContent(event = null): void {
        if (event && Object.keys(event).length > 0) {
            event.target.complete();
        }
        this.store.dispatch(new CategoryActions.Load());
    }
}
