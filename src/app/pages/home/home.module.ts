import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {HomePageRoutingModule} from './home-routing.module';
import {HomePage} from './home.page';
import {IosArrowIconModule} from '@app/common/ui/icons/arrow/ios-arrow-icon.module';
import {WorkerModule} from '@app/common/modules/client/worker/worker.module';
import {CategoryModalModule} from '@app/common/modal/category-modal/category-modal.module';
import {ReservationFormModule} from '@app/common/modules/client/reservation-form/reservation-form.module';
import {CategoryModule} from '@app/common/components/category/category.module';
import {BannerModule} from '@app/common/modules/client/banner/banner.module';
import {FooterModule} from '@app/common/modules/shell/footer/footer.module';
import {HeaderModule} from '@app/common/modules/shell/header/header.module';
import {UniversalSelectModule} from '@app/common/modules/client/popover/universal-select/universal-select.module';
import {DefaultContainerModule} from '@app/common/modules/default-container/default-container.module';
import {SearchModule} from '@app/common/modules/client/search/search.module';
import {CategoryListModule} from '@app/common/components/category-list/category-list.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        ReservationFormModule,
        IosArrowIconModule,
        CategoryModalModule,
        WorkerModule,
        CategoryModule,
        BannerModule,
        FooterModule,
        HeaderModule,
        UniversalSelectModule,
        DefaultContainerModule,
        SearchModule,
        CategoryListModule
    ],
    declarations: [
        HomePage
    ]
})
export class HomePageModule {
}
