import {BusinessActions, BusinessActionsTypes} from '@app/ngrx-store/business/business.actions';
import {BusinessWorkerInterface} from '@app/common/interfaces/modules/client/business/business-worker.interface';
import {BusinessCompanyInterface} from '@app/common/interfaces/modules/client/business/business-company.interface';

export interface State {
    businessCompany: BusinessCompanyInterface;
    businessWorker: BusinessWorkerInterface;
}

export const initialState: State = {
    businessCompany: null,
    businessWorker: null,
};

export function reducer(state: State = initialState, action: BusinessActions) {
    switch (action.type) {

        case BusinessActionsTypes.BusinessWorkerLoadSuccess: {

            return {
                ...state,
                businessWorker: action.payload
            };
        }

        case BusinessActionsTypes.BusinessCompanyLoadSuccess: {

            return {
                ...state,
                businessCompany: action.payload
            };
        }

        default:
            return state;
    }
}

export const selectBusinessCompany = (state: State) => state.businessCompany;
export const selectBusinessWorker = (state: State) => state.businessWorker;
