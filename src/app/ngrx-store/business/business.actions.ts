import {Action} from '@ngrx/store';
import {BusinessCompanyInterface} from '@app/common/interfaces/modules/client/business/business-company.interface';
import {BusinessWorkerInterface} from '@app/common/interfaces/modules/client/business/business-worker.interface';

export enum BusinessActionsTypes {
    BusinessCompanyLoad = '[Business Api] Get Company',
    BusinessCompanyLoadSuccess = '[Business Api] Get Company success',
    BusinessCompanyRefresh = '[Business Api] Get Company Refresh',
    BusinessWorkerLoad = '[Business Api] Get Worker',
    BusinessWorkerLoadSuccess = '[Business Api] Get Worker success',
    BusinessWorkerRefresh = '[Business Api] Get Worker Refresh',

}

export class BusinessCompanyLoad implements Action {
    readonly type = BusinessActionsTypes.BusinessCompanyLoad;

}

export class BusinessCompanyLoadSuccess implements Action {
    readonly type = BusinessActionsTypes.BusinessCompanyLoadSuccess;

    constructor(public payload: BusinessCompanyInterface) {
    }

}

export class BusinessCompanyRefresh implements Action {
    readonly type = BusinessActionsTypes.BusinessCompanyRefresh;

}

export class BusinessWorkerLoad implements Action {
    readonly type = BusinessActionsTypes.BusinessWorkerLoad;

}

export class BusinessWorkerLoadSuccess implements Action {
    readonly type = BusinessActionsTypes.BusinessWorkerLoadSuccess;

    constructor(public payload: BusinessWorkerInterface) {
    }

}

export class BusinessWorkerRefresh implements Action {
    readonly type = BusinessActionsTypes.BusinessWorkerRefresh;

}

export type BusinessActions
    = BusinessCompanyLoad
    | BusinessCompanyLoadSuccess
    | BusinessCompanyRefresh
    | BusinessWorkerLoadSuccess
    | BusinessWorkerLoad
    | BusinessWorkerRefresh;
