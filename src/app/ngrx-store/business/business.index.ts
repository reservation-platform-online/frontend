import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromBusiness from '@app/ngrx-store/business/business.reducer';

const selectBusinessState = createFeatureSelector<fromBusiness.State>('business');


export const selectBusinessCompanyStateCompany = createSelector(
    selectBusinessState,
    fromBusiness.selectBusinessCompany
);

export const selectBusinessWorkerStateWorker = createSelector(
    selectBusinessState,
    fromBusiness.selectBusinessWorker
);
