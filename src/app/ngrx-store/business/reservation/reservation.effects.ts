import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map, withLatestFrom} from 'rxjs/operators';
import {Action, Store} from '@ngrx/store';
import {Tools} from '@app/common/tools/tools';
import {of} from 'rxjs';
import {
    BusinessActionsTypes,
    BusinessCompanyLoad,
    BusinessCompanyRefresh,
    BusinessWorkerLoad,
    BusinessWorkerRefresh
} from '@app/ngrx-store/business/business.actions';
import {AppState} from '@capacitor/core';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {BusinessReservationListInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-list.interface';
import {BusinessReservationItemInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-item.interface';
import {
    BusinessReservationActionsTypes,
    BusinessReservationItemLoad,
    BusinessReservationItemLoadSuccess,
    BusinessReservationItemRefresh,
    BusinessReservationListLoad,
    BusinessReservationListLoadNextPage,
    BusinessReservationListLoadSuccess,
    BusinessReservationListRefresh,
    BusinessReservationParams
} from '@app/ngrx-store/business/reservation/reservation.actions';
import {selectBusinessReservationStateParams} from '@app/ngrx-store';
import {ReservationApiService} from '@app/common/services/api/modules/client/business/reservation/reservation-api.service';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {NavService} from '@app/common/services/util/nav.service';
import {ModalController} from '@ionic/angular';

@Injectable()
export class BusinessReservationEffects {


    /**
     * Load
     */

    @Effect()
    reservationListLoad$ = this.actions$
        .pipe(
            ofType<BusinessReservationListLoad>(BusinessReservationActionsTypes.BusinessReservationListLoad),
            withLatestFrom(this.store.select(selectBusinessReservationStateParams)),
            exhaustMap(([_, params]: [Action, BusinessReservationParamsInterface]) => {

                    return this.reservationApiService
                        .getList$(Tools.clearObject(params))
                        .pipe(
                            map((item: BusinessReservationListInterface) => new BusinessReservationListLoadSuccess(item)),
                            catchError(() => of(new BusinessReservationListLoadSuccess({
                                total: 0,
                                models: []
                            })))
                        );
                }
            )
        );

    @Effect()
    reservationItemLoad$ = this.actions$
        .pipe(
            ofType<BusinessReservationItemLoad>(BusinessReservationActionsTypes.BusinessReservationItemLoad),
            withLatestFrom(this.store.select(selectBusinessReservationStateParams)),
            exhaustMap(([_, params]: [Action, BusinessReservationParamsInterface]) => {

                    return this.reservationApiService
                        .getItem$(Tools.clearObject(params))
                        .pipe(
                            map((item: BusinessReservationItemInterface) => new BusinessReservationItemLoadSuccess(item)),
                            catchError(() => of(new BusinessReservationItemLoadSuccess({} as BusinessReservationItemInterface)))
                        );
                }
            )
        );

    /**
     * Refresh
     */

    @Effect()
    companyRefresh$ = this.actions$
        .pipe(
            ofType<BusinessCompanyRefresh>(BusinessActionsTypes.BusinessCompanyRefresh),
            map((_) => {
                return new BusinessCompanyLoad();
            })
        );

    @Effect()
    workerRefresh$ = this.actions$
        .pipe(
            ofType<BusinessWorkerRefresh>(BusinessActionsTypes.BusinessWorkerRefresh),
            map((_) => {
                return new BusinessWorkerLoad();
            })
        );

    @Effect()
    reservationLisRefresh$ = this.actions$
        .pipe(
            ofType<BusinessReservationListRefresh>(BusinessReservationActionsTypes.BusinessReservationListRefresh),
            withLatestFrom(this.store.select(selectBusinessReservationStateParams)),
            map(([_, params]: [Action, BusinessReservationParamsInterface]) => {
                if (params && params.page !== 1) {
                    this.store.dispatch(new BusinessReservationParams({
                        ...params,
                        page: 1
                    }));
                }
                return new BusinessReservationListLoad();
            })
        );

    @Effect()
    reservationItemRefresh$ = this.actions$
        .pipe(
            ofType<BusinessReservationItemRefresh>(BusinessReservationActionsTypes.BusinessReservationItemRefresh),
            map((_) => {
                return new BusinessReservationItemLoad();
            })
        );

    /**
     * next page
     */

    @Effect()
    reservationListNextPage$ = this.actions$
        .pipe(
            ofType<BusinessReservationListLoadNextPage>(BusinessReservationActionsTypes.BusinessReservationListLoadNextPage),
            withLatestFrom(this.store.select(selectBusinessReservationStateParams)),
            map(([_, params]: [Action, BusinessReservationParamsInterface]) => {
                this.store.dispatch(new BusinessReservationParams({
                    ...params,
                    page: params.page + 1
                }));
                return new BusinessReservationListLoad();
            })
        );

    constructor(
        private readonly actions$: Actions,
        private readonly reservationApiService: ReservationApiService,
        private readonly store: Store<AppState>,
        private readonly navService: NavService,
        private readonly modalController: ModalController,
        private readonly inAppBrowser: InAppBrowser
    ) {
    }
}
