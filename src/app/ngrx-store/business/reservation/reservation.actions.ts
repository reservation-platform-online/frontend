import {Action} from '@ngrx/store';
import {BusinessReservationParamsInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {BusinessReservationListInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-list.interface';
import {BusinessReservationItemInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-item.interface';

export enum BusinessReservationActionsTypes {

    BusinessReservationParams = '[Business Reservation Local] Reservation params',

    BusinessReservationListLoad = '[Business Reservation API] Get Reservation from API',
    BusinessReservationListLoadSuccess = '[Business Reservation API] Reservation list is loaded success',
    BusinessReservationListLoadNextPage = '[Business Reservation API] Get Reservation list next page from API',
    BusinessReservationListRefresh = '[Business Reservation API] Get Reservation List refresh',

    BusinessReservationItemLoad = '[Business Reservation API] Get Reservation Item',
    BusinessReservationItemLoadSuccess = '[Business Reservation API] Get Reservation Item success',
    BusinessReservationItemRefresh = '[Business Reservation API] Get Reservation Item refresh',

}

export class BusinessReservationParams implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationParams;

    constructor(public payload: BusinessReservationParamsInterface) {
    }
}

export class BusinessReservationListLoad implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationListLoad;

}

export class BusinessReservationListLoadSuccess implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationListLoadSuccess;

    constructor(public payload: BusinessReservationListInterface) {
    }
}

export class BusinessReservationListLoadNextPage implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationListLoadNextPage;

}

export class BusinessReservationListRefresh implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationListRefresh;

}

export class BusinessReservationItemLoad implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationItemLoad;

}

export class BusinessReservationItemLoadSuccess implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationItemLoadSuccess;

    constructor(public payload: BusinessReservationItemInterface) {
    }
}

export class BusinessReservationItemRefresh implements Action {
    readonly type = BusinessReservationActionsTypes.BusinessReservationItemRefresh;

}

export type BusinessReservationActions
    = BusinessReservationParams
    | BusinessReservationListLoad
    | BusinessReservationListLoadSuccess
    | BusinessReservationListLoadNextPage
    | BusinessReservationListRefresh
    | BusinessReservationItemLoad
    | BusinessReservationItemLoadSuccess
    | BusinessReservationItemRefresh;
