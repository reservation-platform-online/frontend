import {BusinessReservationListInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-list.interface';
import {
    BusinessReservationParamsInterface,
    BusinessReservationStatusEnum
} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-params.interface';
import {BusinessReservationItemInterface} from '@app/common/interfaces/modules/client/business/reservation/business-reservation-item.interface';
import {BusinessReservationActions, BusinessReservationActionsTypes} from '@app/ngrx-store/business/reservation/reservation.actions';

export interface State {
    params: BusinessReservationParamsInterface;
    list: BusinessReservationListInterface;
    item: BusinessReservationItemInterface;
}

export const initialState: State = {
    params: {
        page: 1,
        count: 25,
        active: null,
        itemId: null,
        start: null,
        end: null,
        statusList: [
            BusinessReservationStatusEnum.ENABLED,
            BusinessReservationStatusEnum.CONFIRMED,
            BusinessReservationStatusEnum.FINISHED,
            // BusinessReservationStatusEnum.USER_WORKER_DID_NOT_COME,
        ]
    },
    list: null,
    item: null,
};

export function reducer(state: State = initialState, action: BusinessReservationActions) {
    switch (action.type) {


        /**
         * Reservation
         */

        case BusinessReservationActionsTypes.BusinessReservationParams: {

            return {
                ...state,
                params: {
                    ...state.params,
                    ...action.payload
                }
            };
        }

        case BusinessReservationActionsTypes.BusinessReservationListLoadSuccess: {

            return {
                ...state,
                ...(state.list && action.payload && state.params.page !== 1 ? {
                    list: {
                        total: action.payload.total,
                        models: [...state.list.models, ...action.payload.models]
                    }
                } : {
                    list: action.payload
                })
            };
        }

        case BusinessReservationActionsTypes.BusinessReservationItemLoadSuccess: {

            return {
                ...state,
                item: action.payload
            };
        }

        default:
            return state;
    }
}

export const selectBusinessReservationList = (state: State) => state.list;
export const selectBusinessReservationParams = (state: State) => state.params;
export const selectBusinessReservationItem = (state: State) => state.item;
