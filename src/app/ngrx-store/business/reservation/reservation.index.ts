import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromBusinessReservation from '@app/ngrx-store/business/reservation/reservation.reducer';

const selectBusinessReservationState = createFeatureSelector<fromBusinessReservation.State>('businessReservation');


export const selectBusinessReservationStateList = createSelector(
    selectBusinessReservationState,
    fromBusinessReservation.selectBusinessReservationList
);

export const selectBusinessReservationStateItem = createSelector(
    selectBusinessReservationState,
    fromBusinessReservation.selectBusinessReservationItem
);

export const selectBusinessReservationStateParams = createSelector(
    selectBusinessReservationState,
    fromBusinessReservation.selectBusinessReservationParams
);
