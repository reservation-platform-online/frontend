import {BusinessPwsParamsInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-params.interface';
import {BusinessPwsListInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-list.interface';
import {BusinessPwsItemInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item.interface';
import {BusinessPwsActions, BusinessPwsActionsTypes} from '@app/ngrx-store/business/pws/pws.actions';

export interface State {

    params: BusinessPwsParamsInterface;
    list: BusinessPwsListInterface;
    item: BusinessPwsItemInterface;
}

export const initialState: State = {

    params: {
        page: 1,
        count: 25,
        active: null,
        itemId: null
    },
    list: null,
    item: null
};

export function reducer(state: State = initialState, action: BusinessPwsActions) {
    switch (action.type) {


        /**
         * Pws
         */

        case BusinessPwsActionsTypes.BusinessPwsParams: {

            return {
                ...state,
                params: {
                    ...state.params,
                    ...action.payload
                }
            };
        }

        case BusinessPwsActionsTypes.BusinessPwsListLoadSuccess: {

            return {
                ...state,
                ...(state.list && action.payload && state.params.page !== 1 ? {
                    list: {
                        total: action.payload.total,
                        models: [...state.list.models, ...action.payload.models]
                    }
                } : {
                    list: action.payload
                })
            };
        }

        case BusinessPwsActionsTypes.BusinessPwsItemLoadSuccess: {

            return {
                ...state,
                item: action.payload
            };
        }

        default:
            return state;
    }
}

export const selectBusinessPwsParams = (state: State) => state.params;
export const selectBusinessPwsList = (state: State) => state.list;
export const selectBusinessPwsItem = (state: State) => state.item;
