import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map, withLatestFrom} from 'rxjs/operators';
import {Action, Store} from '@ngrx/store';
import {Tools} from '@app/common/tools/tools';
import {of} from 'rxjs';
import {
    BusinessPwsActionsTypes,
    BusinessPwsItemLoad,
    BusinessPwsItemLoadSuccess,
    BusinessPwsItemRefresh,
    BusinessPwsListLoad,
    BusinessPwsListLoadNextPage,
    BusinessPwsListLoadSuccess,
    BusinessPwsListRefresh,
    BusinessPwsParams
} from '@app/ngrx-store/business/pws/pws.actions';
import {AppState} from '@capacitor/core';
import {BusinessPwsParamsInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-params.interface';
import {BusinessPwsListInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-list.interface';
import {BusinessPwsItemInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item.interface';
import {selectBusinessPwsParamsStateParams} from '@app/ngrx-store';
import {PwsApiService} from '@app/common/services/api/modules/client/business/pws/pws-api.service';

@Injectable()
export class BusinessPwsEffects {


    /**
     * Load
     */


    @Effect()
    pwsListLoad$ = this.actions$
        .pipe(
            ofType<BusinessPwsListLoad>(BusinessPwsActionsTypes.BusinessPwsListLoad),
            withLatestFrom(this.store.select(selectBusinessPwsParamsStateParams)),
            exhaustMap(([action, params]: [Action, BusinessPwsParamsInterface]) => {

                    return this.pwsApiService
                        .getList(Tools.clearObject(params))
                        .pipe(
                            map((item: BusinessPwsListInterface) => new BusinessPwsListLoadSuccess(item)),
                            catchError(() => of(new BusinessPwsListLoadSuccess({
                                total: 0,
                                models: []
                            })))
                        );
                }
            )
        );

    @Effect()
    pwsItemLoad$ = this.actions$
        .pipe(
            ofType<BusinessPwsItemLoad>(BusinessPwsActionsTypes.BusinessPwsItemLoad),
            withLatestFrom(this.store.select(selectBusinessPwsParamsStateParams)),
            exhaustMap(([action, params]: [Action, BusinessPwsParamsInterface]) => {

                    return this.pwsApiService
                        .getItem(Tools.clearObject(params))
                        .pipe(
                            map((item: BusinessPwsItemInterface) => new BusinessPwsItemLoadSuccess(item)),
                            catchError(() => of(new BusinessPwsItemLoadSuccess({} as BusinessPwsItemInterface)))
                        );
                }
            )
        );

    /**
     * Refresh
     */

    @Effect()
    pwsListRefresh$ = this.actions$
        .pipe(
            ofType<BusinessPwsListRefresh>(BusinessPwsActionsTypes.BusinessPwsListRefresh),
            withLatestFrom(this.store.select(selectBusinessPwsParamsStateParams)),
            map(([action, params]: [Action, BusinessPwsParamsInterface]) => {
                if (params && params.page !== 1) {
                    this.store.dispatch(new BusinessPwsParams({
                        ...params,
                        page: 1
                    }));
                }
                return new BusinessPwsListLoad();
            })
        );

    @Effect()
    pwsItemRefresh$ = this.actions$
        .pipe(
            ofType<BusinessPwsItemRefresh>(BusinessPwsActionsTypes.BusinessPwsItemRefresh),
            map((action) => {
                return new BusinessPwsItemLoad();
            })
        );

    /**
     * next page
     */


    @Effect()
    pwsListListNextPage$ = this.actions$
        .pipe(
            ofType<BusinessPwsListLoadNextPage>(BusinessPwsActionsTypes.BusinessPwsListLoadNextPage),
            withLatestFrom(this.store.select(selectBusinessPwsParamsStateParams)),
            map(([action, params]: [Action, BusinessPwsParamsInterface]) => {
                this.store.dispatch(new BusinessPwsParams({
                    ...params,
                    page: params.page + 1
                }));
                return new BusinessPwsListLoad();
            })
        );

    constructor(
        private readonly actions$: Actions,
        private readonly pwsApiService: PwsApiService,
        private readonly store: Store<AppState>,
    ) {
    }
}
