import {Action} from '@ngrx/store';
import {BusinessPwsParamsInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-params.interface';
import {BusinessPwsListInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-list.interface';
import {BusinessPwsItemInterface} from '@app/common/interfaces/modules/client/business/pws/business-pws-item.interface';

export enum BusinessPwsActionsTypes {
    BusinessPwsParams = '[Business Pws Local] Pws params',

    BusinessPwsListLoad = '[Business Pws API] Get Pws from API',
    BusinessPwsListLoadSuccess = '[Business Pws API] Pws list is loaded success',
    BusinessPwsListLoadNextPage = '[Business Pws API] Get Pws list next page from API',
    BusinessPwsListRefresh = '[Business Pws API] Get Pws List refresh',

    BusinessPwsItemLoad = '[Business Pws API] Get Pws Item',
    BusinessPwsItemLoadSuccess = '[Business Pws API] Get Pws Item success',
    BusinessPwsItemRefresh = '[Business Pws API] Get Pws Item refresh',

}

export class BusinessPwsParams implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsParams;

    constructor(public payload: BusinessPwsParamsInterface) {
    }
}

export class BusinessPwsListLoad implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsListLoad;

}

export class BusinessPwsListLoadSuccess implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsListLoadSuccess;

    constructor(public payload: BusinessPwsListInterface) {
    }
}

export class BusinessPwsListLoadNextPage implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsListLoadNextPage;

}

export class BusinessPwsListRefresh implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsListRefresh;

}

export class BusinessPwsItemLoad implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsItemLoad;

}

export class BusinessPwsItemLoadSuccess implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsItemLoadSuccess;

    constructor(public payload: BusinessPwsItemInterface) {
    }

}

export class BusinessPwsItemRefresh implements Action {
    readonly type = BusinessPwsActionsTypes.BusinessPwsItemRefresh;

}

export type BusinessPwsActions
    = BusinessPwsParams
    | BusinessPwsListLoad
    | BusinessPwsListLoadSuccess
    | BusinessPwsListLoadNextPage
    | BusinessPwsListRefresh
    | BusinessPwsItemLoad
    | BusinessPwsItemLoadSuccess
    | BusinessPwsItemRefresh;

