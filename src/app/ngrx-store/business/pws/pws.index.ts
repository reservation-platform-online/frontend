import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromBusiness from '@app/ngrx-store/business/pws/pws.reducer';

const selectBusinessPwsState = createFeatureSelector<fromBusiness.State>('businessPws');


export const selectBusinessPwsListStateList = createSelector(
    selectBusinessPwsState,
    fromBusiness.selectBusinessPwsList
);

export const selectBusinessPwsItemStateItem = createSelector(
    selectBusinessPwsState,
    fromBusiness.selectBusinessPwsItem
);

export const selectBusinessPwsParamsStateParams = createSelector(
    selectBusinessPwsState,
    fromBusiness.selectBusinessPwsParams
);
