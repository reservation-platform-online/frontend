import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {
    BusinessActionsTypes,
    BusinessCompanyLoad,
    BusinessCompanyLoadSuccess,
    BusinessCompanyRefresh,
    BusinessWorkerLoad,
    BusinessWorkerLoadSuccess,
    BusinessWorkerRefresh
} from '@app/ngrx-store/business/business.actions';
import {AppState} from '@capacitor/core';
import {BusinessWorkerInterface} from '@app/common/interfaces/modules/client/business/business-worker.interface';
import {BusinessCompanyInterface} from '@app/common/interfaces/modules/client/business/business-company.interface';
import {BusinessApiService} from '@app/common/services/api/modules/client/business/business-api.service';

@Injectable()
export class BusinessEffects {


    /**
     * Load
     */

    @Effect()
    companyLoad$ = this.actions$
        .pipe(
            ofType<BusinessCompanyLoad>(BusinessActionsTypes.BusinessCompanyLoad),
            exhaustMap((action) => {

                    return this.businessApiService
                        .getItemCompany()
                        .pipe(
                            map((item: BusinessCompanyInterface) => new BusinessCompanyLoadSuccess(item)),
                            catchError(() => of(new BusinessCompanyLoadSuccess({} as BusinessCompanyInterface)))
                        );
                }
            )
        );

    @Effect()
    workerLoad$ = this.actions$
        .pipe(
            ofType<BusinessWorkerLoad>(BusinessActionsTypes.BusinessWorkerLoad),
            exhaustMap((action) => {

                    return this.businessApiService
                        .getItemWorker()
                        .pipe(
                            map((item: BusinessWorkerInterface) => new BusinessWorkerLoadSuccess(item)),
                            catchError(() => of(new BusinessWorkerLoadSuccess({} as BusinessWorkerInterface)))
                        );
                }
            )
        );

    /**
     * Refresh
     */

    @Effect()
    companyRefresh$ = this.actions$
        .pipe(
            ofType<BusinessCompanyRefresh>(BusinessActionsTypes.BusinessCompanyRefresh),
            map((action) => {
                return new BusinessCompanyLoad();
            })
        );

    @Effect()
    workerRefresh$ = this.actions$
        .pipe(
            ofType<BusinessWorkerRefresh>(BusinessActionsTypes.BusinessWorkerRefresh),
            map((action) => {
                return new BusinessWorkerLoad();
            })
        );

    constructor(
        private readonly actions$: Actions,
        private readonly businessApiService: BusinessApiService,
        private readonly store: Store<AppState>,
    ) {
    }
}
