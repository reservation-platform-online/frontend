import {Action} from '@ngrx/store';
import {ServiceListInterface} from '@app/common/interfaces/modules/client/service/service-list.interface';

export enum ServiceActionsTypes {
    Load = '[service API] Get service from API',
    LoadSuccess = '[service API] service loaded',
    List = '[service] Select List',
    RefreshList = '[List service refresh]'
}

export class Load implements Action {
    readonly type = ServiceActionsTypes.Load;

    constructor(public params: {
        categoryId: number
    }) {
    }
}

export class LoadSuccess implements Action {
    readonly type = ServiceActionsTypes.LoadSuccess;

    constructor(public payload: ServiceListInterface) {
    }
}

export class RefreshList implements Action {
    readonly type = ServiceActionsTypes.RefreshList;

    constructor(public params: {
        categoryId: number
    }) {
    }
}

export type ServiceActions
    = Load
    | LoadSuccess
    | RefreshList;
