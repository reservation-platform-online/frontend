import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {Load, LoadSuccess, RefreshList, ServiceActionsTypes} from '@app/ngrx-store/service/service.actions';
import {ServiceApiService} from '@app/common/services/api/modules/client/service/service-api.service';
import {ServiceListInterface} from '@app/common/interfaces/modules/client/service/service-list.interface';

@Injectable()
export class ServiceEffects {

    @Effect()
    list$ = this.actions$
        .pipe(
            ofType<Load>(ServiceActionsTypes.Load),
            exhaustMap((action) => {

                    return this.serviceService
                        .getList(action.params)
                        .pipe(
                            map((list: ServiceListInterface) => new LoadSuccess(list)),
                            catchError(error => of(new LoadSuccess({
                                total: 0,
                                models: []
                            })))
                        );
                }
            )
        );

    @Effect()
    refreshList$ = this.actions$
        .pipe(
            ofType<RefreshList>(ServiceActionsTypes.RefreshList),
            exhaustMap((action) =>
                this.serviceService
                    .getList(action.params)
                    .pipe(
                        map((list: ServiceListInterface) => new LoadSuccess(list)),
                        catchError(error => of(new LoadSuccess({
                            total: 0,
                            models: []
                        })))
                    )
            )
        );

    constructor(private readonly actions$: Actions,
                private readonly serviceService: ServiceApiService
    ) {
    }
}
