import {ServiceActions, ServiceActionsTypes} from './service.actions';
import {ServiceListInterface} from '@app/common/interfaces/modules/client/service/service-list.interface';

export interface State {
    list: ServiceListInterface;
}

export const initialState: State = {
    list: null
};

export function reducer(state: State = initialState, action: ServiceActions) {
    switch (action.type) {

        case ServiceActionsTypes.LoadSuccess: {
            return {...state, list: action.payload};
        }

        default:
            return state;
    }
}

export const selectList = (state: State) => state.list;
