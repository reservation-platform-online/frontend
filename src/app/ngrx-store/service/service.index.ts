import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromService from '@app/ngrx-store/service/service.reducer';

export const selectServiceState = createFeatureSelector<fromService.State>('service');
export const selectServiceList = createSelector(
    selectServiceState,
    fromService.selectList
);
