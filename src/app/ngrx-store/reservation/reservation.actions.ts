import {Action} from '@ngrx/store';
import {ReservationListInterface} from '@app/common/interfaces/modules/client/reservation/reservation-list.interface';
import {CreateInterface} from '@app/common/interfaces/modules/client/reservation/create.interface';
import {InitCreateInterface} from '@app/common/interfaces/modules/client/reservation/init-create.interface';
import {ReservationParamsInterface} from '@app/common/interfaces/modules/client/reservation/reservation-params.interface';

export enum ReservationActionsTypes {
    Params = '[Reservation Api] Reservation params',
    ParamsHistory = '[Reservation Api] Reservation params history',
    Cancel = '[Reservation Api] Reservation Cancel',
    InitCreate = '[Reservation] Reservation init',
    Create = '[Reservation API] Post Reservation from API',
    Load = '[Reservation API] Get Reservation from API',
    LoadNextPage = '[Reservation API] Get Reservation next page from API',
    LoadSuccess = '[Reservation API] Reservation loaded',
    RefreshList = '[List refresh]',
    LoadHistory = '[Reservation API] Get Reservation from API history',
    LoadHistoryNextPage = '[Reservation API] Get Reservation from API history next page',
    LoadHistorySuccess = '[Reservation API] Reservation loaded history',
    RefreshListHistory = '[List refresh history]',
    OpenComment = '[Reservation API] Open Comment'
}

export class OpenComment implements Action {
    readonly type = ReservationActionsTypes.OpenComment;

    constructor(public payload: {
        pwsId: string,
        reservationUserId: string
    }) {
    }
}

export class Params implements Action {
    readonly type = ReservationActionsTypes.Params;

    constructor(public payload: ReservationParamsInterface) {
    }
}

export class ParamsHistory implements Action {
    readonly type = ReservationActionsTypes.ParamsHistory;

    constructor(public payload: ReservationParamsInterface) {
    }
}

export class InitCreate implements Action {
    readonly type = ReservationActionsTypes.InitCreate;

    constructor(public payload: InitCreateInterface) {
    }
}

export class Create implements Action {
    readonly type = ReservationActionsTypes.Create;

    constructor(public payload: CreateInterface) {
    }
}

export class Cancel implements Action {
    readonly type = ReservationActionsTypes.Cancel;

    constructor(public payload: {
        id: string
    }) {
    }
}

export class Load implements Action {
    readonly type = ReservationActionsTypes.Load;

}

export class LoadNextPage implements Action {
    readonly type = ReservationActionsTypes.LoadNextPage;

}

export class LoadSuccess implements Action {
    readonly type = ReservationActionsTypes.LoadSuccess;

    constructor(public payload: ReservationListInterface) {
    }
}

export class RefreshList implements Action {
    readonly type = ReservationActionsTypes.RefreshList;
}

export class LoadHistory implements Action {
    readonly type = ReservationActionsTypes.LoadHistory;

}

export class LoadHistoryNextPage implements Action {
    readonly type = ReservationActionsTypes.LoadHistoryNextPage;

}

export class LoadHistorySuccess implements Action {
    readonly type = ReservationActionsTypes.LoadHistorySuccess;

    constructor(public payload: ReservationListInterface) {
    }
}

export class RefreshListHistory implements Action {
    readonly type = ReservationActionsTypes.RefreshListHistory;
}

export type ReservationActions
    = Load
    | LoadSuccess
    | Params
    | ParamsHistory
    | InitCreate
    | Create
    | RefreshList
    | LoadHistory
    | LoadHistorySuccess
    | RefreshListHistory;
