import {ReservationActions, ReservationActionsTypes} from './reservation.actions';
import {ReservationListInterface} from '@app/common/interfaces/modules/client/reservation/reservation-list.interface';
import {InitCreateInterface} from '@app/common/interfaces/modules/client/reservation/init-create.interface';
import {ReservationParamsInterface} from '@app/common/interfaces/modules/client/reservation/reservation-params.interface';

export interface State {
    params: ReservationParamsInterface;
    paramsHistory: ReservationParamsInterface;
    list: ReservationListInterface;
    listHistory: ReservationListInterface;
    initCreate: InitCreateInterface;
}

export const initialState: State = {
    list: null,
    listHistory: null,
    params: {

        count: 32,
        page: 1,
        active: 1

    },
    paramsHistory: {

        count: 32,
        page: 1,
        active: 0

    },
    initCreate: null,
};

export function reducer(state: State = initialState, action: ReservationActions) {
    switch (action.type) {

        case ReservationActionsTypes.LoadSuccess: {
            return {
                ...state,
                ...(state.list && action.payload && state.params.page !== 1 ? {
                    list: {
                        total: action.payload.total,
                        models: [...state.list.models, ...action.payload.models]
                    }
                } : {
                    list: action.payload
                })
            };
        }

        case ReservationActionsTypes.LoadHistorySuccess: {
            return {
                ...state,
                ...(state.listHistory && action.payload && state.paramsHistory.page !== 1 ? {
                    listHistory: {
                        total: action.payload.total,
                        models: [...state.listHistory.models, ...action.payload.models]
                    }
                } : {
                    listHistory: action.payload
                })
            };
        }

        case ReservationActionsTypes.InitCreate: {
            return {...state, initCreate: action.payload};
        }

        case ReservationActionsTypes.Params: {

            return {
                ...state,
                params: {
                    ...state.params,
                    ...action.payload
                }
            };
        }

        case ReservationActionsTypes.ParamsHistory: {

            return {
                ...state,
                paramsHistory: {
                    ...state.paramsHistory,
                    ...action.payload
                }
            };
        }

        default:
            return state;
    }
}

export const selectParamsHistory = (state: State) => state.paramsHistory;
export const selectParams = (state: State) => state.params;
export const selectList = (state: State) => state.list;
export const selectListHistory = (state: State) => state.listHistory;
export const selectInitCreate = (state: State) => state.initCreate;
