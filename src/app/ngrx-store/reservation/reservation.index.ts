import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromReservation from '@app/ngrx-store/reservation/reservation.reducer';

export const selectReservationState = createFeatureSelector<fromReservation.State>('reservation');
export const selectReservationList = createSelector(
    selectReservationState,
    fromReservation.selectList
);
export const selectReservationListHistory = createSelector(
    selectReservationState,
    fromReservation.selectListHistory
);
export const selectReservationParamsHistory = createSelector(
    selectReservationState,
    fromReservation.selectParamsHistory
);
export const selectReservationParams = createSelector(
    selectReservationState,
    fromReservation.selectParams
);
export const selectReservationCreateInit = createSelector(
    selectReservationState,
    fromReservation.selectInitCreate
);
