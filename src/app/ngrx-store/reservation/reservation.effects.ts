import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map, tap, withLatestFrom} from 'rxjs/operators';
import {
    Cancel,
    Create,
    Load,
    LoadHistory,
    LoadHistoryNextPage,
    LoadHistorySuccess,
    LoadNextPage,
    LoadSuccess,
    OpenComment,
    Params,
    ParamsHistory,
    RefreshList,
    RefreshListHistory,
    ReservationActionsTypes,
} from '@app/ngrx-store/reservation/reservation.actions';
import {ReservationApiService} from '@app/common/services/api/modules/client/reservation/reservation-api.service';
import {ReservationListInterface} from '@app/common/interfaces/modules/client/reservation/reservation-list.interface';
import {selectProfileData, selectReservationParams, selectReservationParamsHistory} from '@app/ngrx-store';
import {Action, Store} from '@ngrx/store';
import {ReservationParamsInterface} from '@app/common/interfaces/modules/client/reservation/reservation-params.interface';
import {Tools} from '@app/common/tools/tools';
import {ModalController} from '@ionic/angular';
import {CommentModalComponent} from '@app/common/modal/client/comment/comment-modal.component';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';

@Injectable()
export class ReservationEffects {

    /**
     * CRUD
     */

    @Effect()
    cancel$ = this.actions$
        .pipe(
            ofType<Cancel>(ReservationActionsTypes.Cancel),
            exhaustMap((action) => {

                    return this.reservationService
                        .deleteCancel(action.payload.id)
                        .pipe(
                            map(() => new RefreshList()),
                            catchError(() => of(new RefreshList()))
                        );
                }
            )
        );

    @Effect()
    create$ = this.actions$
        .pipe(
            ofType<Create>(ReservationActionsTypes.Create),
            exhaustMap((action) =>
                this.reservationService
                    .postItem(action.payload)
                    .pipe(
                        map(() => new RefreshList()),
                        catchError(() => of(new RefreshList()))
                    )
            )
        );

    /**
     * Load
     */

    @Effect()
    list$ = this.actions$
        .pipe(
            ofType<Load>(ReservationActionsTypes.Load),
            withLatestFrom(this.store.select(selectReservationParams)),
            exhaustMap(([_, params]: [Action, ReservationParamsInterface]) => {

                return this.reservationService
                    .getList(Tools.clearObject(params))
                    .pipe(
                        map((list: ReservationListInterface) => new LoadSuccess(list)),
                        catchError(() => of(new LoadSuccess({
                            models: [],
                            total: 0
                        })))
                    );
                }
            )
        );


    @Effect()
    listHistory$ = this.actions$
        .pipe(
            ofType<LoadHistory>(ReservationActionsTypes.LoadHistory),
            withLatestFrom(this.store.select(selectReservationParamsHistory)),
            exhaustMap(([_, params]: [Action, ReservationParamsInterface]) => {

                return this.reservationService
                    .getList(Tools.clearObject(params))
                    .pipe(
                        map((list: ReservationListInterface) => new LoadHistorySuccess(list)),
                        catchError(() => of(new LoadHistorySuccess({
                            models: [],
                            total: 0
                        })))
                    );
                }
            )
        );

    /**
     * Refresh
     */

    @Effect()
    refreshList$ = this.actions$
        .pipe(
            ofType<RefreshList>(ReservationActionsTypes.RefreshList),
            withLatestFrom(this.store.select(selectReservationParamsHistory)),
            map(([_, params]: [Action, ReservationParamsInterface]) => {
                if (params && params.page !== 1) {
                    this.store.dispatch(new Params({
                        ...params,
                        page: 1,
                        active: 1
                    }));
                }
                return new Load();
            })
        );

    @Effect()
    refreshListHistory$ = this.actions$
        .pipe(
            ofType<RefreshListHistory>(ReservationActionsTypes.RefreshListHistory),
            withLatestFrom(this.store.select(selectReservationParamsHistory)),
            map(([_, params]: [Action, ReservationParamsInterface]) => {
                if (params && params.page !== 1) {

                    this.store.dispatch(new ParamsHistory({
                        ...params,
                        page: 1,
                        active: 0
                    }));

                }
                return new LoadHistory();
            })
        );

    /**
     * Next page
     */

    @Effect()
    listNextPage$ = this.actions$
        .pipe(
            ofType<LoadNextPage>(ReservationActionsTypes.LoadNextPage),
            withLatestFrom(this.store.select(selectReservationParams)),
            map(([_, params]: [Action, ReservationParamsInterface]) => {
                this.store.dispatch(new Params({
                    ...params,
                    page: params.page + 1,
                    active: 1
                }));
                return new Load();
            })
        );

    @Effect()
    listHistoryNextPage$ = this.actions$
        .pipe(
            ofType<LoadHistoryNextPage>(ReservationActionsTypes.LoadHistoryNextPage),
            withLatestFrom(this.store.select(selectReservationParamsHistory)),
            map(([_, params]: [Action, ReservationParamsInterface]) => {
                this.store.dispatch(new ParamsHistory({
                    ...params,
                    page: params.page + 1,
                    active: 0
                }));
                return new LoadHistory();
            })
        );

    /**
     * Open
     */

    @Effect({
        dispatch: false
    })
    openComment$ = this.actions$
        .pipe(
            ofType<OpenComment>(ReservationActionsTypes.OpenComment),
            withLatestFrom(this.store.select(selectProfileData)),
            exhaustMap(([action, profile]: [OpenComment, ProfileInterface]) => {
                if (action?.payload?.pwsId && action?.payload?.reservationUserId) {
                    return this.reservationService.comment$(action.payload).pipe(tap((comment) => {
                        comment = Tools.copyObject(comment);
                        comment['user'] = profile;
                        this.modalController.create({
                            component: CommentModalComponent,
                            componentProps: {
                                comment
                            },
                            cssClass: 'ion-page-without-padding-top'
                        }).then((modal) => {
                            modal.present();

                            modal.onWillDismiss().then(() => {
                                this.store.dispatch(new OpenComment({
                                    reservationUserId: null,
                                    pwsId: null
                                }));
                            });
                        });
                    }));
                }
                return of();
            })
        );

    constructor(
        private readonly actions$: Actions,
        private readonly store: Store,
        private readonly modalController: ModalController,
        private readonly reservationService: ReservationApiService
    ) {
    }
}
