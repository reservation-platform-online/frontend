import {Action} from '@ngrx/store';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';

export enum ProfileActionsTypes {
    Init = '[Profile] Init Profile',
    Load = '[Profile API] Get Profile from API',
    LoadSuccess = '[Profile API] Profile loaded',
    ConnectExternal = '[Login Page] ConnectExternal',
    Data = '[Profile] Select Profile Data',
    ChangeCity = '[Profile City Changed]',
    ChangeData = '[Profile Data Changed]',
    RefreshData = '[Profile Data refresh]'
}

export class ConnectExternal implements Action {
    readonly type = ProfileActionsTypes.ConnectExternal;

    constructor(public payload: {
        code: string,
        platform?: 'zoom'
    }) {

    }
}

export class Init implements Action {
    readonly type = ProfileActionsTypes.Init;
}

export class Load implements Action {
    readonly type = ProfileActionsTypes.Load;
}

export class ChangeData implements Action {
    readonly type = ProfileActionsTypes.ChangeData;

    constructor(public payload: ProfileInterface) {
    }
}

export class ChangeCity implements Action {
    readonly type = ProfileActionsTypes.ChangeCity;

    constructor(public payload: { id: number }) {
    }
}

export class LoadSuccess implements Action {
    readonly type = ProfileActionsTypes.LoadSuccess;

    constructor(public payload: ProfileInterface) {
    }
}

export class Data implements Action {
    readonly type = ProfileActionsTypes.Data;

    constructor(public payload: ProfileInterface) {
    }
}

export class RefreshData implements Action {
    readonly type = ProfileActionsTypes.RefreshData;
}

export type ProfileActions
    = Load
    | LoadSuccess
    | Data
    | Init
    | RefreshData
    | ChangeData;
