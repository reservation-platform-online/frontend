import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map, switchMap, tap} from 'rxjs/operators';
import {
    ChangeCity,
    ConnectExternal,
    Data,
    Load,
    LoadSuccess,
    ProfileActionsTypes,
    RefreshData
} from '@app/ngrx-store/profile/profile.actions';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';
import {ProfileApiService} from '@app/common/services/api/modules/client/profile/profile-api.service';

@Injectable()
export class ProfileEffects {

    @Effect()
    connectExternal$ = this.actions$
        .pipe(
            ofType<ConnectExternal>(ProfileActionsTypes.ConnectExternal),
            map(action => action.payload),
            exhaustMap(payload =>
                this.profileApiService
                    .connectExternal(payload)
                    .pipe(
                        map(() => new Load()),
                        catchError(() => of(new Load()))
                    )
            )
        );

    @Effect()
    Data$ = this.actions$
        .pipe(
            ofType<Load>(ProfileActionsTypes.Load),
            exhaustMap(() => {

                    return this.profileApiService
                        .getProfile()
                        .pipe(
                            map((profile: ProfileInterface) => new LoadSuccess(profile)),
                            catchError(error => of(new Data(error)))
                        );
                }
            )
        );

    @Effect()
    profileLoaded$ = this.actions$
        .pipe(
            ofType<LoadSuccess>(ProfileActionsTypes.LoadSuccess),
            map((profile: LoadSuccess) => new Data(profile.payload)),
            tap(() => {
            })
        );

    @Effect()
    cityChanged$ = this.actions$
        .pipe(
            ofType<ChangeCity>(ProfileActionsTypes.ChangeCity),
            switchMap(() => {

                return this.profileApiService
                    .getProfile()
                    .pipe(
                        map((profile: ProfileInterface) => new Data(profile)),
                        tap(() => {
                        }),
                        catchError(error => of(new Data(error)))
                    );
            })
        );

    @Effect()
    refreshProfile$ = this.actions$
        .pipe(
            ofType<RefreshData>(ProfileActionsTypes.RefreshData),
            exhaustMap(() =>
                this.profileApiService
                    .getProfile()
                    .pipe(
                        map((profile: ProfileInterface) => new Data(profile)),
                        catchError(error => of(new Data(error)))
                    )
            )
        );

    constructor(private readonly actions$: Actions,
                private readonly profileApiService: ProfileApiService
    ) {
    }
}
