import {ProfileActions, ProfileActionsTypes} from './profile.actions';
import {ProfileInterface} from '@app/common/interfaces/modules/client/profile/profile.interface';

export interface State {
    data: ProfileInterface;
}

export const initialState: State = {
    data: null
};

export function reducer(state: State = initialState, action: ProfileActions) {
    switch (action.type) {

        case ProfileActionsTypes.Data: {
            return {...state, data: action.payload};
        }

        case ProfileActionsTypes.ChangeData: {
            return {...state, data: action.payload};
        }

        case ProfileActionsTypes.Init: {
            return {...state, data: null};
        }

        default:
            return state;
    }
}

export const selectData = (state: State) => state.data;
