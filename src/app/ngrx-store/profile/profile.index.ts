import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromProfile from '@app/ngrx-store/profile/profile.reducer';

export const selectProfileState = createFeatureSelector<fromProfile.State>('profile');
export const selectProfileData = createSelector(
    selectProfileState,
    fromProfile.selectData
);
