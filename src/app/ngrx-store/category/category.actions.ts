import {Action} from '@ngrx/store';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {CategoryParamsInterface} from '@app/common/interfaces/modules/client/category/category-params.interface';

export enum CategoryActionsTypes {
    Params = '[Category Params] Params Category',
    Load = '[Category API] Get Category from API',
    LoadSuccess = '[Category API] Category loaded',
    RefreshList = '[List refresh]'
}

export class Params implements Action {
    readonly type = CategoryActionsTypes.Params;

    constructor(public payload: CategoryParamsInterface) {
    }
}

export class Load implements Action {
    readonly type = CategoryActionsTypes.Load;
}

export class LoadSuccess implements Action {
    readonly type = CategoryActionsTypes.LoadSuccess;

    constructor(public payload: CategoryListInterface) {
    }
}

export class RefreshList implements Action {
    readonly type = CategoryActionsTypes.RefreshList;
}

export type CategoryActions
    = Load
    | Params
    | LoadSuccess
    | RefreshList;
