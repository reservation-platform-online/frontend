import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map, withLatestFrom} from 'rxjs/operators';
import {CategoryActionsTypes, Load, LoadSuccess, RefreshList} from '@app/ngrx-store/category/category.actions';
import {CategoryApiService} from '@app/common/services/api/modules/client/category/category-api.service';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {selectCategoryParams} from '@app/ngrx-store';
import {Action, Store} from '@ngrx/store';
import {CategoryParamsInterface} from '@app/common/interfaces/modules/client/category/category-params.interface';

@Injectable()
export class CategoryEffects {

    @Effect()
    list$ = this.actions$
        .pipe(
            ofType<Load>(CategoryActionsTypes.Load),
            withLatestFrom(this.store.select(selectCategoryParams)),
            exhaustMap(([action, params]: [Action, CategoryParamsInterface]) => {

                return this.categoryService
                    .getList(params)
                    .pipe(
                        map((list: CategoryListInterface) => new LoadSuccess(list)),
                        catchError(() => of(new LoadSuccess({
                            total: 0,
                            models: []
                        })))
                    );
                }
            )
        );

    @Effect()
    refreshList$ = this.actions$
        .pipe(
            ofType<RefreshList>(CategoryActionsTypes.RefreshList),
            map(() => new Load()),
        );

    constructor(
        private readonly actions$: Actions,
        private readonly store: Store,
        private readonly categoryService: CategoryApiService
    ) {
    }
}
