import {CategoryActions, CategoryActionsTypes} from './category.actions';
import {CategoryListInterface} from '@app/common/interfaces/modules/client/category/category-list.interface';
import {CategoryParamsInterface} from '@app/common/interfaces/modules/client/category/category-params.interface';
import {CategoryTreeInterface} from '@app/common/interfaces/modules/client/category/category-tree.interface';
import {CategoryTreeListInterface} from '@app/common/interfaces/modules/client/category/category-tree-list.interface';

export interface State {
    treeList: CategoryTreeListInterface;
    list: CategoryListInterface;
    params: CategoryParamsInterface;
}

export const initialState: State = {
    treeList: null,
    list: null,
    params: null,
};

export function reducer(state: State = initialState, action: CategoryActions) {
    switch (action.type) {

        case CategoryActionsTypes.LoadSuccess: {
            return {
                ...state,
                treeList: buildTree(action.payload),
                list: action.payload
            };
        }

        case CategoryActionsTypes.Params: {
            return {
                ...state,
                params: {
                    ...state.params,
                    ...action.payload
                }
            };
        }

        default:
            return state;
    }
}

export const selectTreeList = (state: State) => state.treeList;
export const selectList = (state: State) => state.list;
export const selectParams = (state: State) => state.params;

function buildTree(list: CategoryListInterface): CategoryTreeListInterface {

    const getChildren = (parentId: number): CategoryTreeInterface[] => {
        if (parentId !== undefined) {
            return list.models.filter((item) => item.parentId === parentId).map((item) => {

                const children: CategoryTreeInterface[] = getChildren(item.id);

                const newItem: CategoryTreeInterface = {
                    ...item,
                    numberOfChildren: children.length,
                    children
                };

                return newItem;

            });
        }
        return [];
    };

    const newList = getChildren(null);

    return {
        models: newList,
        total: newList.length
    };

}
