import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromCategory from '@app/ngrx-store/category/category.reducer';

export const selectCategoryState = createFeatureSelector<fromCategory.State>('category');
export const selectCategoryTreeList = createSelector(
    selectCategoryState,
    fromCategory.selectTreeList
);
export const selectCategoryList = createSelector(
    selectCategoryState,
    fromCategory.selectList
);
export const selectCategoryParams = createSelector(
    selectCategoryState,
    fromCategory.selectParams
);
