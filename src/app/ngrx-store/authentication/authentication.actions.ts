import {Action} from '@ngrx/store';
import {AuthenticateInterface} from '@app/common/interfaces/modules/client/authentication/authenticate.interface';

export enum AuthenticationActionTypes {
    Login = '[Login Page] Login',
    LoginExternal = '[Login Page] LoginExternal',
    LoginSuccess = '[Auth API] Login Success',
    LoginFailure = '[Auth API] Login Failure',
    Logout = '[Auth] Logout',
    LogoutComplete = '[Auth API] Logout Complete'
}

export class LoginExternal implements Action {
    readonly type = AuthenticationActionTypes.LoginExternal;

    constructor(public payload: {
        code: string,
        platform?: 'zoom'
    }) {

    }
}

export class Login implements Action {
    readonly type = AuthenticationActionTypes.Login;

    constructor(public payload: AuthenticateInterface) {

    }
}

export class LoginSuccess implements Action {
    readonly type = AuthenticationActionTypes.LoginSuccess;

    constructor(public payload: { token: string }) {

    }
}

export class LoginFailure implements Action {
    readonly type = AuthenticationActionTypes.LoginFailure;

    constructor(public payload: any) {
    }
}

export class Logout implements Action {
    readonly type = AuthenticationActionTypes.Logout;
}

export class LogoutComplete implements Action {
    readonly type = AuthenticationActionTypes.LogoutComplete;
}

export type AuthenticationActions
    = Login
    | LoginExternal
    | LoginSuccess
    | LoginFailure
    | Logout
    | LogoutComplete;
