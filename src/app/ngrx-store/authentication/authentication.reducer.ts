import {AuthenticationActions, AuthenticationActionTypes} from './authentication.actions';

export interface State {
    token: string;
}

export const initialState: State = {
    token: null
};

export function reducer(state: State = initialState, action: AuthenticationActions) {
    switch (action.type) {

        case AuthenticationActionTypes.LoginSuccess:
            return {...state, token: action.payload.token};

        case AuthenticationActionTypes.LogoutComplete:
            return initialState;

        default:
            return state;
    }
}

export const selectToken = (state: State) => state.token;
