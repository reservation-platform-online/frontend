import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromAuth from '@app/ngrx-store/authentication/authentication.reducer';

export const selectAuthState = createFeatureSelector<fromAuth.State>('auth');
export const selectAuthToken = createSelector(
    selectAuthState,
    fromAuth.selectToken
);
export const selectIsLoggedIn = createSelector(selectAuthToken, token => !!token);
