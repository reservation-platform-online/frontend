import {Injectable} from '@angular/core';
import {
    AuthenticationActionTypes,
    Login, LoginExternal,
    LoginFailure,
    LoginSuccess,
    Logout,
    LogoutComplete
} from './authentication.actions';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {AuthenticationApiService} from '@app/common/services/api/modules/client/authentication/authentication-api.service';
import {ProfileApiService} from '@app/common/services/api/modules/client/profile/profile-api.service';

@Injectable()
export class AuthenticationEffects {

    @Effect()
    loginExternal$ = this.actions$
        .pipe(
            ofType<LoginExternal>(AuthenticationActionTypes.LoginExternal),
            map(action => action.payload),
            exhaustMap(payload =>
                this.authenticationApiService
                    .loginExternal(payload)
                    .pipe(
                        map(token => new LoginSuccess({token: token})),
                        catchError(error => of(new LoginFailure(error)))
                    )
            )
        );

    @Effect()
    login$ = this.actions$
        .pipe(
            ofType<Login>(AuthenticationActionTypes.Login),
            map(action => action.payload),
            exhaustMap(authentication =>
                this.authenticationApiService
                    .login(authentication)
                    .pipe(
                        map(token => new LoginSuccess({token: token})),
                        catchError(error => of(new LoginFailure(error)))
                    )
            )
        );

    @Effect()
    logout$ = this.actions$
        .pipe(
            ofType<Logout>(AuthenticationActionTypes.Logout),
            exhaustMap(() =>
                this.authenticationApiService.logout()
                    .pipe(
                        map(() => new LogoutComplete()),
                        catchError(() => of(new LogoutComplete()))
                    ))
        );


    constructor(
        private readonly actions$: Actions,
        private readonly profileApiService: ProfileApiService,
        private readonly authenticationApiService: AuthenticationApiService
    ) {
    }
}
