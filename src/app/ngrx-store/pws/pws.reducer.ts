import {PwsActions, SearchActionsTypes} from './pws.actions';
import {SearchItemListInterface} from '@app/common/interfaces/modules/client/pws/search-item-list.interface';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {SearchItemCommentListInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment-list.interface';
import {SearchCommentsInterface} from '@app/common/interfaces/modules/client/pws/search-comments.interface';

export interface State {
    list: SearchItemListInterface;
    item: SearchItemInterface;
    calendar: SearchItemInterface;
    comments: SearchItemCommentListInterface;
    params: SearchInterface;
    paramsCalendar: SearchInterface;
    paramsComments: SearchCommentsInterface;
}

export const initialState: State = {
    list: undefined,
    item: undefined,
    calendar: undefined,
    comments: undefined,
    params: {
        category: null
    },
    paramsCalendar: null,
    paramsComments: null
};

export function reducer(state: State = initialState, action: PwsActions) {
    switch (action.type) {

        case SearchActionsTypes.LoadItemSuccess: {
            return {
                ...state,
                item: action.payload
            };
        }

        case SearchActionsTypes.LoadListSuccess: {
            return {
                ...state,
                ...(state.list && action.payload && state.params.page !== 1 ? {
                    list: {
                        total: action.payload.total,
                        models: [...state.list.models, ...action.payload.models]
                    }
                } : {
                    list: action.payload
                })
            };
        }

        case SearchActionsTypes.LoadCalendarSuccess: {
            return {
                ...state,
                calendar: action.payload
            };
        }

        case SearchActionsTypes.LoadCommentsSuccess: {
            return {
                ...state,
                ...(state.comments && action.payload && state.paramsComments.page !== 1 ? {
                    comments: {
                        total: action?.payload?.total,
                        models: [...state?.comments?.models, ...action?.payload?.models]
                    }
                } : {
                    comments: action.payload
                })
            };
        }

        case SearchActionsTypes.Params: {

            return {
                ...state,
                params: {
                    ...state.params,
                    ...action.params
                },
                ...(action.params.category && state.params.category ? (
                    (action.params.category.id !== state.params.category.id ||
                    action.params.page === 1) ? {
                        list: {
                            models: [],
                            total: 0
                        }
                    } : {}
                ) : {})
            };
        }

        case SearchActionsTypes.ParamsCalendar: {

            return {
                ...state,
                paramsCalendar: {
                    ...state.paramsCalendar,
                    ...action.paramsCalendar
                }
            };
        }

        case SearchActionsTypes.ParamsComments: {

            return {
                ...state,
                paramsComments: {
                    ...state.paramsComments,
                    ...action.paramsComments
                }
            };
        }

        default:
            return state;
    }
}

export const selectList = (state: State) => state.list;
export const selectItem = (state: State) => state.item;
export const selectCalendar = (state: State) => state.calendar;
export const selectComments = (state: State) => state.comments;
export const selectParams = (state: State) => state.params;
export const selectParamsCalendar = (state: State) => state.paramsCalendar;
export const selectParamsComments = (state: State) => state.paramsComments;
