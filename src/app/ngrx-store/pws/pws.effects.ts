import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map, withLatestFrom} from 'rxjs/operators';
import {PwsService} from '@app/common/services/api/modules/client/pws/pws.service';
import {
    LoadCalendar,
    LoadCalendarSuccess,
    LoadComments,
    LoadCommentsSuccess,
    LoadItem,
    LoadItemSuccess,
    LoadList,
    LoadListSuccess,
    NextCommentsPage,
    NextListPage,
    Params,
    ParamsComments,
    RefreshCalendar,
    RefreshComments,
    RefreshItem,
    RefreshList,
    SearchActionsTypes
} from '@app/ngrx-store/pws/pws.actions';
import {SearchItemListInterface} from '@app/common/interfaces/modules/client/pws/search-item-list.interface';
import {Tools} from '@app/common/tools/tools';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {Action, Store} from '@ngrx/store';
import {AppState} from '@capacitor/core';
import {selectPwsParams, selectPwsParamsCalendar, selectPwsParamsComments} from '@app/ngrx-store/pws/pws.index';
import {SearchItemCommentListInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment-list.interface';
import {SearchCommentsInterface} from '@app/common/interfaces/modules/client/pws/search-comments.interface';

@Injectable()
export class PwsEffects {

    /**
     * Load
     */

    @Effect()
    list$ = this.actions$
        .pipe(
            ofType<LoadList>(SearchActionsTypes.LoadList),
            withLatestFrom(this.store.select(selectPwsParams)),
            exhaustMap(([action, params]: [Action, SearchInterface]) => {

                    return this.pwsService
                        .getList(Tools.clearObject(params))
                        .pipe(
                            map((list: SearchItemListInterface) => new LoadListSuccess(list)),
                            catchError(() => of(new LoadListSuccess({
                                models: [],
                                total: 0
                            })))
                        );
                }
            )
        );

    @Effect()
    item$ = this.actions$
        .pipe(
            ofType<LoadItem>(SearchActionsTypes.LoadItem),
            exhaustMap((action) => {

                    return this.pwsService
                        .getItem(Tools.clearObject(action.params))
                        .pipe(
                            map((item: SearchItemInterface) => new LoadItemSuccess(item)),
                            catchError(() => of(new LoadItemSuccess({} as SearchItemInterface)))
                        );
                }
            )
        );

    @Effect()
    calendar$ = this.actions$
        .pipe(
            ofType<LoadCalendar>(SearchActionsTypes.LoadCalendar),
            exhaustMap((action) => {

                    return this.pwsService
                        .getCalendar(Tools.clearObject(action.paramsCalendar))
                        .pipe(
                            map((item: SearchItemInterface) => new LoadCalendarSuccess(item)),
                            catchError(() => of(new LoadCalendarSuccess({} as SearchItemInterface)))
                        );
                }
            )
        );

    @Effect()
    comments$ = this.actions$
        .pipe(
            ofType<LoadComments>(SearchActionsTypes.LoadComments),
            withLatestFrom(this.store.select(selectPwsParamsComments)),
            exhaustMap(([action, params]: [Action, SearchCommentsInterface]) => {

                    return this.pwsService
                        .getComments(Tools.clearObject(params))
                        .pipe(
                            map((item: SearchItemCommentListInterface) => new LoadCommentsSuccess(item)),
                            catchError(() => of(new LoadCommentsSuccess({} as SearchItemCommentListInterface)))
                        );
                }
            )
        );

    /**
     * Refresh
     */

    @Effect()
    refreshList$ = this.actions$
        .pipe(
            ofType<RefreshList>(SearchActionsTypes.RefreshList),
            withLatestFrom(this.store.select(selectPwsParams)),
            map(([action, params]: [Action, SearchInterface]) => {
                if (params && params.page !== 1) {
                    this.store.dispatch(new Params({
                        ...params,
                        page: 1
                    }));
                }
                return new LoadList();
            })
        );

    @Effect()
    refreshCalendar$ = this.actions$
        .pipe(
            ofType<RefreshCalendar>(SearchActionsTypes.RefreshCalendar),
            withLatestFrom(this.store.select(selectPwsParamsCalendar)),
            map(([action, params]: [Action, SearchInterface]) => {
                return new LoadCalendar(params);
            })
        );

    @Effect()
    refreshComments$ = this.actions$
        .pipe(
            ofType<RefreshComments>(SearchActionsTypes.RefreshComments),
            withLatestFrom(this.store.select(selectPwsParamsComments)),
            map(([action, params]: [Action, SearchCommentsInterface]) => {
                if (params && params.page !== 1) {
                    this.store.dispatch(new ParamsComments({
                        ...params,
                        page: 1
                    }));
                }
                return new LoadComments();
            })
        );

    @Effect()
    refreshItem$ = this.actions$
        .pipe(
            ofType<RefreshItem>(SearchActionsTypes.RefreshItem),
            withLatestFrom(this.store.select(selectPwsParams)),
            map(([action, params]: [Action, SearchInterface]) => {
                return new LoadItem(params);
            })
        );

    /**
     * next page
     */

    @Effect()
    nextListPage$ = this.actions$
        .pipe(
            ofType<NextListPage>(SearchActionsTypes.NextListPage),
            withLatestFrom(this.store.select(selectPwsParams)),
            map(([action, params]: [Action, SearchInterface]) => {
                this.store.dispatch(new Params({
                    ...params,
                    page: params.page + 1
                }));
                return new LoadList();
            })
        );

    @Effect()
    nextCommentsPage$ = this.actions$
        .pipe(
            ofType<NextCommentsPage>(SearchActionsTypes.NextCommentsPage),
            withLatestFrom(this.store.select(selectPwsParamsComments)),
            map(([action, params]: [Action, SearchCommentsInterface]) => {
                this.store.dispatch(new ParamsComments({
                    ...params,
                    page: params.page + 1
                }));
                return new LoadComments();
            })
        );

    constructor(
        private readonly actions$: Actions,
        private readonly pwsService: PwsService,
        private readonly store: Store<AppState>,
    ) {
    }
}
