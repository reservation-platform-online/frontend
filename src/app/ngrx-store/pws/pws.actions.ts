import {Action} from '@ngrx/store';
import {SearchInterface} from '@app/common/interfaces/modules/client/pws/search.interface';
import {SearchItemListInterface} from '@app/common/interfaces/modules/client/pws/search-item-list.interface';
import {SearchItemInterface} from '@app/common/interfaces/modules/client/pws/search-item.interface';
import {SearchCommentsInterface} from '@app/common/interfaces/modules/client/pws/search-comments.interface';
import {SearchItemCommentListInterface} from '@app/common/interfaces/modules/client/pws/search-item-comment-list.interface';

export enum SearchActionsTypes {
    Params = '[Search] Params',
    ParamsCalendar = '[Search] ParamsCalendar',
    ParamsComments = '[Search] ParamsComments',

    LoadItem = '[Search API] Get search item',
    LoadComments = '[Search API] Get search comments',
    LoadList = '[Search API] Get search list',
    LoadCalendar = '[Search API] Get search calendar',

    LoadListSuccess = '[Search API Success] Search load list success',
    LoadCommentsSuccess = '[Search API Success] Search load comments success',
    LoadCalendarSuccess = '[Search API Success] Search load calendar success',
    LoadItemSuccess = '[Search API Success] Search load item success',

    RefreshComments = '[Search Item Comments refresh]',
    RefreshItem = '[Search Item refresh]',
    RefreshCalendar = '[Search Calendar refresh]',
    RefreshList = '[Search List refresh]',

    NextListPage = '[Search Next List Page]',
    NextCommentsPage = '[Search Next Comments Page]'
}

/**
 * Init
 */

export class Params implements Action {
    readonly type = SearchActionsTypes.Params;

    constructor(public params: SearchInterface) {
    }
}

export class ParamsCalendar implements Action {
    readonly type = SearchActionsTypes.ParamsCalendar;

    constructor(public paramsCalendar: SearchInterface) {
    }
}

export class ParamsComments implements Action {
    readonly type = SearchActionsTypes.ParamsComments;

    constructor(public paramsComments: SearchCommentsInterface) {
    }
}

/**
 * Action load
 */

export class LoadItem implements Action {
    readonly type = SearchActionsTypes.LoadItem;

    constructor(public params: SearchInterface) {
    }
}

export class LoadCalendar implements Action {
    readonly type = SearchActionsTypes.LoadCalendar;

    constructor(public paramsCalendar: SearchInterface) {
    }
}

export class LoadComments implements Action {
    readonly type = SearchActionsTypes.LoadComments;

    constructor(public paramsComments?: SearchCommentsInterface) {
    }

}

export class LoadList implements Action {
    readonly type = SearchActionsTypes.LoadList;

}

/**
 * Success load
 */

export class LoadItemSuccess implements Action {
    readonly type = SearchActionsTypes.LoadItemSuccess;

    constructor(public payload: SearchItemInterface) {
    }
}

export class LoadCommentsSuccess implements Action {
    readonly type = SearchActionsTypes.LoadCommentsSuccess;

    constructor(public payload: SearchItemCommentListInterface) {
    }
}

export class LoadCalendarSuccess implements Action {
    readonly type = SearchActionsTypes.LoadCalendarSuccess;

    constructor(public payload: SearchItemInterface) {
    }
}

export class LoadListSuccess implements Action {
    readonly type = SearchActionsTypes.LoadListSuccess;

    constructor(public payload: SearchItemListInterface) {
    }
}

/**
 * Refresh
 */

export class RefreshList implements Action {
    readonly type = SearchActionsTypes.RefreshList;
}

export class RefreshComments implements Action {
    readonly type = SearchActionsTypes.RefreshComments;
}

export class RefreshCalendar implements Action {
    readonly type = SearchActionsTypes.RefreshCalendar;
}

export class RefreshItem implements Action {
    readonly type = SearchActionsTypes.RefreshItem;
}

/**
 * Next page
 */

export class NextListPage implements Action {
    readonly type = SearchActionsTypes.NextListPage;
}

export class NextCommentsPage implements Action {
    readonly type = SearchActionsTypes.NextCommentsPage;
}

export type PwsActions
    = Params
    | ParamsCalendar
    | ParamsComments
    | LoadItemSuccess
    | LoadCommentsSuccess
    | LoadCalendarSuccess
    | LoadListSuccess;
