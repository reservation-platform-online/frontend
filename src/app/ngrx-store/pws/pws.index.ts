import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromSearch from '@app/ngrx-store/pws/pws.reducer';

export const selectPwsState = createFeatureSelector<fromSearch.State>('pws');

export const selectPwsParams = createSelector(
    selectPwsState,
    fromSearch.selectParams
);

export const selectPwsParamsCalendar = createSelector(
    selectPwsState,
    fromSearch.selectParamsCalendar
);

export const selectPwsParamsComments = createSelector(
    selectPwsState,
    fromSearch.selectParamsComments
);

export const selectPwsList = createSelector(
    selectPwsState,
    fromSearch.selectList
);

export const selectPwsCalendar = createSelector(
    selectPwsState,
    fromSearch.selectCalendar
);

export const selectPwsItem = createSelector(
    selectPwsState,
    fromSearch.selectItem
);

export const selectPwsComments = createSelector(
    selectPwsState,
    fromSearch.selectComments
);
