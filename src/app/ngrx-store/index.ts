import {ActionReducerMap} from '@ngrx/store';

import * as fromAuth from '@app/ngrx-store/authentication/authentication.reducer';
import * as fromProfile from '@app/ngrx-store/profile/profile.reducer';
import * as fromReservation from '@app/ngrx-store/reservation/reservation.reducer';
import * as fromPws from '@app/ngrx-store/pws/pws.reducer';
import * as fromCategory from '@app/ngrx-store/category/category.reducer';
import * as fromService from '@app/ngrx-store/service/service.reducer';
import * as fromBusiness from '@app/ngrx-store/business/business.reducer';
import * as fromBusinessPws from '@app/ngrx-store/business/pws/pws.reducer';
import * as fromBusinessReservation from '@app/ngrx-store/business/reservation/reservation.reducer';

export * from '@app/ngrx-store/authentication/authentication.index';
export * from '@app/ngrx-store/profile/profile.index';
export * from '@app/ngrx-store/reservation/reservation.index';
export * from '@app/ngrx-store/pws/pws.index';
export * from '@app/ngrx-store/category/category.index';
export * from '@app/ngrx-store/service/service.index';
export * from '@app/ngrx-store/business/business.index';
export * from '@app/ngrx-store/business/pws/pws.index';
export * from '@app/ngrx-store/business/reservation/reservation.index';

export interface State {
    auth: fromAuth.State;
    profile: fromProfile.State;
    reservation: fromReservation.State;
    category: fromCategory.State;
    service: fromService.State;
    pws: fromPws.State;
    business: fromBusiness.State;
    businessPws: fromBusinessPws.State;
    businessReservation: fromBusinessReservation.State;
}

export const reducers: ActionReducerMap<State> = {
    auth: fromAuth.reducer,
    profile: fromProfile.reducer,
    reservation: fromReservation.reducer,
    category: fromCategory.reducer,
    service: fromService.reducer,
    pws: fromPws.reducer,
    business: fromBusiness.reducer,
    businessPws: fromBusinessPws.reducer,
    businessReservation: fromBusinessReservation.reducer
};
