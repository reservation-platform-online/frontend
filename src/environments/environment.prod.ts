import {apiPaths} from './api-paths';
import {Keys} from './data/keys';
import {typeCompanyList} from './data/type-company-id';
import {filterList} from './data/filter-list';
import {reservation} from './data/reservation';

export const environment = {
  production: true,
  host: 'https://reservation-platform-online/',
  hostProxy: 'local-external/',
  paths: apiPaths,
  keys: Keys,
  clientId: 'aa50194a-7f7f-48c1-91f7-31a279a9cb6c',
  typeCompanyList,
  filters: filterList,
  data: {
    reservation
  }
};
