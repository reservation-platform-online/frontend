export const apiPaths = {

    client: {
        report: 'api/v1/client/report',
        privacyPolicy: 'api/v1/client/privacy-policy',
        auth: {
            login: 'api/v1/client/auth/login',
            loginExternal: 'api/v1/client/auth/external-login',
            platformList: 'api/v1/client/auth/platform-list',
            registration: 'api/v1/client/auth/registration',
            registrationCompany: 'api/v1/client/auth/registration-company',
            resetPassword: 'api/v1/client/auth/reset-password',
            confirmEmail: 'api/v1/client/auth/confirm-email',
            agreements: 'api/v1/client/auth/agreements'
        },
        companies: {
            update: 'api/v1/client/companies',
            create: 'api/v1/client/companies',
            view: 'api/v1/client/companies/',
            index: 'api/v1/client/companies'
        },
        categories: {
            index: 'api/v1/client/categories'
        },
        services: {
            index: 'api/v1/client/services'
        },
        pws: {
            index: 'api/v1/client/pws',
            view: 'api/v1/client/pws/',
            comments: 'api/v1/client/pws/',
            calendar: 'api/v1/client/pws/'
        },
        workers: {
        },
        points: {
        },
        localities: {
            cities: 'api/v1/client/localities/cities',
        },
        user: {
            me: 'api/v1/client/user/me',
            connectExternal: 'api/v1/client/user/external-connect',
            logout: 'api/v1/client/user/logout',
            changePassword: 'api/v1/client/user/change-password',
            edit: 'api/v1/client/user',
            uploadImage: 'api/v1/client/user/upload-image'
        },
        reservation: {
            create: 'api/v1/client/reservation',
            comment: 'api/v1/client/reservation/comment',
            cancel: 'api/v1/client/reservation/',
            index: 'api/v1/client/reservation',
        }
    },

    worker: {
        zoom: {
            connect: 'api/v1/company/workers/connect-zoom/',
            disconnect: 'api/v1/company/workers/disconnect-zoom/',
        },
        reservations: {
            view: 'api/v1/worker/reservations/',
            index: 'api/v1/worker/reservations',
            create: 'api/v1/worker/reservations',
            update: 'api/v1/worker/reservations/',
            delete: 'api/v1/worker/reservations',
            createRoom: 'api/v1/worker/reservations/',
        },
        worker: {
            index: 'api/v1/worker/worker',
            update: 'api/v1/worker/worker',
            delete: 'api/v1/worker/worker',
            toggleEnable: 'api/v1/worker/worker/toggle-enable',
            uploadImage: 'api/v1/worker/worker/upload-image'
        }
    },

    company: {
        company: {
            update: 'api/v1/company/company',
            delete: 'api/v1/company/company',
            index: 'api/v1/company/company',
            uploadImage: 'api/v1/company/company/upload-image'
        },
        workers: {
            view: 'api/v1/company/workers/',
            index: 'api/v1/company/workers',
            create: 'api/v1/company/workers',
            update: 'api/v1/company/workers/',
            delete: 'api/v1/company/workers/',
            connectService: 'api/v1/company/workers/connect-service/',
            disconnectService: 'api/v1/company/workers/disconnect-service/',
            uploadImage: 'api/v1/company/workers/upload-image/'
        },
        pws: {
            index: 'api/v1/company/pws',
            view: 'api/v1/company/pws/',
            update: 'api/v1/company/pws/',
            comments: 'api/v1/company/pws/'
        },
        workTime: {
            view: 'api/v1/company/work-times/',
            create: 'api/v1/company/work-times',
            update: 'api/v1/company/work-times/',
            delete: 'api/v1/company/work-times/',
        },
        points: {
            view: '',
            index: ''
        },
        services: {
            view: 'api/v1/company/services/',
            index: 'api/v1/company/services',
            create: 'api/v1/company/services',
            update: 'api/v1/company/services/',
            delete: 'api/v1/company/services/',
            connectWorker: 'api/v1/company/services/connect-worker/',
            disconnectWorker: 'api/v1/company/services/disconnect-worker/',
            uploadImage: 'api/v1/company/services/upload-image/'

        },
        reservations: {
            view: 'api/v1/company/reservations/',
            index: 'api/v1/company/reservations',
            create: 'api/v1/company/reservations',
            update: 'api/v1/company/reservations/',
            delete: 'api/v1/company/reservations',
        },
    },

    filter: {
        limit: 10,
        page: 1,
        type: null,
        orderBy: 'created_at',
        orderDir: 'DESC',
        status: null
    },

};
