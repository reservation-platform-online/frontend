// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {apiPaths} from './api-paths';
import {Keys} from './data/keys';
import {typeCompanyList} from './data/type-company-id';
import {filterList} from './data/filter-list';
import {reservation} from './data/reservation';

export const environment = {
    production: false,
    host: 'https://reservation-platform-online/',
    hostProxy: 'local/',
    paths: apiPaths,
    clientId: 'aa50194a-7f7f-48c1-91f7-31a279a9cb6c',
    keys: Keys,
    typeCompanyList,
    filters: filterList,
    data: {
        reservation
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
