export const reservation = {
    status: [
        'Anulowana przez klienta',
        'Oczekuje opłaty',
        'Potwierdzono',
        'Skończona',
        'Anulowana przez wykonawcę',
        'Wygasł czas oczekiwania',
        'Wykonawca nie przyszedł'
    ]
};
