export const typeCompanyList = [
    {
        id: 1,
        name: 'Jedna osoba'
    },
    {
        id: 2,
        name: 'Mikro firma (do 9 pracowników)'
    },
    {
        id: 3,
        name: 'Mała firma (10 - 49 pracowników)'
    },
    {
        id: 4,
        name: 'Średnia firma (50 - 249 pracowników)'
    },
    {
        id: 5,
        name: 'Duża firma (250+ pracowników)'
    },
];
