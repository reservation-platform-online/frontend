export const filterList = [
    {
        name: 'Cena: od najmniejszej',
        value: {
            dir: 'ASC',
            by: 'price'
        }
    },
    {
        name: 'Cena: od największej',
        value: {
            dir: 'DESC',
            by: 'price'
        }
    },
    {
        name: 'Ocena: od najmniejszej',
        value: {
            dir: 'ASC',
            by: 'score'
        }
    },
    {
        name: 'Ocena: od największej',
        value: {
            dir: 'DESC',
            by: 'score'
        }
    },
];
