export const Keys = {
    AUTH_TOKEN: 'reservation-platform-app-token',
    COMPANY_ID: 'reservation-platform-app-company-id',
    WORKER_ID: 'reservation-platform-app-worker-id'

};
