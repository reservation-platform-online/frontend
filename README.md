#IONIC
- Version: 5
```
ionic serve
```

Before run android app need build ionic www via command `ionic build` after building need open android studio and run project or build apk (appbundle for published)

#ANGULAR
- Version: 10
```
ng serve
```

#Links
- https://ionicframework.com/docs/angular/lifecycle
